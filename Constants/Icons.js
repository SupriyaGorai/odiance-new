
export default Icons = {
  home: require('../assets/icons/home.png'),
  history: require('../assets/icons/history.png'),
  notification: require('../assets/icons/notification.png'),
  profile: require('../assets/icons/profile.png'),
  search: require('../assets/icons/search.png'),
  
};
