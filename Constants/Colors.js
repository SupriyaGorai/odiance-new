
export default Colors = {
    primary: '#66001a',
    white: '#fff',
    greyIcon: 'grey',
    lightGrey: '#e5e5e5',
    blueGrey: '#f6f6f6',
    black: '#111'
}