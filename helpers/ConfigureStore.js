import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import codeReducer from "../_reducers/CodeVerificationReducer";
import {NavigationReducer} from "../_reducers/NavigationReducer";
import {
  HomeReducers,
  AuthenticationReducers,
  PostReducers,
  CommentsReducers,
  HistoryReducer,
} from "../_reducers";

const rootReducer = combineReducers({
  code: codeReducer,
  search: HomeReducers.toggleSearch,
  user: AuthenticationReducers.status,
  post: PostReducers.Post_status,
  comment: CommentsReducers.Comment_status,
  history: HistoryReducer,
  route :  NavigationReducer.setRoute,
});

const configureStore = () => createStore(rootReducer, applyMiddleware(thunk));

export default configureStore;
