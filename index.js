/**
 * @format
 */
import React from 'react';


import { name as appName } from './app.json';
import { Provider } from 'react-redux';

import {AppRegistry} from 'react-native';
import App from './App';
// import * as RNLocalize from "react-native-localize";



import configureStore from './helpers/ConfigureStore';

const store = configureStore();

const AppMain = () =>
  <Provider store={store}>
    <App />
  </Provider>

AppRegistry.registerComponent(appName, () => AppMain);
AppRegistry.registerComponent('RNAudioRecorderPlayerEx',  () => App);
// AppRegistry.registerComponent(RNLocalize, () => App)
