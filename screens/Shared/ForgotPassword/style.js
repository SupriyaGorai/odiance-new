import { StyleSheet, Dimensions } from "react-native";
const devHeight = Dimensions.get("window").height;
const devWidth = Dimensions.get("window").width;

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  header: {
    width: (devWidth * 100) / 100,
    backgroundColor: "#66001a",
    height: 60,
  },
  boderP: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#dcdcdc",
    borderRadius: 30,
    height: 50,
    width: "90%",
    top: "15%",
  },
  labelContainer: {
    position: "absolute",
    color: "#66001a",
    top: -15,
    left: 25,
    padding: 5,
    zIndex: 100,
    backgroundColor: "#fff",
    fontSize: 15,
  },
  input: {
    flex: 0.95,
    color: "#66001a",
    borderColor: "#dcdcdc",
    paddingLeft: "5%",
   
  },
  icon: {},
  getOtpBtn: {
    top: "12%",
    backgroundColor: "#66001a",
    borderRadius: 30,
    marginTop:40,
    paddingVertical: 8,
    paddingHorizontal: 20,
  },
  getOtpText: {
    color: "#fff",
  },
  verifyView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    top: "35%",
  },
  verifyIinput: {
    borderWidth: 1,
    borderColor: "#dcdcdc",
    borderRadius: 30,
    height: 42,
    width: "50%",
    marginHorizontal: 10,
    paddingHorizontal: 10,
  },
  veifyBtn: {
    backgroundColor: "#66001a",
    borderRadius: 30,
    paddingVertical: 8,
    paddingHorizontal: 20,
  },
  verifyText: {
    color: "#fff",
  },
});

export default styles;
