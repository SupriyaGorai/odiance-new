import React, { Component } from "react";
import getOTPCall from  '../../../_services/getOTPServices';
import OTPVerifyCall from  '../../../_services/VerifyOTPServices';
import ChangePasswordCall from '../../../_services/changePasswordServices';
import {connect} from 'react-redux';
import ProfileHeader from '../../Creator/components/ProfileHeader'
import { TouchableOpacity, ScrollView, Image, TextInput,ToastAndroid} from "react-native";
import {ActivityIndicator} from 'react-native-paper';
import {
  Container,
  Header,
  Text,
  View,
  Left,
  Body,
  Button,
  
  Title,
} from "native-base";
import styles from "./style";
import { Icon } from "react-native-elements";

export class ForgotPasswordFirstPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success : false,
      newpasswordSet :false,
      currentPassemail : '',
      currentPassOTP :'',
      currentPassNewPassword:'',
      currentPassRePassword:'',
      Token: '',
      ActivityIndicator: false
    };
  }
  onPress(){
    this.setState({success: false})
    this.setState({newpasswordSet: false})
  }
  async GetOTP(){
    this.setState({ActivityIndicator: !this.state.ActivityIndicator})
    const getRes = await getOTPCall(this.state.currentPassemail)
    console.log(getRes,'kkkk');
    if(getRes.success == true)
    {this.setState({success: !this.state.success});
    this.setState({ActivityIndicator: !this.state.ActivityIndicator})
  }
    else{
      this.setState({ActivityIndicator: !this.state.ActivityIndicator})
    }
  }
  async Verify(){
    this.setState({ActivityIndicator: !this.state.ActivityIndicator})
    const getVerifyRes = await OTPVerifyCall(this.state.currentPassemail,this.state.currentPassOTP)
    console.log(getVerifyRes,'verify res')
    if(getVerifyRes.ack == 1){
      this.setState({Token: getVerifyRes.token})
      this.setState({newpasswordSet: !this.state.newpasswordSet})
      this.setState({ActivityIndicator: !this.state.ActivityIndicator})
    }
    else{
      this.setState({ActivityIndicator: !this.state.ActivityIndicator})
    }
  }
  async ChangePassword(){
    this.setState({ActivityIndicator: !this.state.ActivityIndicator})
    if(this.state.currentPassNewPassword == this.state.currentPassRePassword){
      
   const getChangePasswordRes = await ChangePasswordCall(this.state.Token,this.state.currentPassRePassword)
   console.log(getChangePasswordRes,'getChangePasswordRes')
   if(getChangePasswordRes.success == true){
    this.setState({ActivityIndicator: !this.state.ActivityIndicator})
    ToastAndroid.show(getChangePasswordRes.msg,0.5)
    this.props.navigation.jumpTo('EmailLogin')
   }
   else{
    this.setState({ActivityIndicator: !this.state.ActivityIndicator})
  }
  }else{
    ToastAndroid.show("Password doesn't match",0.5)
    this.setState({ActivityIndicator: !this.state.ActivityIndicator})
  }
  }
  doClear(fieldname) {
    const textInput = this.refs[fieldname];
    textInput.clear();
  }
  render() {
    const { navigation, status } = this.props;
    console.log(status,'llllkk');
    return (
      <ScrollView>
      <Container style={styles.container}>
      
      <ProfileHeader  navigation={navigation} />
     

         {/* <Left>
            <Button onPress={() => navigation.goBack()} transparent>
              <Icon color="#fff" name="arrow-back" />
            </Button>
          </Left>
          <Body style={{ left: 20 }}>
             <Title style={{ color: "#fff", fontSize: 17 }}>
              Forgot Password
          </Title> 
          </Body>  */}
        {/* get otp */}
        {this.state.success ?
        (
          this.state.newpasswordSet ?(
            <View style={{justifyContent:'center',alignItems:'center'}}>
              <TouchableOpacity style={{position:'absolute',right:-10,top:10,width:50,height:30}}
         onPress ={() =>  this.onPress()}
         >
           <Text style={{fontWeight:'bold',color:'#66001a'}}>
             Cancle
           </Text>
         </TouchableOpacity>
        <Text style={{fontSize:25,marginTop:55,color:'#66001a'}}>
         Reset Your Password
        </Text>
        <Text style={{padding:10,paddingHorizontal:30,textAlign: 'center',color:'#dcdcdc'}}>
          Please enter a new password
        </Text>
     
        </View>
          ):
          (<View style={{justifyContent:'center',alignItems:'center'}}>
         <TouchableOpacity style={{position:'absolute',right:20,top:10,width:50,height:30}}
         onPress ={() =>  this.onPress()}
         >
           <Text style={{fontWeight:'bold',color:'#66001a'}}>
             Cancle
           </Text>
         </TouchableOpacity>
        <Text style={{fontSize:25,marginTop:55,color:'#66001a'}}>
          Verification
        </Text>
        <Text style={{padding:10,paddingHorizontal:30,textAlign: 'center',color:'#dcdcdc'}}>
          Please Enter your OTP code sent to your email address
        </Text>
     
        </View>)
        ):
        (
          <View style={{justifyContent:'center',alignItems:'center'}}>
          <Text style={{fontSize:25,marginTop:55,color:'#66001a'}}>
            Forgot password?
          </Text>
          <Text style={{padding:10,paddingHorizontal:30,textAlign: 'center',color:'#dcdcdc'}}>
            Just enter your registered email address to 
            send you OTP
          </Text>
       
          </View>

        )}
        {this.state.success ?
        (
          this.state.newpasswordSet ?(
            
            <View style={{flex:1}}>
              <View style={styles.boderP}>
          <Text style={styles.labelContainer}>New Password</Text>
          <TextInput
          ref="New Password"
            style={styles.input}
            placeholder="New Password"
            value={this.state.currentPassNewPassword}
             secureTextEntry={true}
            onChangeText={(NewPassword) => this.setState({ currentPassNewPassword: NewPassword })}
          />
          <Icon
            name="cancel"
            color="#DFDFDF"
            containerStyle={styles.icon}
            onPress={() => this.doClear("New Password")}
          />
        </View>
        <View
        style={{height:25}}
        />
        <View style={styles.boderP}>
          <Text style={styles.labelContainer}>Re-enter Password</Text>
          <TextInput
          ref="Re-enter Password"
            style={styles.input}
            placeholder="Re-enter Password"
            value={this.state.currentPassRePassword}
             secureTextEntry={true}
             onChangeText={(RePassword) => this.setState({ currentPassRePassword: RePassword })}
          />
          <Icon
            name="cancel"
            color="#DFDFDF"
            containerStyle={styles.icon}
            onPress={() => this.doClear("Re-enter Password")}
          />
        </View>

            </View>
          ):
          (<View style={styles.boderP}>
          <Text style={styles.labelContainer}>Enter OTP</Text>
          <TextInput
          ref="enter OTP"
            style={styles.input}
            placeholder="enter OTP"
            value={this.state.currentPassOTP}
             secureTextEntry={true}
             onChangeText={(OTP) => this.setState({ currentPassOTP: OTP })}
          />
          <Icon
            name="cancel"
            color="#DFDFDF"
            containerStyle={styles.icon}
            onPress={() => this.doClear("enter OTP")}
          />
        </View>)
        ):
        (
       
        <View style={styles.boderP}>
          <Text style={styles.labelContainer}>Enter your email</Text>
          <TextInput
          ref="email-address"
            style={styles.input}
            placeholder="enter email"
            keyboardType="email-address"
            value={this.state.currentPass}
            secureTextEntry={true}
           onChangeText={(email) => this.setState({ currentPassemail: email })}
          />
          <Icon
            name="cancel"
            color="#DFDFDF"
            containerStyle={styles.icon}
            onPress={() => this.doClear("email-address")}
          />
        </View>)}

        {this.state.success ?
        (
          this.state.newpasswordSet ?(
            <View style={{flex:1}}>
            <TouchableOpacity
          style={styles.getOtpBtn}
          onPress={()=> this.ChangePassword()}
          // onPress ={()=> this.setState({success: !this.state.success})}
        >
           {this.state.ActivityIndicator ? (
             <ActivityIndicator />

          ):
          (
            <Text style={styles.getOtpText}>Change Password</Text>
          )}
          
          
        </TouchableOpacity>
        </View>
          ):

          (<TouchableOpacity
          style={styles.getOtpBtn}
          //  onPress ={()=> this.setState({newpasswordSet: !this.state.newpasswordSet})}
           onPress={()=> this.Verify()}
        >
          {this.state.ActivityIndicator ? (
             <ActivityIndicator />

          ):
          (
            <Text style={styles.getOtpText}>Verify</Text>
          )}
         
        </TouchableOpacity>)
        ):
        (
        <TouchableOpacity
          style={styles.getOtpBtn}
          onPress={()=> this.GetOTP()}
          //  onPress ={()=> this.setState({success: !this.state.success})}
        >
          {this.state.ActivityIndicator ? (
             <ActivityIndicator />

          ):
          (
            <Text style={styles.getOtpText}>Get OTP</Text>

          )}
          
        </TouchableOpacity>)}

        
      </Container>
      </ScrollView>
    );
  }
}
function mapStatetoProps(state) {
  return {
    status: state.user,
  };
}

export default connect(
  mapStatetoProps,
 
  
)(ForgotPasswordFirstPage);

