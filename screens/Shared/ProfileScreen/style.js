import {styles, StyleSheet, CheckBox} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {PixelRatio, Dimensions} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../../Constants/Colors';
const {height, width} = Dimensions.get('window');
export default {
    boder: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 'auto',
      },
      inputName: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#dcdcdc',
        borderRadius: wp(30),
        height: hp('6%'),
        //width: 'auto',
        // width : wp('100%'),
        flex: 0.45,
        minWidth: wp(35),
        top: hp('5%'),
      },
      labelContainer: {
        position: 'absolute',
        color: Colors.primary,
        top: -18,
        left: 25,
        padding: 5,
        zIndex: 100,
        width: 'auto',
        backgroundColor: Colors.white,
        fontSize: 15,
      },
      iconF: {
        alignItems: 'center',
        justifyContent: 'space-around',
        width: 'auto',
        //right : wp('7%')
      },
      boderL: {
        flex: 0.45,
        borderWidth: 1,
        borderColor: '#dcdcdc',
        borderRadius: wp(30),
        height: hp('6%'),
        flexDirection: 'row',
        minWidth: wp(35),
        top: hp('5%'),
        //width: wp('40%'),
      },
      inputL: {
        flex: 0.95,
        borderColor: '#dcdcdc',
        paddingLeft: wp(7),
        color : Colors.primary,
        height: 50,
        //width: wp('40%'),
        //left: '50%',
      },
      iconL: {
        alignItems: 'center',
        justifyContent: 'space-around',
        width: 'auto',
        //right : wp('8%')
      },
    }