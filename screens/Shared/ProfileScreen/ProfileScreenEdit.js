import React from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  Modal,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Icon } from "react-native-elements";
import Colors from "../../../Constants/Colors";
import { connect } from "react-redux";
import ImagePicker from "react-native-image-crop-picker";
import urls from "../../../Constants/urls";
import LoadUserDetails from "../../../_services/UserDetails";
import UpdateArtist from "../../../_services/UserArtistEditService";
import { CommonActions } from "@react-navigation/native";
import { POST } from "../../../_services/services";

export class ProfileScreenEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      creator_id: null,
      posts: [],
      response: null,
      edittingData: false,
    };
  }
  async componentDidMount() {
    const user_data = await LoadUserDetails(this.props.status.id);

    if (user_data) {
      this.setState({ user_data: user_data });
    }
  }
  render() {
    const { status, Post_status } = this.props;
    const navigation = this.props.navigation;
    return (
      <View>
        {this.state.user_data ? (
          <Details
            navigation={navigation}
            image={this.state.user_data}
            token={status.token}
            isCreator={this.props.status.isCreator}
          />
        ) : null}
      </View>
    );
  }
}

class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edittingData: false,
      Coverphoto: this.props.image.cover_pic,
      modalVisible: false,
      modalVisibleForCover: false,
      token: this.props.token,
      userId: this.props.image.id,
      first_name_edit: this.props.image.first_name,
      last_name_edit: this.props.image.last_name,
      avatar: this.props.image.avatar,
      bio: "",
      about: "",
      lat: this.props.image.latitude,
      long: this.props.image.longitude,
      photoData: [],
      userImage: "",
      updateArtistData: [],
    };
  }

  choosePhotofromGallery = () => {
    this.setState({ modalVisible: !this.state.modalVisible });
    const tempImage = this.state.avatar;
    ImagePicker.openPicker({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then((response) => {
      if (response.path !== "undefined") {
        this.setState({ avatar: response.path });
      } else {
        this.setState({ avatar: tempImage });
      }
    });
  };

  choosePhotofromGalleryForCover = () => {
    this.setState({ modalVisibleForCover: !this.state.modalVisibleForCover });
    const tempImage = this.state.Coverphoto;
    ImagePicker.openPicker({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then((response) => {
      if (response.path !== "undefined") {
        this.setState({ Coverphoto: response.path });
      } else {
        this.setState({ Coverphoto: tempImage });
      }
    });
  };
  choosePhotofromCamera() {
    this.setState({ modalVisible: !this.state.modalVisible });
    const tempImage = this.state.avatar;
    ImagePicker.openCamera({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then((response) => {
      if (response.path !== "undefined") {
        this.setState({ avatar: response.path });
      } else {
        this.setState({ avatar: tempImage });
      }
    });
  }
  choosePhotofromCameraCover() {
    this.setState({ modalVisibleForCover: !this.state.modalVisibleForCover });
    const tempImage = this.state.Coverphoto;
    ImagePicker.openCamera({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then((response) => {
      if (response.path !== "undefined") {
        this.setState({ Coverphoto: response.path });
      } else {
        this.setState({ Coverphoto: tempImage });
      }
    });
  }
  async uploadPhoto(token) {
    const cover_pic = {
      name: `${new Date().getTime()}Coverphoto.jpg`,
      type: "image/jpg",
      uri: this.state.Coverphoto,
    };

    const formData = new FormData();

    formData.append("avatar", cover_pic);
    formData.append("image_type", "cover");

    await fetch(`${urls.base}${urls.avatar}`, {
      method: "POST",
      headers: {
        Authorization: `Token ${token}`,
      },
      body: formData,
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({ Coverphoto: responseData.link });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  uploadProfilePhoto = async (token) => {
    const Photo_pic = {
      name: `${new Date().getTime()}photo.jpg`,
      type: "image/jpg",
      uri: this.state.avatar,
    };
    const formData = new FormData();
    formData.append("avatar", Photo_pic);
    formData.append("image_type", "profile");

    fetch(`${urls.base}${urls.avatar}`, {
      method: "POST",
      headers: {
        Authorization: `Token ${token}`,
      },
      body: formData,
    })
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({ avatar: responseData.link }, async () => {
          const data = {
            id: this.state.userId,
            first_name: this.state.first_name_edit,
            last_name: this.state.last_name_edit,
            avatar: this.state.avatar,
            alumni: "",
            bio: this.state.bio,
            about: this.state.about,
            city: "",
            country: "",
            latitude: this.state.lat,
            longitude: this.state.long,
            is_creator: this.props.image.is_creator,
          };

          const url = "user/update-artist/";

          const res = await POST(url, token, data);
          if (res.status === 200) {
            this.setState({ edittingData: false });
            if (this.props.isCreator) {
              this.props.navigation.navigate("profile", { updateData: true });
            } else {
              this.reset("History");
            }
          }
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  reset = (name) => {
    const resetAction = CommonActions.reset({
      index: 0,
      routes: [{ name: name }],
    });
    this.props.navigation.dispatch(resetAction);
  };

  choosePhotofromCameraForCover() {
    this.setState({ modalVisibleForCover: !this.state.modalVisibleForCover });

    ImagePicker.openCamera({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then((response) => {
      this.setState({ Coverphoto: response });
    });
  }
  async userEditDataSave() {
    this.setState({ edittingData: true });
    await this.uploadPhoto(this.props.token);
    await this.uploadProfilePhoto(this.props.token);
  }

  doClear(fieldname) {
    let textInput = this.refs[fieldname];
    textInput.clear();
  }

  imageLink = (props) => {
    if (this.state.avatar !== undefined) {
      if (this.state.avatar[0] === "/") {
        return `http://111.93.169.90:8008${this.state.avatar}`;
      } else {
        return `${this.state.avatar}`;
      }
    }
  };

  render() {
    const { image, token } = this.props;
    const navigation = this.props.navigation;
    return (
      <ScrollView style={{ backgroundColor: "white" }}>
        <View style={{ flexDirection: "column", backgroundColor: "white" }}>
          <View style={{ height: wp(50), width: "100%" }}>
            <TouchableOpacity
              style={{
                position: "absolute",
                zIndex: 10,
                padding: 10,
                margin: 10,
                backgroundColor: "#00000020",
                borderRadius: 5,
              }}
            >
              <Icon
                name="arrowleft"
                type="antdesign"
                size={20}
                color="#fff"
                onPress={() => {
                  navigation.goBack();
                }}
              />
            </TouchableOpacity>
            <View style={{ height: wp(50), backgroundColor: "#7f8c8d" }}>
              {this.state.Coverphoto ? (
                <Image
                  source={{ uri: this.state.Coverphoto }}
                  style={{
                    width: "auto",
                    resizeMode: "cover",
                    height: "100%",
                    borderRadius: 3,
                    flex: 1,
                  }}
                />
              ) : null}
            </View>
            <View>
              <Icon
                name="camera"
                type="font-awesome"
                size={25}
                color={"#fff"}
                onPress={() =>
                  this.setState({
                    modalVisibleForCover: !this.state.modalVisibleForCover,
                  })
                }
                containerStyle={{
                  position: "absolute",
                  top: -35,

                  right: 15,
                  padding: 5,
                  backgroundColor: "#00000020",
                  borderRadius: 5,
                }}
              />
            </View>
          </View>
          <View
            style={{
              width: "100%",
              flexDirection: "row",
              display: "flex",
              justifyContent: "center",
              background: "#000",
            }}
          >
            <View
              style={{
                position: "relative",
                width: 150,
                height: 150,
                marginTop: -80,
              }}
            >
              {this.state.avatar ? (
                <Image
                  source={{ uri: this.imageLink() }}
                  // source={{ uri: `${this.state.avatar}` }}
                  style={{
                    width: "100%",
                    height: "100%",
                    borderRadius: 100,
                    aspectRatio: 1,
                    borderColor: "#eee",
                    borderWidth: 5,
                    justifyContent: "center",
                  }}
                />
              ) : (
                <Image
                  source={require("../../../assets/icons/user_male2-512.png")}
                  style={{
                    width: "100%",
                    height: "100%",
                    borderRadius: 100,
                    aspectRatio: 1,
                    borderColor: "black",
                    borderWidth: 5,
                    justifyContent: "center",
                  }}
                />
              )}
              <Icon
                name="pencil"
                type="font-awesome"
                size={25}
                onPress={() =>
                  this.setState({ modalVisible: !this.state.modalVisible })
                }
                containerStyle={{
                  position: "absolute",
                  height: 35,
                  width: 35,
                  bottom: 0,
                  right: 20,
                  zIndex: 10,
                  backgroundColor: "#fff",
                  padding: 5,
                  borderRadius: 30,
                  borderWidth: 1,
                  borderColor: "#000",
                }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: "column",
              padding: 10,
              justifyContent: "space-between",
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                width: "auto",
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  borderWidth: 1,
                  borderColor: "#dcdcdc",
                  // borderRadius: wp(30),
                  height: hp("6%"),
                  //width: 'auto',
                  // width : wp('100%'),
                  flex: 0.45,
                  minWidth: wp(25),
                  top: hp("5%"),
                }}
              >
                <Text
                  style={{
                    position: "absolute",
                    color: Colors.primary,
                    top: -18,
                    left: 25,
                    padding: 5,
                    zIndex: 100,
                    width: "auto",
                    backgroundColor: Colors.white,
                    fontSize: 15,
                  }}
                >
                  First name *
                </Text>
                <TextInput
                  ref="first_name_edit"
                  style={{
                    flex: 0.95,
                    borderColor: "#dcdcdc",
                    paddingLeft: wp("0%"),
                    color: Colors.primary,
                    height: 50,
                    // width: wp('40%'),
                    right: wp("10%"),
                    left: wp("4%"),
                  }}
                  placeholder="First Name"
                  defaultValue={this.state.first_name_edit}
                  onChangeText={(first_name_edit) => {
                    this.setState({ first_name_edit: first_name_edit });
                  }}
                />
                <Icon
                  name="cancel"
                  color="#DFDFDF"
                  containerStyle={{
                    alignItems: "center",
                    justifyContent: "space-around",
                    width: "auto",
                  }}
                  onPress={() => this.doClear("first_name_edit")}
                />
              </View>
              <View style={{ width: 6 }} />
              <View
                style={{
                  flex: 0.45,
                  borderWidth: 1,
                  borderColor: "#dcdcdc",
                  // borderRadius: wp(30),
                  height: hp("6%"),
                  flexDirection: "row",
                  minWidth: wp(25),
                  top: hp("5%"),
                }}
              >
                <Text
                  style={{
                    position: "absolute",
                    color: Colors.primary,
                    top: -18,
                    left: 25,
                    padding: 5,
                    zIndex: 100,
                    width: "auto",
                    backgroundColor: Colors.white,
                    fontSize: 15,
                  }}
                >
                  Last name *
                </Text>
                <TextInput
                  ref="last_name_edit"
                  style={{
                    flex: 0.95,
                    borderColor: "#dcdcdc",
                    paddingLeft: wp("0%"),
                    color: Colors.primary,
                    height: 50,
                    // width: wp('40%'),
                    right: wp("10%"),
                    left: wp("4%"),
                  }}
                  placeholder="Last Name"
                  defaultValue={this.state.last_name_edit}
                  onChangeText={(last_name_edit) => {
                    this.setState({ last_name_edit: last_name_edit });
                  }}
                />
                <Icon
                  name="cancel"
                  color="#DFDFDF"
                  containerStyle={{
                    alignItems: "center",
                    justifyContent: "space-around",
                    width: "auto",
                  }}
                  onPress={() => this.doClear("last_name_edit")}
                />
              </View>
            </View>
            <View style={{ height: 20 }} />
            <View
              style={{
                flexDirection: "row",
                borderWidth: 1,
                borderColor: "#dcdcdc",
                // borderRadius: wp(30),
                height: hp("13%"),
                //width: 'auto',
                // width : wp('100%'),
                flex: 0.45,
                minWidth: wp(35),
                top: hp("5%"),
                marginHorizontal: 5,
              }}
            >
              <Text
                style={{
                  position: "absolute",
                  color: Colors.primary,
                  top: -18,
                  left: 25,
                  padding: 5,
                  zIndex: 100,
                  width: "auto",
                  backgroundColor: Colors.white,
                  fontSize: 15,
                }}
              >
                Bio *
              </Text>
              <TextInput
                ref="bio"
                style={{
                  flex: 0.95,
                  borderColor: "#dcdcdc",
                  paddingLeft: wp("3%"),
                  color: Colors.primary,
                  height: 50,
                  // width: wp('40%'),
                  right: wp("10%"),
                  left: wp("4%"),
                }}
                multiline
                placeholder="bio"
                onChangeText={(bio) => {
                  this.setState({ bio: bio });
                }}
              />
              <Icon
                name="cancel"
                color="#DFDFDF"
                containerStyle={{
                  alignItems: "center",
                  justifyContent: "space-around",
                  width: "auto",
                }}
                onPress={() => this.doClear("bio")}
              />
            </View>

            <View style={{ height: 20 }} />
            <View
              style={{
                flexDirection: "row",
                borderWidth: 1,
                borderColor: "#dcdcdc",
                // borderRadius: wp(30),
                height: hp("13%"),
                //width: 'auto',
                // width : wp('100%'),
                flex: 0.45,
                minWidth: wp(35),
                top: hp("5%"),
                marginHorizontal: 5,
              }}
            >
              <Text
                style={{
                  position: "absolute",
                  color: Colors.primary,
                  top: -18,
                  left: 25,
                  padding: 5,
                  zIndex: 100,
                  width: "auto",
                  backgroundColor: Colors.white,
                  fontSize: 15,
                }}
              >
                About *
              </Text>
              <TextInput
                ref="about"
                style={{
                  flex: 0.95,
                  borderColor: "#dcdcdc",
                  paddingLeft: wp("3%"),
                  color: Colors.primary,
                  height: 50,
                  // width: wp('40%'),
                  right: wp("10%"),
                  left: wp("4%"),
                }}
                placeholder="about"
                onChangeText={(about) => {
                  this.setState({ about: about });
                }}
              />
              <Icon
                name="cancel"
                color="#DFDFDF"
                containerStyle={{
                  alignItems: "center",
                  justifyContent: "space-around",
                  width: "auto",
                }}
                onPress={() => this.doClear("about")}
              />
            </View>

            <View style={{ height: 100 }} />
          </View>
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              bottom: "5%",
            }}
          >
            <TouchableOpacity
              disabled={this.state.edittingData}
              style={{
                backgroundColor: "#800D1F",
                borderWidth: 1,
                borderColor: "#dcdcdc",
                // borderRadius: wp(30),
                height: 50,
                width: 350,
                alignItems: "center",
                justifyContent: "center",
              }}
              onPress={() => this.userEditDataSave(image)}
            >
              <View>
                {this.state.edittingData ? (
                  <ActivityIndicator color="#fff" />
                ) : (
                  <Text
                    style={{
                      fontSize: 18,
                      textAlign: "center",
                      color: "#FCECF1",
                    }}
                  >
                    SAVE
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          </View>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() =>
              this.setState({ modalVisible: !this.state.modalVisible })
            }
          >
            <View
              style={{ flex: 1, backgroundColor: "rgba(0,0,0,0.5)" }}
              onStartShouldSetResponder={() =>
                this.setState({ modalVisible: !this.state.modalVisible })
              }
            >
              <View
                style={{
                  flex: 0.3,
                  backgroundColor: Colors.white,
                  top: "75%",
                  padding: "5%",
                  borderRadius: 15,
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.25 }}
                  onPress={() => this.choosePhotofromCamera()}
                >
                  <Text style={{ fontSize: 20 }}>Take Photo</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 0.25 }}
                  onPress={() => this.choosePhotofromGallery()}
                >
                  <Text style={{ fontSize: 20 }}>
                    Choose Photo from Library
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ alignSelf: "flex-end", flex: 0.5 }}
                  onPress={() =>
                    this.setState({ modalVisible: !this.state.modalVisible })
                  }
                >
                  <Text style={{ fontSize: 20, right: "5%", top: "50%" }}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>

          {/* for cover */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisibleForCover}
            onRequestClose={() =>
              this.setState({
                modalVisibleForCover: !this.state.modalVisibleForCover,
              })
            }
          >
            <View
              style={{ flex: 1, backgroundColor: "rgba(0,0,0,0.5)" }}
              onStartShouldSetResponder={() =>
                this.setState({
                  modalVisibleForCover: !this.state.modalVisibleForCover,
                })
              }
            >
              <View
                style={{
                  flex: 0.3,
                  backgroundColor: Colors.white,
                  top: "75%",
                  padding: "5%",
                  borderRadius: 15,
                }}
              >
                <TouchableOpacity
                  style={{ flex: 0.25 }}
                  onPress={() => this.choosePhotofromCameraCover()}
                >
                  <Text style={{ fontSize: 20 }}>Take Photo</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ flex: 0.25 }}
                  onPress={() => this.choosePhotofromGalleryForCover()}
                >
                  <Text style={{ fontSize: 20 }}>
                    Choose Photo from Library
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ alignSelf: "flex-end", flex: 0.5 }}
                  onPress={() =>
                    this.setState({
                      modalVisibleForCover: !this.state.modalVisibleForCover,
                    })
                  }
                >
                  <Text style={{ fontSize: 20, right: "5%", top: "50%" }}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>
    );
  }
}
function mapStatetoProps(state) {
  return {
    status: state.user,
    Post_status: state.post,
  };
}
export default connect(mapStatetoProps)(ProfileScreenEdit);
