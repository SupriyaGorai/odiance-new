import React from "react";
import { ScrollView, Image } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Colors from "../../../Constants/Colors";
import loadPosts from "../../../_services/LoadPosts";
import LoadUserDetails from "../../../_services/UserDetails";
import Details from "../component/Details";
import { FeaturedVideos } from "../component/FeaturedVideos";
import { PivateDraftVideoTab } from "../component/FeaturedVideos";
import { PostCreditTab } from "../component/FeaturedVideos";
import { connect } from "react-redux";
import { POST } from "../../../_services/services";
import ActivityIndicator from "./../../Viewer/components/ActivityIndicator";

const Tab = createMaterialTopTabNavigator();
class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: "Profile",
      creator_id: null,
      posts: [],
      response: null,
      user_data: [],
      id: null,
      following: 0,
      follower: 0,
      update: false,
      updatedData: [],
      isLoading: true,
    };
  }

  navigate = (id) => {
    const { navigation } = this.props;
    navigation.navigate("Post", { post_id: id , creator_id : this.state.user_data.id});
  };

  async componentDidMount() {
    const user_data = await LoadUserDetails(this.props.status.id);

    if (user_data) {
      this.setState({ user_data: user_data });
    }

    const posts = await loadPosts(this.props.status.id);

    if (posts) {
      this.setState({ posts: posts });
    }

    let res = await POST(
      `user/${this.props.status.id}/followlist/`,
      this.props.status.token
    );

    if (res.status == 200) {
      this.setState({
        follower: res.data.follower_count,
        following: res.data.following_count,
      });
    }
  }

  render() {
    // const { status, Post_status, posts } = this.props;
    const navigation = this.props.navigation;

    if (this.state.user_data === null) {
      return <ActivityIndicator />;
    } else {
      return (
        <ScrollView style={{ flex: 1, flexDirection: "column" }}>
          {this.state.user_data ? (
            <Details
              navigation={navigation}
              image={this.state.user_data}
              follower={this.state.follower}
              following={this.state.following}
            />
          ) : null}
          {this.state.posts ? (
            <FeaturedVideos
              videos={this.state.posts}
              navigate={this.navigate}
            />
          ) : null}
          {this.props.status.id ? (
            <Tab.Navigator
              initialRouteName="Posts"
              tabBarOptions={{
                indicatorStyle: { backgroundColor: Colors.primary, height: 5 },
                showIcon: true,
                iconStyle: { width: 30, height: 30 },
                showLabel: false,
              }}
            >
              <Tab.Screen
                name="Posts"
                component={PostCreditTab}
                initialParams={{
                  navigate: this.navigate,
                  id: this.props.status.id,
                }}
                options={{
                  tabBarIcon: () => (
                    <Image
                      source={require("../../../assets/icons/grid.png")}
                      style={{ width: 25, height: 25 }}
                    />
                  ),
                }}
              />
              <Tab.Screen
                name="Credits"
                component={PivateDraftVideoTab}
                initialParams={{
                  navigate: this.navigate,
                }}
                options={{
                  tabBarIcon: () => (
                    <Image
                      source={require("../../../assets/icons/profile(1).png")}
                      style={{ width: 25, height: 25 }}
                    />
                  ),
                }}
              />
            </Tab.Navigator>
          ) : null}
        </ScrollView>
      );
    }
  }
}
function mapStatetoProps(state) {
  return {
    status: state.user,
    Post_status: state.post,
  };
}
export default connect(mapStatetoProps)(ProfileScreen);
