import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  Modal,
  Button,
  TouchableOpacity,
  Picker,
  TextInput,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {Icon} from 'react-native-elements';
import Colors from '../../../Constants/Colors';

import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import urls from '../../../Constants/urls';
// import {TextInput} from 'react-native-paper';
import UpdateViewer from '../../../_services/UserViewerEditService';

// import Select from 'react-select';

//import { TouchableOpacity } from 'react-native-gesture-handler';
const iconSize = 14;
const mainMargin = 5;
const fontSize = 10;
const Tab = createMaterialTopTabNavigator();

class NavIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      iconStyle,
      name,
      label,
      type,
      labelStyle,
      source,
      onPressButton,
    } = this.props;
    return (
      <View>
        <Image source={source} style={iconStyle} />
      </View>
    );
  }
}
class ViewerUpdate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      creator_id: null,
      posts: [],
      response: null,
    };
  }
  navigate = id => {
    const {navigation} = this.props;
    navigation.navigate('Post', {post_id: id});
  };
  // async componentDidMount() {
  //   const _this = this;
  //   if (this.props.status.user.id) {
  //     const posts = await loadPosts(this.props.status.user.id);
  //     if (posts) {
  //       //console.log(posts,'profilescreen');
  //       this.setState({posts: posts});
  //     } else {
  //       throw new Error('problem');
  //     }
  //   }
  // }
  render() {
    //     this.componentDidMount();
    const {status, Post_status} = this.props;
    //this.setState({creator_id : status.})
    console.log(status, 'ProValue');
    const navigation = this.props.navigation;
    const videos = this.state.posts;
    //this.componentDidMount();
    console.log(navigation, 'profilerender here');
    return (
      // <ScrollView style={{backgroundColor:'white'}}>
        <View>
        <Details
          navigation={navigation}
          image={status.user}
          token={status.token}
          post_upload={Post_status.user}
          post_id={this.props.status.user.id}
          navigate={this.navigate}
        />
        </View>
      // </ScrollView>
    );
  }
}
class Details extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      photo: null,
      Coverphoto: null,
      modalVisible: false,
      modalVisibleForCover: false,
      token: null,
      first_name_edit: '',
      last_name_edit: '',
      bio: '',
      about: '',
      updateArtistData: [],
      photoData: [],
      // cover_photoData:[]
    };
  }

  choosePhotofromGallery = () => {
    this.setState({modalVisible: !this.state.modalVisible});
    ImagePicker.openPicker({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then(response => {
      // console.log(response);
      this.setState({photo: response});
      console.log(this.state.photo);
    });
  };
  choosePhotofromGalleryForCover = () => {
    this.setState({modalVisibleForCover: !this.state.modalVisibleForCover});
    ImagePicker.openPicker({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then(response => {
      // console.log(response);
      this.setState({Coverphoto: response});
      console.log(this.state.Coverphoto, 'ggg');
    });
  };
  choosePhotofromCamera() {
    this.setState({modalVisible: !this.state.modalVisible});

    ImagePicker.openCamera({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then(response => {
      //console.log(response);
      this.setState({photo: response});
      // console.log(this.state.photo);
    });
  }
  uploadPhoto(token) {
    const cover_pic = {
      name: 'Coverphoto.jpg',
      type: 'image/jpg',
      uri: this.state.Coverphoto.path,
    };
    console.log(token, 'jjjj');
    console.log('hereee');
    const formData = new FormData();
    console.log(formData, 'ggg');
    formData.append('avatar', cover_pic);
    formData.append('image_type', 'cover');
    fetch(`${urls.base}${urls.avatar}`, {
      method: 'POST',
      headers: {
        //   'Content-Type': 'multipart/form-data',
        Authorization: `Token ${token}`,
        //  'enctype' : 'multipart/form-data',
      },
      body: formData,
    })
      .then(response => response.json())
      .then(responseData => {
        console.log('Fetch Success================== cover');
        console.log(responseData);
      })

      // .then(response => {
      //   console.log(response, 'response photo creator');
      //   // this.setState({cover_photoData : response})
      //   //  console.log(cover_photoData,'kkkk');

      // })
      .catch(err => {
        console.log(err);
      });
  }
  uploadProfilePhoto(token,image) {
    const Photo_pic = {
      name: 'photo.jpg',
      type: 'image/jpg',
      uri: this.state.photo.path,
    };
    console.log(token, 'jjjj');
    console.log('hereee');
    const formData = new FormData();
    console.log(formData, 'ggg');
    formData.append('avatar', Photo_pic);
    formData.append('image_type', 'profile');
    fetch(`${urls.base}${urls.avatar}`, {
      method: 'POST',
      headers: {
        //   'Content-Type': 'multipart/form-data',
        Authorization: `Token ${token}`,
        //  'enctype' : 'multipart/form-data',
      },
      body: formData,
    })
      .then(response => response.json())
      .then(responseData => {
        console.log('Fetch Success================== profile');
        image.avatar = responseData.link
        console.log(image.avatar,'image');
        console.log(responseData, 'res');
      })

      // .then(response => {
      //   console.log(response, 'response photo creator');
      //   // this.setState({cover_photoData : response})
      //   //  console.log(cover_photoData,'kkkk');

      // })
      .catch(err => {
        console.log(err);
      });
  }
  choosePhotofromCameraForCover() {
    this.setState({modalVisibleForCover: !this.state.modalVisibleForCover});

    ImagePicker.openCamera({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then(response => {
      //console.log(response);
      this.setState({Coverphoto: response});
      // console.log(this.state.photo);
    });
  }
  async userEditDataSave(navigation,image,token) {
    
    

    
    
    
    
    // if(this.state.Coverphoto !== null){
    //   console.log(token,'jjjj');
    //   this.uploadPhoto(token)
    // }
    
    
    const post_id = await image.id;
    // const photo_path_Viewer = await this.state.photo.path;
    
    if(this.state.first_name_edit == '' )
    {
      // name.first_name_edit = image.first_name
      this.setState({first_name_edit: image.first_name})
    }
    if(this.state.last_name_edit == '' )
    { 
      // name.last_name_edit = image.last_name
      this.setState({last_name_edit: image.last_name})
    }
    const name = this.state;
    const response =  await UpdateViewer(name,token,post_id);
    if(response.success == true){
       console.log(this.props.status,'uph');
       console.log(response,'res');
       (image.first_name = response.user.first_name);
       (image.last_name = response.user.last_name);
      //  (this.props.status.user.avatar = response.user.avatar);

      
    }

    if (this.state.photo !== null) {
      
      this.uploadProfilePhoto(token,image)
    }
    if (this.state.Coverphoto !== null) {
      console.log(token, 'jjjj');
      this.uploadPhoto(token);
    }
    navigation.navigate('main');
      console.log('after')
  }
  doClear(fieldname) {
    let textInput = this.refs[fieldname];
    textInput.clear();
  }
  render() {
    const {image, token} = this.props;
    console.log(image, 'image');
    console.log(token,'token')

    const navigation = this.props.navigation;

    return (
      <ScrollView style={{backgroundColor:'white'}}>
      <View style={{flexDirection: 'column',backgroundColor:'white' }}>
        {/* <TouchableOpacity
          onPress={() => this.userEditDataSave(navigation, token)}>
          <View
            style={{
              flexDirection: 'row-reverse',
              paddingLeft: 40,
              paddingTop: 20,
            }}>
            <Text>save</Text>
          </View>
        </TouchableOpacity> */}

        <View style={{height: 250, width: 'auto', marginVertical:0}}>
          {this.state.Coverphoto ? (
            <Image
              source={{uri: this.state.Coverphoto.path}}
              //  source={image.avater}
              style={{
                width: 'auto',
                height: 'auto',
                resizeMode: 'contain',
                // borderRadius: 100,
                aspectRatio: 1,
                borderColor: 'white',
                borderWidth: 5,

                borderRadius: 3,
              }}
            />
          ) : (
            <Image
              source={require('../../../assets/images/shakira.jpeg')}
              //  source={image.avater}
              style={{
                width: 'auto',

                borderRadius: 3,
              }}
            />
          )}
          <View>
            <Icon
              name="camera"
              type="font-awesome"
              size={25}
              onPress={() =>
                this.setState({
                  modalVisibleForCover: !this.state.modalVisibleForCover,
                })
              }
              containerStyle={{
                // backgroundColor: Colors.white,
                position: 'absolute',
                // underlayColor: 'rgba(255,255,255,0)',
                // justifyContent: 'center',
                top: -35,
                right: 15,
                // width: 26,
                // aspectRatio: 1,
                // borderRadius: wp(50),
              }}
            />
          </View>
        </View>

        <View
          style={{
            position: 'absolute',

            height: 10,
            width: 10,

            left: '30%',
            top: '15%',
          }}>
          {this.state.photo ? (
            <Image
              source={{uri: this.state.photo.path}}
              //  source={image.avater}
              style={{
                width: 150,
                height:150,
                borderRadius: 100,
                aspectRatio: 1,
                borderColor: 'white',
                borderWidth: 5,

                // borderRadius: 3,
              }}
            />
          ) : (
            <Image
               source={{uri: `${image.avatar}`}}
              // source={require('../../../assets/images/jenifer.jpeg')}
                // source={image.avater}
              style={{
                width: 150,
                height:150,
                borderRadius: 100,
                // resizeMode: 'cover',
                aspectRatio: 1,
                borderColor: 'white',
                borderWidth: 5,
                // borderRadius: 3,
              }}
            />
          )}
        </View>
        <View>
          <Icon
            name="pencil"
            type="font-awesome"
            size={25}
            onPress={() =>
              this.setState({modalVisible: !this.state.modalVisible})
            }
            containerStyle={{
              // backgroundColor: Colors.white,
              position: 'absolute',
              // underlayColor: 'rgba(255,255,255,0)',
              // justifyContent: 'center',
              top: -40,
              right: 110,
              // width: 26,
              // aspectRatio: 1,
              // borderRadius: wp(50),
            }}
          />
        </View>
        <View style={{flexDirection: 'column',justifyContent:'space-between'}}>
          
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              width: 'auto',
            }}>
            <View
              style={{
                flexDirection: 'row',
                borderWidth: 1,
                borderColor: '#dcdcdc',
                borderRadius: wp(30),
                height: hp('6%'),
                //width: 'auto',
                // width : wp('100%'),
                flex: 0.45,
                minWidth: wp(35),
                top: hp('5%'),
              }}>
              <Text
                style={{
                  position: 'absolute',
                  color: Colors.primary,
                  top: -18,
                  left: 25,
                  padding: 5,
                  zIndex: 100,
                  width: 'auto',
                  backgroundColor: Colors.white,
                  fontSize: 15,
                }}>
                First name *
              </Text>
              <TextInput
                ref="first_name_edit"
                style={{
                  flex: 0.95,
                  borderColor: '#dcdcdc',
                  paddingLeft: wp('3%'),
                  color: Colors.primary,
                  height: 50,
                  // width: wp('40%'),
                  right: wp('10%'),
                  left: wp('4%'),
                }}
                placeholder={image.first_name}
                onChangeText={first_name_edit => {
                  this.setState({first_name_edit: first_name_edit});
                }}
              />
              <Icon
                name="cancel"
                color="#DFDFDF"
                containerStyle={{
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  width: 'auto',
                }}
                onPress={() => this.doClear('first_name_edit')}
              />
            </View>
            <View
              style={{
                flex: 0.45,
                borderWidth: 1,
                borderColor: '#dcdcdc',
                borderRadius: wp(30),
                height: hp('6%'),
                flexDirection: 'row',
                minWidth: wp(35),
                top: hp('5%'),
              }}>
              <Text
                style={{
                  position: 'absolute',
                  color: Colors.primary,
                  top: -18,
                  left: 25,
                  padding: 5,
                  zIndex: 100,
                  width: 'auto',
                  backgroundColor: Colors.white,
                  fontSize: 15,
                }}>
                Last name *
              </Text>
              <TextInput
                ref=""
                style={{
                  flex: 0.95,
                  borderColor: '#dcdcdc',
                  paddingLeft: wp('3%'),
                  color: Colors.primary,
                  height: 50,
                  // width: wp('40%'),
                  right: wp('10%'),
                  left: wp('4%'),
                }}
                placeholder={image.last_name}
                onChangeText={last_name_edit => {
                  this.setState({last_name_edit: last_name_edit});
                }}
              />
              <Icon
                name="cancel"
                color="#DFDFDF"
                containerStyle={{
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  width: 'auto',
                }}
                onPress={() => this.doClear('last_name_edit')}
              />
            </View>
          </View>
          <View style={{height:20}}></View>
          <View
              style={{
                flexDirection: 'row',
                borderWidth: 1,
                borderColor: '#dcdcdc',
                borderRadius: wp(30),
                height: hp('13%'),
                //width: 'auto',
                // width : wp('100%'),
                flex: 0.45,
                minWidth: wp(35),
                top: hp('5%'),
                marginHorizontal:5,
              }}>
              <Text
                style={{
                  position: 'absolute',
                  color: Colors.primary,
                  top: -18,
                  left: 25,
                  padding: 5,
                  zIndex: 100,
                  width: 'auto',
                  backgroundColor: Colors.white,
                  fontSize: 15,
                }}>
                Bio *
              </Text>
              <TextInput
                ref="bio"
                style={{
                  flex: 0.95,
                  borderColor: '#dcdcdc',
                  paddingLeft: wp('3%'),
                  color: Colors.primary,
                  height: 50,
                  // width: wp('40%'),
                  right: wp('10%'),
                  left: wp('4%'),
                }}
                placeholder="bio"
                onChangeText={bio => {
                  this.setState({bio: bio});
                }}
              />
              <Icon
                name="cancel"
                color="#DFDFDF"
                containerStyle={{
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  width: 'auto',
                }}
                onPress={() => this.doClear('bio')}
              />
            </View>

            <View style={{height:20}}></View>
          <View
              style={{
                flexDirection: 'row',
                borderWidth: 1,
                borderColor: '#dcdcdc',
                borderRadius: wp(30),
                height: hp('13%'),
                //width: 'auto',
                // width : wp('100%'),
                flex: 0.45,
                minWidth: wp(35),
                top: hp('5%'),
                marginHorizontal:5,
              }}>
              <Text
                style={{
                  position: 'absolute',
                  color: Colors.primary,
                  top: -18,
                  left: 25,
                  padding: 5,
                  zIndex: 100,
                  width: 'auto',
                  backgroundColor: Colors.white,
                  fontSize: 15,
                }}>
                About *
              </Text>
              <TextInput
                ref="about"
                style={{
                  flex: 0.95,
                  borderColor: '#dcdcdc',
                  paddingLeft: wp('3%'),
                  color: Colors.primary,
                  height: 50,
                  // width: wp('40%'),
                  right: wp('10%'),
                  left: wp('4%'),
                }}
                placeholder="about"
                onChangeText={about => {
                  this.setState({about: about});
                }}
              />
              <Icon
                name="cancel"
                color="#DFDFDF"
                containerStyle={{
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  width: 'auto',
                }}
                onPress={() => this.doClear('about')}
              />
            </View>

            <View style={{height:100}}></View>
          
        </View>
        <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              bottom: '5%',
            }}>
            <TouchableOpacity
              style={{backgroundColor: '#800D1F',
              //borderWidth: 1,
              //borderColor: '#dcdcdc',
              //borderRadius: 65,
              borderWidth: 1,
              borderColor: '#dcdcdc',
              borderRadius: wp(30),
              height: 50,
              width: 350,
              //height: hp('5%'),
              alignItems: 'center',
              justifyContent: 'center',
              
              }}
              onPress={() => this.userEditDataSave(navigation,image,token)}
              >
              <View>
                <Text style={{fontSize: hp(3),
    
    textAlign: 'center',
    color: '#FCECF1'}}>SAVE</Text>
    </View>
             
            </TouchableOpacity>
          </View>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() =>
            this.setState({modalVisible: !this.state.modalVisible})
          }>
          <View
            style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.5)'}}
            onStartShouldSetResponder={() =>
              this.setState({modalVisible: !this.state.modalVisible})
            }>
            <View
              style={{
                flex: 0.3,
                backgroundColor: Colors.white,
                top: '75%',
                padding: '5%',
                borderRadius: 15,
              }}>
              <TouchableOpacity
                style={{flex: 0.25}}
                onPress={() => this.choosePhotofromCamera()}>
                <Text style={{fontSize: 20}}>Take Photo</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{flex: 0.25}}
                onPress={() => this.choosePhotofromGallery()}>
                <Text style={{fontSize: 20}}>Choose Photo from Library</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{alignSelf: 'flex-end', flex: 0.5}}
                onPress={() =>
                  this.setState({modalVisible: !this.state.modalVisible})
                }>
                <Text style={{fontSize: 20, right: '20%', top: '50%'}}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        {/* for cover */}
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisibleForCover}
          onRequestClose={() =>
            this.setState({
              modalVisibleForCover: !this.state.modalVisibleForCover,
            })
          }>
          <View
            style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.5)'}}
            onStartShouldSetResponder={() =>
              this.setState({
                modalVisibleForCover: !this.state.modalVisibleForCover,
              })
            }>
            <View
              style={{
                flex: 0.3,
                backgroundColor: Colors.white,
                top: '75%',
                padding: '5%',
                borderRadius: 15,
              }}>
              <TouchableOpacity
                style={{flex: 0.25}}
                onPress={() => this.choosePhotofromCameraForCover()}>
                <Text style={{fontSize: 20}}>Take Photo</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{flex: 0.25}}
                onPress={() => this.choosePhotofromGalleryForCover()}>
                <Text style={{fontSize: 20}}>Choose Photo from Library</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{alignSelf: 'flex-end', flex: 0.5}}
                onPress={() =>
                  this.setState({
                    modalVisibleForCover: !this.state.modalVisibleForCover,
                  })
                }>
                <Text style={{fontSize: 20, right: '20%', top: '50%'}}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
      </ScrollView>
    );
  }
}
function mapStatetoProps(state) {
  return {
    status: state.user,
    Post_status: state.post,
  };
}
export default connect(mapStatetoProps)(ViewerUpdate, Details);
