import React, { Component } from "react";
import {
  ScrollView,
  View,
  ActivityIndicator,
  Animated,
  TouchableOpacity,
  Image,
  TextInput,
} from "react-native";
import Colors from "../../../Constants/Colors";
import style from "./style";
import { connect } from "react-redux";
import * as Animatable from "react-native-animatable";
import { Icon } from "react-native-elements";
import { getInitialState } from "../../../_actions/LoginAction";
import LoadUserDetails from "../../../_services/UserDetails";
import { POST } from "../../../_services/services";
import {
  // HistoryBanner,
  RecentVideos,
  HistoryActions,
  PlayLists,
} from "../component/HistoryComponent";

class HistoryScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_data: [],
      recentVideo: [],
      notifications: [],
      arrayholder: [],
      loading: false,
      showSearch: false,
      sliderPosition: new Animated.Value(0),
      search: null,
      isCreator: null,
    };
  }

  navigate = (id) => {
    const { navigation } = this.props;
    navigation.navigate("postvideo", { post_id: id });
  };
  async componentDidMount() {
    // user data
    const user_data = await LoadUserDetails(this.props.status.id);
    if (user_data) {
      this.setState({ user_data: user_data });
    }

    const id = this.props.status.id;
    const token = this.props.status.token;
    const body = "";

    // recent videos
    const url = `user/${id}/recentlyViewlist/`;
    const recentVideosList = await POST(url, token, body);
    this.setState({ recentVideo: recentVideosList.data.views_list });

    // notification list
    const url1 = `notification/list/`;
    const apiBody = {
      received_by: id,
      notification_id: 0,
    };

    const notificationList = await POST(url1, token, apiBody);
    console.log(notificationList);
    console.log("apiBody");
    if (notificationList.status === 200) {
      this.setState({
        notifications: [
          ...this.state.notifications,
          ...notificationList.data.notification,
        ],
        arrayholder: notificationList.data.notification,
      });
    }
  }
  searchFilterFunction = (text) => {
    const newData = this.state.arrayholder.filter((item) => {
      const itemData = `${item.notification_text.toUpperCase()}   
      ${item.sent_by_name.toUpperCase()} `;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ notifications: newData });
  };

  slideIn() {
    Animated.spring(this.state.sliderPosition, {
      toValue: { x: 40, y: 0 },
    }).start();
    this.setState({ showSearch: !this.state.showSearch });
  }

  imageLink = () => {
    if (this.state.user_data.avatar !== undefined) {
      if (this.state.user_data.avatar[0] === "/") {
        console.log(this.state.user_data.avatar);
        return `http://111.93.169.90:8008${this.state.user_data.avatar}`;
      } else {
        return `${this.state.user_data.avatar}`;
      }
    }
  };

  render() {
    const { status } = this.props;
    const navigation = this.props.navigation;
    return (
      <>
        <View style={style.banner.mainView}>
          {this.state.showSearch ? (
            <Animatable.View
              animation="slideInRight"
              duration={800}
              style={style.banner.searchView}
            >
              <TextInput
                style={style.banner.searchBarStyle}
                placeholder="Search"
                placeholderTextColor={"#fff"}
                onChangeText={(text) => this.searchFilterFunction(text)}
                onSubmitEditing={() =>
                  this.setState({ showSearch: !this.state.showSearch })
                }
              />
            </Animatable.View>
          ) : null}
          <TouchableOpacity
            style={style.banner.searchIconBtn}
            onPress={() => this.slideIn()}
          >
            <Icon
              name="search1"
              type="antdesign"
              size={20}
              onPress={() =>
                this.setState({ showSearch: !this.state.showSearch })
              }
              iconStyle={style.banner.searchIconStyle}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={style.banner.avatarBtn}
            onPress={() => navigation.navigate("Settings")}
          >
            <Image
              source={{ uri: this.imageLink() }}
              style={style.banner.avatar}
            />

            {/* <Image
            source={
              image
                ? { uri: `${image.avatar}` }
                : require("../../../assets/icons/user_male2-512.png")
            }
            style={style.banner.avatar}
          /> */}
          </TouchableOpacity>
        </View>
        <ScrollView style={style.mainView}>
          {/* <HistoryBanner image={this.state.user_data} navigation={navigation} /> */}
          <RecentVideos
            videos={this.state.recentVideo}
            navigate={this.navigate}
          />
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: Colors.lightGrey,
              height: 5,
            }}
          />
          <HistoryActions notificationData={this.state.notifications} />

          <PlayLists stats={status} navigate={this.navigate} />
        </ScrollView>
      </>
    );
  }
}

function mapStatetoProps(state) {
  return {
    status: state.user,
  };
}

const actionsHistory = {
  getInitialState: getInitialState,
};

export default connect(
  mapStatetoProps,
  actionsHistory
)(HistoryScreen);
