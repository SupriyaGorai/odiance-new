import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  View,
  Icon,
  Left,
  Body,
  Right,
} from "native-base";
import ProfileHeader from "../../Creator/components/ProfileHeader";
import style from "./style";
import { logout } from "../../../_actions/LoginAction";
import { connect } from "react-redux";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from "@react-native-community/google-signin";


class SettingsScreen extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    GoogleSignin.configure({
      scopes: ["https://www.googleapis.com/auth/drive.readonly"], // what API you want to access on behalf of the user, default is email and profile
      webClientId:
        "250802093474-m5cipn2b70u4qop4e165lmgo1g6uom1a.apps.googleusercontent.com", // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      //hostedDomain: '', // specifies a hosted domain restriction
      //loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
      //accountName: '', // [Android] specifies an account name on the device that should be used
      //iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });
  }

  onPressLogout = async () => {
    this.props.navigation.push("Viewer");
    this.props.logout();
    await GoogleSignin.signOut();
  };

  render() {
    const navigation = this.props.navigation;

    return (
      <Container>
        <ProfileHeader header="Settings" navigation={navigation} />

        <Content>
          <List>
            <ListItem button={true}>
              <View style={style.IconView}>
                <Icon type="FontAwesome" name="user" style={style.IconStyle} />
              </View>
              <Body>
                <Text style={style.TextStyle}>Invite People</Text>
              </Body>
            </ListItem>
            <ListItem
              button={true}
              onPress={() => navigation.navigate("profileScreenEdit")}
            >
              <View style={style.IconView}>
                <Icon
                  type="FontAwesome"
                  name="pencil"
                  style={style.IconStyle}
                />
              </View>
              <Body>
                <Text style={style.TextStyle}>Edit Profile</Text>
              </Body>
            </ListItem>

            <ListItem
              button={true}
              onPress={() => navigation.navigate("ChangePassword")}
            >
              <View style={style.IconView}>
                <Icon type="FontAwesome" name="home" style={style.IconStyle} />
              </View>
              <Body>
                <Text style={style.TextStyle}>Change Password</Text>
              </Body>
            </ListItem>

            <ListItem
              button={true}
              onPress={() => navigation.navigate("PushNotification")}
            >
              <View style={style.IconView}>
                <Icon type="FontAwesome" name="bell" style={style.IconStyle} />
              </View>
              <Body>
                <Text style={style.TextStyle}>Push notification</Text>
              </Body>
            </ListItem>

            <ListItem button={true}>
              <View style={style.IconView}>
                <Icon
                  type="FontAwesome"
                  name="arrow-down"
                  style={style.IconStyle}
                />
              </View>
              <Body>
                <Text style={style.TextStyle}>Data saver</Text>
              </Body>
            </ListItem>

            <ListItem
              button={true}
              onPress={() => navigation.navigate("CommunityGuide")}
            >
              <View style={style.IconView}>
                <Icon type="Feather" name="book-open" style={style.IconStyle} />
              </View>
              <Body>
                <Text style={style.TextStyle}>Community guidelines</Text>
              </Body>
            </ListItem>

            <ListItem
              button={true}
              onPress={() => navigation.navigate("PrivacyPolicy")}
            >
              <View style={style.IconView}>
                <Icon type="FontAwesome" name="lock" style={style.IconStyle} />
              </View>
              <Body>
                <Text style={style.TextStyle}>Privacy policy</Text>
              </Body>
            </ListItem>

            <ListItem
              button={true}
              onPress={() => navigation.navigate("CopyrightPolicy")}
            >
              <View style={style.IconView}>
                <Icon
                  type="FontAwesome"
                  name="copyright"
                  style={style.IconStyle}
                />
              </View>
              <Body>
                <Text style={style.TextStyle}>Copyright policy</Text>
              </Body>
            </ListItem>

            <ListItem
              button={true}
              onPress={() => navigation.navigate("ReportProblem")}
            >
              <View style={style.IconView}>
                <Icon type="Octicons" name="smiley" style={style.IconStyle} />
              </View>
              <Body>
                <Text style={style.TextStyle}>Report a problem</Text>
              </Body>
            </ListItem>

            <ListItem button={true} onPress={this.onPressLogout}>
              <View style={style.IconView}>
                <Icon
                  type="FontAwesome"
                  name="power-off"
                  style={style.IconStyle}
                />
              </View>
              <Body>
                <Text style={style.TextStyle}>Logout</Text>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {};
}
const actionCreators = {
  logout: logout,
};

export default connect(
  mapStateToProps,
  actionCreators
)(SettingsScreen);
