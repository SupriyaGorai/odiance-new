import { PixelRatio, Dimensions } from "react-native";
import Colors from "../../../Constants/Colors";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const { height, width } = Dimensions.get("window");

export default {
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  TextStyle: {
    fontSize: 13,
  },

  IconView: {
    borderRadius: 20,
    aspectRatio: 1,
    backgroundColor: "#7f131f",
    width: 25,
    justifyContent: "center",
    alignItems: "center",
  },
  IconStyle: {
    fontSize: 15,
    color: "white",
  },

  boderP: {
    flexDirection: "row",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#dcdcdc",
    borderRadius: wp(30),
    height: hp("6%"),
    width: wp("90%"),
    top: hp("15%"),
  },
  labelContainer: {
    position: "absolute",
    color: Colors.primary,
    top: -15,
    left: 25,
    padding: 5,
    zIndex: 100,
    width: "auto",
    backgroundColor: "#fff",
    fontSize: 15,
  },
  input: {
    flex: 0.95,
    borderColor: "#dcdcdc",
    paddingLeft: wp("5%"),
    color: Colors.primary,
    height: 50,
    // width: wp("90%"),
  },
  icon: {
    alignItems: "center",
    justifyContent: "space-around",
    width: "auto",
  },
  boderNp: {
    flexDirection: "row",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#dcdcdc",
    borderRadius: wp(30),
    height: hp("6%"),
    width: wp("90%"),
    top: hp("21%"),
  },

  boderConp: {
    flexDirection: "row",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#dcdcdc",
    borderRadius: wp(30),
    height: hp("6%"),
    width: wp("90%"),
    top: hp("28%"),
  },
  submitBtn: {
    marginTop: 270,
    margin: 10,
    backgroundColor: "#66001a",
    borderRadius: wp(30),
  },
};
