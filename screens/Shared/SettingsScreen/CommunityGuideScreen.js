import React, { Component } from "react";
import { Dimensions, ScrollView } from "react-native";
import {
  Container,
  Header,
  Text,
  Icon,
  View,
  Left,
  Body,
  Button,
  Title,
} from "native-base";

const devHeight = Dimensions.get("window").height;
const devWidth = Dimensions.get("window").width;

export default class CommunityGuideScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const navigation = this.props.navigation;
    return (
      <Container style={{ flex: 1, backgroundColor: "#f8f8f8" }}>
        <Header
          style={{
            width: devWidth,
            backgroundColor: "#66001a",
            height: 60,
          }}
        >
          <Left>
            <Button onPress={() => navigation.navigate("Settings")} transparent>
              <Icon style={{ color: "#fff" }} name="arrow-back" />
            </Button>
          </Left>

          <Body style={{ left: -10 }}>
            <Title style={{ color: "#fff", fontSize: 17 }}>
              Community Guidelines
            </Title>
          </Body>
        </Header>
        <Container>
          <ScrollView>
            <View style={{ padding: 20 }}>
              <Text
                style={{
                  color: "#333",
                  fontSize: 18,
                  fontWeight: "bold",
                  textAlign: "center",
                  paddingBottom: 10,
                }}
              >
                Community Guidelines
              </Text>
              <Text
                style={{ color: "#333", fontSize: 15, textAlign: "justify" }}
              >
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi
                nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor
                sit amet, consectetur, adipisci velit, sed quia non numquam eius
                modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                voluptatem. Ut enim ad minima veniam, quis nostrum
                exercitationem ullam corporis suscipit laboriosam, nisi ut
                aliquid ex ea commodi consequatur? Quis autem vel eum iure
                reprehenderit qui in ea voluptate velit esse quam nihil
                molestiae consequatur, vel illum qui dolorem eum fugiat quo
                voluptas nulla pariatur?
              </Text>
              <View style={{ padding: 10 }} />
              <Text
                style={{ color: "#333", fontSize: 15, textAlign: "justify" }}
              >
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                quae ab illo inventore veritatis et quasi architecto beatae
                vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                voluptas sit aspernatur aut odit aut fugit, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi
                nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor
                sit amet, consectetur, adipisci velit, sed quia non numquam eius
                modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                voluptatem. Ut enim ad minima veniam, quis nostrum
                exercitationem ullam corporis suscipit laboriosam, nisi ut
                aliquid ex ea commodi consequatur? Quis autem vel eum iure
                reprehenderit qui in ea voluptate velit esse quam nihil
                molestiae consequatur, vel illum qui dolorem eum fugiat quo
                voluptas nulla pariatur?
              </Text>
            </View>
          </ScrollView>
        </Container>
      </Container>
    );
  }
}
