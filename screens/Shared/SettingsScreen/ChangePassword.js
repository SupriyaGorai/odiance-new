import React, { Component } from "react";
import {
  TextInput,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  ToastAndroid,
} from "react-native";
import style from "./style";
import { Icon } from "react-native-elements";
import { Header, Text, View, Left, Body, Button, Title } from "native-base";
import { connect } from "react-redux";
import { POST } from "./../../../_services/services";
import { logout } from "./../.././../_actions/LoginAction";

const devHeight = Dimensions.get("window").height;
const devWidth = Dimensions.get("window").width;

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPass: "",
      newPass: "",
      ConfPass: "",
    };
  }

  doClear = (stateName) => {
    stateName === "currentPass"
      ? this.setState({ currentPass: "" })
      : stateName === "newPass"
      ? this.setState({ newPass: "" })
      : stateName === "ConfPass"
      ? this.setState({ ConfPass: "" })
      : null;
  };

  onSubmit = async () => {
    const CurrentPassword = this.state.currentPass;
    const NewPassword = this.state.newPass;
    const Confirmpassword = this.state.ConfPass;
    if (
      CurrentPassword !== "" &&
      NewPassword !== "" &&
      Confirmpassword !== ""
    ) {
      if (NewPassword === Confirmpassword) {
        if (CurrentPassword === Confirmpassword) {
          ToastAndroid.show(
            "Current password and new password is same",
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM
          );
        } else {
          const token = this.props.status.token;
          const url = "user/changepassword/";
          const body = {
            password: CurrentPassword,
            newPassword: Confirmpassword,
          };

          let response = await POST(url, token, body);
          console.log(response);
          console.log(" change password response");
          if (response.status === 200) {
            ToastAndroid.show(
              response.data.msg,
              ToastAndroid.LONG,
              ToastAndroid.BOTTOM
            );
            this.props.navigation.push("Viewer");
            this.props.logout();
          }
        }
      } else {
        ToastAndroid.show(
          "New password and confirm password is not same",
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM
        );
      }
    } else {
      ToastAndroid.show(
        "Please enter something",
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM
      );
    }
  };

  render() {
    const { navigation } = this.props;
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={style.container}>
          <Header
            style={{
              width: (devWidth * 100) / 100,
              backgroundColor: "#66001a",
              height: 60,
            }}
          >
            <Left>
              <TouchableOpacity onPress={() => navigation.goBack()} transparent>
                <Icon color="#fff" name="arrow-back" />
              </TouchableOpacity>
            </Left>
            <Body style={{ left: 5 }}>
              <Title style={{ color: "#fff", fontSize: 17 }}>
                Change Password
              </Title>
            </Body>
          </Header>
          <View style={style.boderP}>
            <Text style={style.labelContainer}>Current Password *</Text>
            <TextInput
              style={style.input}
              placeholder="ex password"
              value={this.state.currentPass}
              secureTextEntry={true}
              onChangeText={(val) => this.setState({ currentPass: val })}
            />
            <Icon
              name="cancel"
              color="#DFDFDF"
              containerStyle={style.icon}
              onPress={() => this.doClear("currentPass")}
            />
          </View>

          <View style={style.boderNp}>
            <Text style={style.labelContainer}>New Password *</Text>
            <TextInput
              style={style.input}
              placeholder="ex password"
              value={this.state.newPass}
              secureTextEntry={true}
              onChangeText={(val) => this.setState({ newPass: val })}
            />
            <Icon
              name="cancel"
              color="#DFDFDF"
              containerStyle={style.icon}
              onPress={() => this.doClear("newPass")}
            />
          </View>

          <View style={style.boderConp}>
            <Text style={style.labelContainer}>Confirm New Password *</Text>
            <TextInput
              style={style.input}
              placeholder="ex password"
              value={this.state.ConfPass}
              secureTextEntry={true}
              onChangeText={(val) => this.setState({ ConfPass: val })}
            />
            <Icon
              name="cancel"
              color="#DFDFDF"
              containerStyle={style.icon}
              onPress={() => this.doClear("ConfPass")}
            />
          </View>

          <Button onPress={this.onSubmit} style={style.submitBtn} block success>
            <Text>Submit</Text>
          </Button>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

function mapStatetoProps(state) {
  return {
    status: state.user,
    Post_status: state.post,
  };
}

const actionCreators = {
  logout: logout,
};
export default connect(
  mapStatetoProps,
  actionCreators
)(ChangePassword);
