import React, { Component } from 'react';
import { Dimensions,ScrollView ,Image,ToastAndroid} from 'react-native';
import { Container, Header, Text, View, Icon, Left, Body, Button,Title, Content, Textarea, Form  } from 'native-base';
import { connect } from "react-redux";
import ReportProblemFromUser from "../../../_services/ReportProblemFromUser";
const devHeight = Dimensions.get('window').height;
const devWidth = Dimensions.get('window').width;


 class ReportProblemScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ReportProblem:'',
    };
  }
_submit= async () =>{
  if (this.state.ReportProblem === null) {
    ToastAndroid.show(
      "Please enter your problem first ",
      ToastAndroid.SHORT
    );
  }
  else{
    console.log(this.state.ReportProblem);
    const data = {
      content : this.state.ReportProblem
    };
    const jsonData = JSON.stringify(data);
    await ReportProblemFromUser( jsonData,this.props.status.token)
  }

}
  render() {
    console.log(this.props,'re');
    const navigation = this.props.navigation;
    return (
      <Container style={{flex:1,backgroundColor:"#DAE0E2"}}>
      <Header style={{ width: devWidth * 100 / 100, backgroundColor: '#66001a', height: devHeight * 7 / 100,top:3}}>
      <Left>
        <Button 
        onPress={() => navigation.navigate('Settings')}
        transparent>
          <Icon 
          
          style={{color:"#fff"}}
          name='arrow-back' />
        </Button>
      </Left>
      <Body style={{left:45}}>
        <Title style={{color:"#fff",fontSize:17}}>Support</Title>
      </Body>
    </Header>
       <Text style={{color:"#616C6F",top:30,padding:10,marginLeft:5}}>Help us to know where you are facing the issue.Our expert will get back to you.</Text>
        
         <Form  style={{top:30,padding:15,}}>
        <Textarea 
        style={{color:"#616C6F",top:5,borderColor:"#66001a"}}
        rowSpan={5} bordered placeholder="Explain your issues here..." 
        onChangeText={(text) => {
          this.setState({ ReportProblem: text });
        }}
        />
        </Form>

        <Button 
         style={{marginTop:130,margin:10,backgroundColor:"#66001a"}}
         onPress={() => this._submit()}
        block success>
        <Text>Submit</Text>
      </Button>

      </Container>
    );
  }
}
function mapStateToProps(state) {
  return {
    status: state.user,
    
  };
}



export default connect(
  mapStateToProps,
  
)(ReportProblemScreen);
