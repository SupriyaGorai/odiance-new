import React, { Component } from 'react';
import { Dimensions,ScrollView,View } from 'react-native';
import { Container, Header, Text,  Left, Body, Button,Title, Content, ListItem, Switch,Right ,Icon} from 'native-base';
import style from './style';

const devHeight = Dimensions.get('window').height;
const devWidth = Dimensions.get('window').width;



export default class PushNotificationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      on : true,
    };
  }
closeSwitch = () => {
  this.setState({on : !this.state.on})
}
  render() {
    const navigation = this.props.navigation;
    return (
      <Container style={{flex:1,backgroundColor:"#DAE0E2"}}>
      <Header style={{ width: devWidth * 100 / 100, backgroundColor: '#66001a', height: devHeight * 7 / 100,top:3}}>
      <Left>
        <Button 
        style={{right:105}}
        onPress={() => navigation.navigate('Settings')}
        transparent>
          <Icon 
          
          style={{color:"#fff"}}
          name='arrow-back' />
        </Button>
      </Left>
      <Body style={{position:"absolute"}}>
        <Title style={{color:"#fff",fontSize:17}}>Permission</Title>
      </Body>
    </Header>
    <Content style={{top:30}}   >
       <ListItem 
       icon>
       
       <View style={style.IconView}>
       <Icon type="FontAwesome" name="bell" style={style.IconStyle} />
   </View>
            

            <Body style={{left:7}}>
            <Text>Push Notification Mode</Text>
          </Body>
          
          <Right>
              <Switch value={this.state.on} onTouchStart ={()=> this.closeSwitch()} />
            </Right>
       
       </ListItem>
    </Content>
    
    </Container>
    );
  }
}