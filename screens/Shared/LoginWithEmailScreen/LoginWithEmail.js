import React, { Component } from "react";
import { connect } from "react-redux";
import { login } from "../../../_actions/LoginAction";
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Platform,
  StatusBar,
  Keyboard,
  ToastAndroid,
} from "react-native";

import style from "./styles.js";
import { Icon, CheckBox, Header } from "react-native-elements";
import Colors from "../../../Constants/Colors";
import { ActivityIndicator } from "react-native-paper";
import styles from "./styles.js";
import ProfileHeader from "../../Creator/components/ProfileHeader";
import PushNotification from "react-native-push-notification";
import PushNotificationIOS from '@react-native-community/push-notification-ios';
export class LoginWithEmail extends React.Component {
  state = {
    email: null,
    password: null,
    check: false,
    deviceId: "ce7f7efddgaav",
    deviceType: "ios",
  };

  doClear(fieldname) {
    let textInput = this.refs[fieldname];
    textInput.clear();
  }
  onPress() {
    if (this.state.email === null) {
      ToastAndroid.show("Please enter your email", ToastAndroid.SHORT);
    } else {
      if (this.state.password === null) {
        ToastAndroid.show("Please enter your password", ToastAndroid.SHORT);
      } else {
        this.props.login(
          this.state.email,
          this.state.password,
          this.state.deviceId,
          this.state.deviceType
        );
        this.setState({ check: true });
      }
    }
  }
  componentDidMount() {
    var _this = this;
    if(Platform.OS === 'android')
    {
    PushNotification.configure({
      senderID: "105952880220",

      onRegister: function(token) {
        console.log("TOKEN:", token.token);
        if (token) {
          _this.setState({ deviceId: token.token, deviceType: token.os });
          console.log(_this.state.deviceId, _this.state.deviceType);
        }
      },

      onNotification: function(notification) {
        console.log("NOTIFICATION:", notification);
      },
      onAction: function(notification) {
        console.log("ACTION:", notification);
      },
      onRegistrationError: function(err) {
        console.error("err.message", err);
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
  }
  // PushNotificationIOS.addEventListener('register', onRegister);
  // // PushNotificationIOS.addEventListener('notification',onNotification);
  // // PushNotificationIOS.addEventListener('notification', onAction);
  // PushNotificationIOS.addEventListener('registrationError',onRegistrationError);

  // // PushNotification.configure({
  // //   senderID: "105952880220",

  // const onRegister = (register) =>  {
  //     //console.log("TOKEN:", token.token);
  //     console.log(deviceToken);
  //     if (token) {
  //       _this.setState({ deviceId: token.token, deviceType: token.os });
  //       console.log(_this.state.deviceId, _this.state.deviceType);
  //     }
  //   }

  //   // const onNotification = (notification) =>  {
  //   //   console.log("NOTIFICATION:", notification);
  //   // },
  //   // onAction: function(notification) {
  //   //   console.log("ACTION:", notification);
  //   // },
  //   const onRegistrationError = (err) => {
  //     console.error("err.message", err);
  //   }
  //   // popInitialNotification: true,
  //   // requestPermissions: true
  // }
}

  render() {
    const { loggingIn, loggedIn, msg, isCreator } = this.props.status;
    const { navigation } = this.props;
    if (loggedIn) {
      if (isCreator === false) {
        navigation.navigate("Home");
      }
    }

    return (
      <View style={style.mainView}>
        {/* <ProfileHeader header="LoginWithEmail" navigation={navigation} /> */}

        {/* <StatusBar backgroundColor={Colors.primary} barStyle="light-content" /> */}
        {/*<HomeHeader name = {'Sign In'}/>*/}
        <View style={style.imageView}>
          <Image
            source={require("../../../assets/logo-no-border.png")}
            style={style.logoImage}
          />
        </View>
        <View style={style.textInput}>
          <View style={style.input}>
            <Text style={style.labelContainer}> Email Id </Text>
            <TextInput
              ref="textInput"
              style={style.inputF}
              placeholder="ex Jhone"
              placeholderTextColor={Colors.white}
              value={this.state.email}
              onChangeText={(email) => this.setState({ email })} 
            />
            <Icon
              name="cancel"
              color="#DFDFDF"
              containerStyle={style.icon}
              onPress={() => this.doClear("textInput")}
            />
          </View>
          <View style={style.input}>
            <Text style={style.labelContainer}> Password </Text>
            <TextInput
              ref="textInput1"
              style={style.inputF}
              placeholder="ex password"
              placeholderTextColor={Colors.white}
              value={this.state.password}
              onChangeText={(password) => this.setState({ password })}
              secureTextEntry={true}
            />
            <Icon
              name="cancel"
              color="#DFDFDF"
              containerStyle={style.icon}
              onPress={() => this.doClear("textInput1")}
            />
          </View>
        </View>

        <View style={styles.checkText}>
          <View>
            <TouchableOpacity
              style={{ height: 50 }}
              onPress={() => navigation.jumpTo("Forgot")}
            >
              <Text style={styles.text}> Forgot Password ? </Text>
            </TouchableOpacity>
          </View>
          {/* <CheckBox
            right
            title="Remember me"
            uncheckedIcon="square-o"
            checked={!this.state.check}
            onPress={() =>
              this.setState({
                check: !this.state.check,
              })
            }
            checkedColor="#D4D4D4"
            textStyle={style.test}
            containerStyle={style.checkBox}
          /> */}
        </View>
        <View>
          <TouchableOpacity
            style={style.submit}
            disabled={loggingIn}
            onPress={() => {
              {
                Keyboard.dismiss();
              }
              this.onPress();
            }}
          >
            {loggingIn ? (
              <ActivityIndicator color={Colors.white} />
            ) : (
              <Text style={style.submitText}> SIGN IN </Text>
            )}
          </TouchableOpacity>
        </View>
        <View style={{ height: 45 }} />

        <View style={{ flexDirection: "row", alignSelf: "center" }}>
          <Text style={{ color: "#D4D4D4" }}> Not registered yet ? </Text>

          <TouchableOpacity onPress={() => navigation.jumpTo("ViewerRegister")}>
            <Text
              style={{
                color: Colors.primary,
                fontWeight: "bold",
                marginHorizontal: 10,
                fontSize: 16.5,
              }}
            >
              Sign up here
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    status: state.user,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    login: (email, password, deviceId, deviceType) =>
      dispatch(login(email, password, deviceId, deviceType)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginWithEmail);
