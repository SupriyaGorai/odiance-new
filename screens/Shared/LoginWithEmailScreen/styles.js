import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../../Constants/Colors';
import {Platform} from 'react-native'
export default {
  mainView: {
    flex: 1,
  backgroundColor: '#131313',
  justifyContent: 'center',
  },
  imageView :{
    alignItems : 'center',
    flexDirection : 'column',
    justifyContent: 'center',
    flex : 0.3,
},
  logoImage : {
   height : '75%',
   width : wp('30%'),
   alignItems : 'center',
   resizeMode : 'contain',
   justifyContent: 'center',
   marginTop : hp('5%'),
  },
  input : {
    
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: Colors.white,
    borderRadius: wp(10),
    paddingLeft : wp('6%'),
    marginTop : hp('5%'),
    paddingVertical: Platform.OS=='ios' ? 10 : 0,

  },
  textInput : {
   flexDirection : 'column',
   marginHorizontal : wp('10%'),
   
  },
  
  icon : {
    alignItems: 'center',
    justifyContent: 'space-around',
    width: 'auto',
  },
  labelContainer: {
    position: 'absolute',
    color: '#D4D4D4',
    top: -18,
    left: 25,
    padding: 5,
    zIndex: 100,
    width: 'auto',
    backgroundColor: '#131313' ,
    fontSize: 15,
  },
  inputF : {
   width : 'auto',
   flex : 0.95,
   color : Colors.white,
   
  },
  submit: {
    backgroundColor: '#800D1F',
    borderWidth: 1,
    borderColor: '#131313',
    borderRadius: wp(10),
    height: hp('7%'),
    alignItems: 'center',
    justifyContent: 'center',
    top : hp('5%'),
    marginHorizontal : wp('10%'),
  },
  submitText: {
    fontSize: hp(2),
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#FCECF1',
  },
  text : {
  color : '#D4D4D4',
  fontWeight : 'bold',
  marginHorizontal : wp('12%'),
  top : hp('2%'),
  },
  test : {
    color : '#D4D4D4',   

  },
  checkBox : {
    backgroundColor : '#131313',
    borderColor : '#131313',
    right : wp('5%'),
  },
  checkText : {
   flexDirection : 'row',
   justifyContent : 'space-between',
  },


};
