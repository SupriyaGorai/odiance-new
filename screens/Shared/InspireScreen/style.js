import { Dimensions } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
export default {
  inspireScreenView: {
    backgroundColor: '#fafafa',
  },
  inspirecardMainView: {
    flex: 1,
    padding: 5,
    width: '100%',
    // maxHeight: '15%',
  },

  TouchableOpacityView: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    width: '100%',
    height: 'auto',
    flexDirection: 'row',

    backgroundColor: '#ffffff',
    shadowColor: '#000',
    // shadowOffset: {
    //   width: wp(1),
    //   height: hp(2),
    // },
    // shadowOpacity: 0.25,
    elevation: 2,
    borderRadius: wp(1),
  },

  ImageAndDetailsView: {
    flex: 0.7,
    flexDirection: 'row',
    alignItems: 'center'
  },

  imgStyle: {
    width: '100%',
    height: 'auto',
    aspectRatio: 1,
    borderRadius: wp(10),
    borderWidth: wp(0.7),
    borderColor: '#d1d1d1',
  },
  ImageView: {
    width: 65,
    height: 'auto',
  },
  inspirecardDetailsView: {
    flexDirection: 'column',
    marginLeft: 10,
  },
  RemoveDetails: {
    flexDirection: 'row',
    width: 100,
    height: 'auto',
    aspectRatio: 3,
    alignSelf: 'flex-end',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 35,
    backgroundColor: '#80131e',
  },
  nameText: {
    fontWeight: 'bold',
    fontSize: 16,
    textTransform: 'capitalize'
  },
  professionText: {
    fontSize: 13,
    color: '#999',
    textTransform: 'lowercase'
  },
  inspirecardRemoveView: {
    flex: 0.3,
    justifyContent: 'center',
  },
  removebtnText: {
    color: 'white',
    fontSize: 14,
    marginHorizontal: 5,
  },
};
