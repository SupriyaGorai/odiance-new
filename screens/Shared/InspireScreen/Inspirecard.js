import React, { Component } from 'react';
import { Text, View, Image, ScrollView } from 'react-native';
import style from './style';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import urls from '../../../Constants/urls';

export default class InspiresCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text:false,
    }
  }
  pressFloow(id){
    this.props.unfollow(id)
    this.setState({text: !this.state.text})
  }

  render() {
    const { name, Profession, picture, id, navigation,creator_id,item,isCreator } = this.props;
    console.log(name,'in');
    return (
      <View style={style.inspirecardMainView}>
        
        <View>
          <View style={style.TouchableOpacityView}>
            <View style={style.ImageAndDetailsView}>
              <View style={style.ImageView}>
               
                <TouchableOpacity 
                
                 onPress={ isCreator === true ? () =>
                navigation.navigate("PostVideoProfile", {
                  creatorId: creator_id,
                })  : null
              }
                > 
                { picture ? 
                   picture.charAt(0)=== 'h' ? 
                  <Image source={{ uri: picture }} style={style.imgStyle} />
                  : <Image source={require("../../../assets/icons/user_male2-512.png")} style={style.imgStyle} />
               : null }
                </TouchableOpacity>
              </View>

              <View style={style.inspirecardDetailsView}>
                <TouchableOpacity
                 onPress={ isCreator === true ? () =>
                  navigation.navigate("PostVideoProfile", {
                    creatorId: creator_id,
                  })  : null }
                >
                  <Text style={style.nameText}>{name}</Text>
                  <Text style={style.professionText}>@{Profession}</Text>
                </TouchableOpacity>
              </View>
            </View>
            {
              this.props.type === 1 ?
                <View style={style.inspirecardRemoveView}>
                  <TouchableOpacity onPress={ () => this.pressFloow(id)}>
                    <View style={style.RemoveDetails}>
                      {this.state.text ?
                    (
                      <Text style={style.removebtnText}>follow</Text>
                    ) :
                    (
                      <Text style={style.removebtnText}>unfollow</Text>
                    ) 
                    }
            <Text style={style.removebtnText}></Text>
                    </View>
                  </TouchableOpacity>
                </View> : null
            }
          </View>
        </View>
      </View>
    );
  }
}
