import React from 'react';
import { View, ScrollView,TouchableOpacity, Text } from 'react-native';
import InspiresCard from './Inspirecard';
import { connect } from 'react-redux';
import style from './style';
import { POST } from '../../../_services/services';
import urls from '../../../Constants/urls';
import AddFollower from '../../../_services/AddFollowerApi';
import { ActivityIndicator } from 'react-native-paper';


class InspireScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      user: this.props.status,
      listType: '',
      followerList: [],
      followingList: [],
      is_follow: true,
      textt:0,
      loading : false,
    }
  }

  async componentDidMount() {
    console.log(this.state.user);
    this.setState({
      loading : true,
      listType: this.props.route.name
    });
    let res = await POST(`user/${this.state.user.id}/followlist/`, this.state.user.token, "");
    if (res.data.length !== null) {
      this.setState({
        followingList: res.data.following_list,
         followerList: res.data.follower_list,
         loading : false
         })
    }
  }
  reset = (name,index) => {
    const resetAction = CommonActions.reset({
      index: index,
      routes: [{ name: name }],
    });
    this.props.navigation.dispatch(resetAction);
  };

  handleUnfollow =  async(id, key,item) => {
    
    console.log(item,'item');
    const data = {
      follow_to: item.follow_to.id,
      follow_type: "remove",
    };
    const jsonData = JSON.stringify(data);
    var res = await AddFollower(jsonData, this.state.user.token, this.state.user.id);
    console.log(res);
    if(res.success) {
      // this.reset('Inspirations',0)
    
      var arr = this.state.followingList;
      arr.splice(key, 1)
      this.setState({ followingList: arr})
     
        this.setState({ textt: 1})
     
    }
  }
  

  render() {
     console.log(this.state.followingList);
     //console.log(this.props,'followerpro');
    if(this.state.loading === true)
    return <ActivityIndicator/>
    else {
     return (
      <ScrollView  >
        <View Style={style.inspireScreenView}>
          {
            this.state.listType == 'Inspirations' ?
            this.state.followerList.length !== null ? 
              this.state.followerList.map((item, index) => 
                 <InspiresCard
                  key={index}
                  name={item.follow_to.first_name + " " + item.follow_to.last_name}
                  Profession={item.follow_to.username}
                  picture={item.follow_to.avatar}
                  type={1}
                  item ={item}
                  creator_id ={item.follow_to.id}
                  navigation= {this.props.navigation}
                  unfollow = {() => this.handleUnfollow(this.state.user.id, key,item)}
                  isCreator = {item.follow_to.is_creator}
              />
               ) : null
                // </TouchableOpacity>
              :
              this.state.followingList.length !== null ? 
              this.state.followingList.map((item, index) =>  
                 <InspiresCard
                  key={index}
                  name={item.followed_by.first_name + " " + item.followed_by.last_name}
                  creator_id ={item.followed_by.id}
                  navigation= {this.props.navigation}
                  Profession={item.followed_by.username}
                  picture={item.followed_by.avatar}
                  type={2}
                  isCreator = {item.followed_by.is_creator}

                />
                ) : null
               
          }
        </View>
      </ScrollView>
    );
  }
}
}
function mapStatetoProps(state) {
  return {
    status: state.user,
  };
}

export default connect(
  mapStatetoProps
)(InspireScreen);
