import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import urls from "../../../Constants/urls";

export default function NotificationsList(props) {
  return (
    <View style={styles.container}>
      {props.data.sent_by_avatar ?
      props.data.sent_by_avatar.charAt(0) === 'h' 
      ?  
      <Image
        resizeMode="contain"
        source = {{ uri : `${props.data.sent_by_avatar}` }} 
        style={styles.avatar}
      />
      : <Image
      resizeMode="contain"
      source = {{uri : `${urls.avatarbase}${props.data.sent_by_avatar}`}} 
      style={styles.avatar}
    /> 
      : <Image
      resizeMode="contain"
      source={require("../../../assets/icons/user_male2-512.png")}
      style={styles.avatar}
    /> }
      <Text style={styles.notificationText}>
        {props.data.sent_by_name} {props.data.notification_text}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: "#fff",
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#ccc",
    margin: 5,
    padding: 5,
  },
  avatar: {
    height: 30,
    width: 30,
    borderRadius: 50,
  },
  notificationText: {
    marginLeft: 5,
  },
});
