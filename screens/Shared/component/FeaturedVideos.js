import React from "react";
import { View, Text } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { Icon } from "react-native-elements";
import Colors from "../../../Constants/Colors";
import { DynamicHorizontalScroll } from "../../Viewer/components/PostThumbnail";
import loadPosts from "../../../_services/LoadPosts";
import { PostVerticalScroll } from "../../Creator/InspirationScreen/inspirationScreen";
import { string } from "prop-types";
const mainMargin = 5;

export class FeaturedVideos extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { videos, navigate } = this.props;

    return (
      <View style={{ flex: 1, margin: mainMargin, marginTop: 0, top: -10 }}>
        <View
          style={{
            marginLeft: mainMargin,
            flex: 0.2,
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Text
            style={{
              marginLeft: mainMargin,
              marginBottom: mainMargin,
              fontSize: 16,
              fontWeight: "300",
            }}
          >
            Featured Posts
          </Text>
        </View>
        {videos ? (
          <DynamicHorizontalScroll navigate={navigate} content={videos} />
        ) : null}
        <View
          style={{
            marginHorizontal: mainMargin,
            borderBottomWidth: 1,
            borderBottomColor: Colors.lightGrey,
          }}
        />
      </View>
    );
  }
}
export class PivateDraftVideoTab extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "flex-start", padding: 10 }}>
        <Text
          style={{ color: Colors.greyIcon, fontSize: 18, fontWeight: "bold" }}
        >
          The following videos are private and currently hidden
        </Text>
      </View>
    );
  }
}

export class PostCreditTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
    };
  }
  async componentDidMount() {
    console.log((this.props,'ll'));
    const posts = await loadPosts(this.props.route.params.id);

    if (posts) {
      this.setState({ posts: posts });
    } else {
      throw new Error("problem");
    }
  }
  render() {
    const { navigate } = this.props.route.params;
    return <PostVerticalScroll post={this.state.posts} navigate={navigate} />;
  }
}
