import Colors from "../../../Constants/Colors";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { PixelRatio, Dimensions } from "react-native";
const { height, width } = Dimensions.get("window");
const mainMargin = 5;

export default (style = {
  mainView: {
    backgroundColor: Colors.white,
  },
  banner: {
    mainView: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "flex-end",
      backgroundColor: Colors.primary,
      width: "100%",
      height: 60,
    },

    searchView: {
      left: 35,
      width: "80%",
    },
    searchBarStyle: {
      borderColor: Colors.white,
      borderRadius: hp(50),
      paddingLeft: 15,
      paddingRight: 40,
      marginRight: 5,
      padding: 2,
      color: Colors.white,
      borderWidth: 2,
      height: 30,
    },
    searchIconBtn: {
      paddingHorizontal: 3,
    },
    searchIconStyle: {
      color: "#fff",
    },
    avatarBtn: {
      paddingHorizontal: wp(4),
    },
    avatar: {
      width: 30,
      height: 30,
      aspectRatio: 1,
      resizeMode: "cover",
      borderRadius: 30 / 2,
      borderColor: "#d1d1d1",
      borderWidth: 1,
    },
  },
  recentVideos: {
    mainViewContainer: {
      flex: 1,
      margin: mainMargin,
    },
    mainView: {
      marginLeft: mainMargin,
      flexDirection: "row",
      alignItems: "center",
      paddingVertical: 5,
    },
    labelText: {
      marginLeft: mainMargin,
      fontWeight: "600",
    },
    bottomBorder: {
      marginHorizontal: mainMargin,
      borderBottomWidth: 1,
      borderBottomColor: Colors.lightGrey,
    },
  },
  historyAction: {
    mainView: {
      flex: 1,
      margin: mainMargin,
      flexDirection: "column",
    },
    header: {
      marginLeft: mainMargin,
      flex: 0.1,
      flexDirection: "row",
      alignItems: "center",
    },
    labelText: {
      marginLeft: mainMargin,
      fontWeight: "300",
    },
    itemsContainer: {
      height: 250,
    },
    LowerViewforMore: {
      flexDirection: "row",
      backgroundColor: "#f6f6f6",
    },
    SpaceView: {
      flex: 0.83,
    },
    MoreView: {
      flex: 0.17,
      backgroundColor: Colors.primary,
      borderRadius: 10,
      alignItems: "center",
      justifyContent: "center",
      margin: 3,
      marginBottom: 10,
      height: 20,
    },
    MoreTextView: { color: "white", fontSize: 12 },
  },
  Modal: {
    ModalView: {
      height: height,
      paddingHorizontal: wp(10),
      //flexDirection : 'row',
      backgroundColor: "rgba(0,0,0,0.5)",
    },
    mainView: {
      //flex : 0.2,
      height: 170,
      //height : '20%',
      top: "25%",
      //position : 'absolute',
      alignSelf: "center",
      padding: "5%",
      width: "100%",
      backgroundColor: Colors.white,
      borderRadius: 15,
    },
    flexRow: {
      flexDirection: "row-reverse",
      justifyContent: "space-between",
      //position : 'absolute',
      bottom: 10,
    },
    textInputRow: {
      flex: 0.8,
      backgroundColor: "#fff",
      width: "100%",
      paddingLeft: 20,
    },
    emptyView: {
      flex: 0.2,
    },
    labelContainer: {
      color: Colors.primary,
      fontSize: 20,
    },
    inputF: {
      color: Colors.primary,
      borderColor: Colors.primary,
      borderBottomWidth: 1,
      height: 50,
      width: 200,
      //position : 'absolute',
      //backgroundColor : 'red',
    },
    iconImage: {
      height: 22,
      width: 22,
    },
    imageDropdown: {
      elevation: 8,
      backgroundColor: Colors.white,
      height: 10,
      top: "100%",
      height: "100%",
      left: "7%",

      padding: "5%",
      position: "absolute",
    },
    box: {
      height: 50,
      width: 80,
      flexDirection: "row",
      alignItems: "center",
      //backgroundColor : 'red',
    },
    iconsOpacity: {
      width: width / 8,
      height: 30,
      alignItems: "center",
      alignSelf: "center",
    },
    buttonRow: {
      flexDirection: "row",
      position: "absolute",
      top: "130%",
      left: "51%",
      width: "100%",
      //backgroundColor : 'red',
    },
    buttonText: {
      color: Colors.primary,
      fontSize: 17,
    },
  },
  playlists: {
    mainViewContainer: {
      flex: 1,
      margin: mainMargin,
      flexDirection: "column",
    },
    mainView: {
      marginLeft: mainMargin,
      flex: 0.2,
      flexDirection: "row",
      justifyContent: "space-between",
      borderBottomWidth: 1,
      borderBottomColor: Colors.lightGrey,
      height: 30,
    },
    createNewView: {
      flex: 0.5,
      flexDirection: "row",
    },
    createNewText: {
      fontWeight: "300",
      margin: 4,
    },
    rightContainer: {
      flexDirection: "row",
      flex: 0.3,
      alignItems: "center",
    },
    sortView: {
      // flex: 0.35,
      width: "auto",
    },
    sortText: {
      fontWeight: "bold",
    },
    typeView: {
      backgroundColor: Colors.lightGrey,
      borderRadius: 20,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
      height: 20,
      width: "auto",
      padding: 7,
    },
    typeText: {
      fontSize: 12,
      fontWeight: "bold",
    },
  },
  playlistTab: {
    mainView: {
      flex: 1,
      flexDirection: "column",
      margin: mainMargin,
    },
    labelContainer: {
      flexDirection: "row",
      marginBottom: mainMargin,
      borderBottomWidth: 1,
      borderBottomColor: Colors.lightGrey,
    },
    label: {
      borderBottomWidth: 3,
      fontWeight: "500",
      borderBottomColor: Colors.primary,
    },
    labelText: {
      fontSize: 14,
      fontWeight: "500",
    },
    fill: {
      flex: 1,
    },
  },
  historyComponent: {
    HistoryComponentMainView: {
      backgroundColor: "#f6f6f6", //Colors.lightGrey
    },
    ItemsMainView: {
      flex: 1,
      flexDirection: "row",
      padding: mainMargin,
    },
    DescriptionAndIconsView: {
      flex: 1,
      flexDirection: "column",
    },
    DescriptionAnd3dotsIconView: {
      flex: 0.2,
      flexDirection: "row",
      alignItems: "center",
    },
    middlespaceOfDescriptionAnd3dotsIconView: {
      flex: 0.1,
      alignItems: "stretch",
    },
    LowerIconsView: {
      flex: 0.3,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
    },
    LowerIcons1st3View: {
      flex: 0.6,
      flexDirection: "row",
      maxWidth: 170,
      margin: 5,
    },
    SpaceBetweenLoweIcons1st3View: {
      flex: 0.2,
    },
    LowerIconWithContentView: {
      flexDirection: "row",
      alignSelf: "flex-start",
    },
    LowerImageIconStyle: {
      height: 14, //iconSize
      // resizeMode: "contain",
    },

    DescriptionTextStyle: {
      flex: 0.9,
      marginHorizontal: mainMargin,

      fontSize: 11,
    },
    ContentTextStyle: {
      fontSize: 10, //fontSize
    },
    SpaceBetweenLoweIcons1st3AndRemoveIconView: {
      flex: 0.4,
      alignItems: "stretch",
    },
  },
  boder: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "auto",
  },
  inputName: {
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "#dcdcdc",
    borderRadius: wp(30),
    height: hp("6%"),
    //width: 'auto',
    // width : wp('100%'),
    flex: 0.45,
    minWidth: wp(35),
    top: hp("5%"),
  },
  labelContainer: {
    position: "absolute",
    color: Colors.primary,
    top: -18,
    // left: 25,
    padding: 5,
    zIndex: 100,
    width: "auto",
    backgroundColor: Colors.white,
    fontSize: 15,
  },
  inputF: {
    flex: 0.95,
    borderColor: "#dcdcdc",
    paddingLeft: wp("3%"),
    color: Colors.primary,
    height: 50,
    // width: wp('40%'),
    right: wp("10%"),
    left: wp("4%"),
  },
  iconF: {
    alignItems: "center",
    justifyContent: "space-around",
    width: "auto",
    left: wp("0%"),
  },
  boderL: {
    flex: 0.45,
    borderWidth: 1,
    borderColor: "#dcdcdc",
    borderRadius: wp(30),
    height: hp("6%"),
    flexDirection: "row",
    minWidth: wp(35),
    top: hp("5%"),
    //width: wp('40%'),
  },
  inputL: {
    flex: 0.95,
    borderColor: "#dcdcdc",
    paddingLeft: wp(7),
    color: Colors.primary,
    height: 50,
    //width: wp('40%'),
    //left: '50%',
  },
  iconL: {
    alignItems: "center",
    justifyContent: "space-around",
    width: "auto",
    //right : wp('8%')
  },
});
