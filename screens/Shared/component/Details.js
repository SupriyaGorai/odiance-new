import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Icon } from "react-native-elements";
import Colors from "../../../Constants/Colors";

export default class Details extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      photo: null,
      modalVisible: false,
      token: null,
      pic: null,
    };
  }

  render() {
    const { image } = this.props;
    const navigation = this.props.navigation;

    return (
      <View>
        <View style={{ height: 250 }}>
          <Image
            source={{ uri: `${image.cover_pic}` }}
            style={{
              width: "auto",
              resizeMode: "cover",
              height: "100%",
              borderRadius: 3,
              flex: 1,
            }}
          />
        </View>
        <View
          style={{
            position: "absolute",
            backgroundColor: "#0008",
            width: "100%",
            height: 250,
          }}
        >
          <TouchableOpacity
            style={{
              position: "absolute",
              left: 0,
              padding: 10,
              margin: 10,
              backgroundColor: "#00000020",
              borderRadius: 5,
            }}
            onPress={() => this.props.navigation.goBack()}
          >
            <Icon name="arrowleft" type="antdesign" size={18} color={"#fff"} />
          </TouchableOpacity>
          {this.props.inPostProfileScreen ? null : (
            <TouchableOpacity
              style={{
                position: "absolute",
                right: 0,
                padding: 10,
                margin: 10,
                backgroundColor: "#00000020",
                borderRadius: 5,
              }}
            >
              <Icon
                type="ionicon"
                name="ios-settings"
                color="#fff"
                onPress={() => navigation.navigate("Settings")}
                size={30}
              />
            </TouchableOpacity>
          )}
          <View
            style={{
              alignSelf: "center",
              height: 120,
              width: 120,
              top: 50,
              aspectRatio: 1,
              justifyContent: "center",
            }}
          >
            {image.avatar ? 
              image.avatar.charAt(0)=== 'h'
              ? 
              (
              <Image
                source={{ uri: `${image.avatar}` }}
                key={Date.now()}
                style={{
                  height: "100%",
                  width: "100%",
                  alignSelf: "center",
                  borderRadius: wp(100),
                }}
              />
             ) :(
              <Image
                source={{ uri: `${urls.avatarbase}${image.avatar}` }}
                key={Date.now()}
                style={{
                  height: "100%",
                  width: "100%",
                  alignSelf: "center",
                  borderRadius: wp(100),
                }}
              />
             ) 

             : (
              <Image
                //source = {{uri : `${urls.avatarbase}${image.avatar}`}}
                source={require("../../../assets/images/shakira.jpeg")}
                style={{
                  height: "100%",
                  width: "100%",
                  alignSelf: "center",
                  borderRadius: wp(100),
                }}
              />
            )}
          </View>
          <View style={{ flex: 0.3, top: 60, alignItems: "center" }}>
            {image ? (
              <Text style={{ fontSize: 18, color: "#fff", fontWeight: "500" }}>
                {image.first_name} {image.last_name}
              </Text>
            ) : (
              <Text style={{ fontSize: 18, color: "#fff", fontWeight: "500" }}>
                User
              </Text>
            )}

            {image ? (
              <Text style={{ color: "#fff", fontSize: 11 }}>
                {image.alumni}
              </Text>
            ) : null}
            {image ? (
              <Text style={{ color: "#fff", fontSize: 8 }}>{image.bio}</Text>
            ) : null}
          </View>
          <View
            style={{
              flex: 0.2,
              top: -10,
              flexDirection: "row",
              alignSelf: "center",
              alignItems: "center",
            }}
          />
        </View>
        <View
          style={{
            backgroundColor: "#fff",
            flexDirection: "row",
            shadowRadius: 15,
            elevation: 10,
            justifyContent: "space-between",
            alignSelf: "center",
            width: 250,
            borderRadius: wp(50),
            top: -28,
            height: 50,
          }}
        >
          <View
            style={{
              flex: 0.495,
              width: 100,
              alignSelf: "center",
              alignItems: "center",
              marginHorizontal: wp(5),
            }}
          >
            <Text style={{ color: Colors.primary, fontWeight: "bold" }}>
              {this.props.follower}
            </Text>
            <Text>Inspirations</Text>
          </View>
          <View
            style={{
              flex: 0.01,
              backgroundColor: Colors.lightGrey,
              width: 1,
              marginVertical: 7,
            }}
          />
          <View
            style={{
              flex: 0.495,
              width: 100,
              alignSelf: "center",
              alignItems: "center",
              marginHorizontal: wp(5),
            }}
            onStartShouldSetResponder={() =>
              navigation.navigate("Inspirations")
            }
          >
            <Text style={{ color: Colors.primary, fontWeight: "bold" }}>
              {this.props.following}
            </Text>
            <Text>Inspires</Text>
          </View>
        </View>
      </View>
    );
  }
}
