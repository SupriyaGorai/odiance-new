import React from "react";
import { View, TouchableOpacity } from "react-native";
import style from "./../HomeScreen/style";
import { Icon } from "react-native-elements";
import DatePicker from "react-native-datepicker";
import Colors from "../../../Constants/Colors";
import { connect } from "react-redux";
import moment from "moment";
import { HomeActions } from "../../../_actions";

class DaysFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      date2: "",
      day3: "",
      picker: false,
    };
  }

  componentDidMount() {
    let Today = this.convertDate(new Date());
    this.setState({ day3: Today });
  }

  convertDate = (str) => {
    var date = new Date(str);
    var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
    var day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  };

  onChangeDate = (date) => {
    if (date === "Today") {
      this.setState({ day3: this.convertDate(new Date()) });
    } else {
      this.setState({ day3: date });
      this.props.setDate(date);
    }

    // if (date === "Today") {
    //   this.setState({ date: moment(new Date()).format("ddd, MMM D") });
    // } else {
    //   this.setState({ date: moment(date, "ddd, MMM D") });
    // }
  };

  renderPicker() {
    return (
      <DatePicker
        style={{ height: "90%", left: 39, bottom: 2.5 }}
        ref={(picker) => {
          this.datePicker = picker;
        }}
        customStyles={{
          dateInput: {
            borderWidth: 0,
            borderColor: "#dcdcdc",
          },
          dateText: {
            fontSize: 12,
            fontWeight: "bold",
            bottom: 8,
            right: 18,
          },
          placeholderText: {
            fontSize: 12,
            fontWeight: "bold",
            color: "black",
            bottom: 13,
            right: 18,
          },
          dateIcon: { left: "10000%" },
          dateTouchBody: { color: Colors.primary },
        }}
        date={this.state.day3}
        mode="date"
        // format="YYYY-MM-DD"
        maxDate={new Date()}
        confirmBtnText="OK"
        cancelBtnText="Cancel"
        onDateChange={(date) => this.onChangeDate(date)}
        getDateStr={(date) => {
          let Today = this.convertDate(new Date());
          let currentDate = this.convertDate(date);
          const value = Today === currentDate ? "Today" : currentDate;
          return value;
        }}
        // getDateStr={(date) => {
        //   const currentDate = new Date();
        //   const value =
        //     date.getDate() === currentDate.getDate() &&
        //     date.getMonth() === currentDate.getMonth()
        //       ? "Today"
        //       : moment(date).format("ddd, MMM D");
        //   // return value;
        // }}
      />
    );
  }

  minusdate() {
    let date = this.state.day3;
    let subDate = moment(date).subtract(1, "days");
    this.setState({ day3: subDate });
    this.props.setDate(this.convertDate(subDate));

    // let date = this.convertDate();

    // console.log(moment(new Date()).subtract(1, "days"));
    // var dateObj = moment(this.state.date);
    // var dateObj2 = moment(dateObj).subtract(1, "days");
    // let convertedDate = this.convertDate(dateObj2);
    // this.setState({
    //   date: dateObj2,
    //   date2: convertedDate,
    // });
  }

  plusdate() {
    let date = this.state.day3;
    let subDate = moment(date).add(1, "days");
    this.setState({ day3: subDate });
    this.props.setDate(this.convertDate(subDate));

    // var dateObj = moment(this.state.date);
    // var dateObj2 = moment(dateObj).add(1, "days");

    // let convertedDate = this.convertDate(dateObj2);
    // this.setState({
    //   date: dateObj2,
    //   date2: convertedDate,
    // });
  }
  render() {
    return (
      <View style={style.DaysFilterView}>
        <View>
          <TouchableOpacity
            // style={{alignItems: 'center', right: 10}}
            onPress={() => this.minusdate()}
          >
            <Icon
              name="caret-left"
              type="font-awesome"
              color="#80131e"
              size={25}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            borderRadius: 10,
            borderWidth: 2,
            width: 75,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {this.renderPicker()}
        </View>

        <View>
          <TouchableOpacity
            // style={style.RightArrow}
            // disabled={
            //   moment(this.state.date).format("ddd, MMM D") ===
            //   moment(new Date()).format("ddd, MMM D")
            // }
            disabled={
              this.convertDate(this.state.day3) === this.convertDate(new Date())
            }
            onPress={() => this.plusdate()}
          >
            <Icon
              name="caret-right"
              type="font-awesome"
              color="#80131e"
              size={25}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    selectedDate: state.search.date,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setDate: (val) => dispatch(HomeActions.dateValue(val)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DaysFilter);
