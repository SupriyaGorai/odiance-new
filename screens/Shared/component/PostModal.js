import React, { useState } from "react";
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  FlatList,
} from "react-native";
import { Icon } from "react-native-elements";

const PostModal = (props) => {
  const onPressThumb = (id) => {
    props.toggleModal();

    props.navigate(id);
  };

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={props.handleModal}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            {/* close btn */}
            <TouchableHighlight
              style={styles.closeButton}
              onPress={props.toggleModal}
            >
              <Icon
                name="closecircleo"
                type="antdesign"
                size={25}
                color={"#66001a"}
              />
            </TouchableHighlight>

            {/* list */}
            <FlatList
              contentContainerStyle={styles.listStyle}
              numColumns={3}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              data={props.content}
              renderItem={({ item, index }) => (
                <TouchableOpacity
                  key={item.post[0].id}
                  style={styles.thumbnailBtn}
                  onPress={() => onPressThumb(item.post[0].id)}
                >
                  <Image
                    style={styles.thumbnailImage}
                    source={{ uri: String(item.post[0].thumbnail) }}
                  />
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#00000050",
  },
  modalView: {
    backgroundColor: "white",
    alignItems: "center",
    height: "75%",
    width: "85%",
    paddingVertical: 10,
  },

  thumbnailBtn: {
    height: 80,
    width: 80,
    margin: 8,
  },
  thumbnailImage: {
    height: 80,
    width: 80,
  },
  closeButton: {
    position: "absolute",
    right: -10,
    top: -10,
    backgroundColor: "#fff",
    borderRadius: 30,
    padding: 5,
  },
});

export default PostModal;
