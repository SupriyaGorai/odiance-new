import React from "react";
import {
  Text,
  View,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from "react-native";
import MapboxGL from "@react-native-mapbox-gl/maps";
import { Thumbnail } from "native-base";
import exampleIcon from "./../../../assets/icons/marker2.png";

const styles = {
  icon: {
    iconImage: exampleIcon,
    iconAllowOverlap: true,
    iconSize: 0.5,
  },
};

class CustomMarker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onSourceLayerPress() {
    this.props.navigation.jumpTo("LoginScreen");
    // alert("You pressed a layer here are your features:");
  }

  render() {
    const { coordinate, navigation } = this.props;
    // console.log("coordinate");
    return (
      <MapboxGL.ShapeSource
        id="symbolLocationSource"
        hitbox={{ width: 20, height: 20 }}
        onPress={this.onSourceLayerPress}
        shape={coordinate}
      >
        <MapboxGL.SymbolLayer
          id="symbolLocationSymbols"
          minZoomLevel={5}
          style={styles.icon}
        />
      </MapboxGL.ShapeSource>

      // <MapboxGL.PointAnnotation
      //   key={id}
      //   id={id}
      //   title="Test"
      //   coordinate={coordinate}
      // >
      //   <TouchableOpacity onPress={() => alert("clicked")}>
      //     <Text style={{ lineHeight: 58 }}>
      //       <Image
      //         source={require("./../../../assets/icons/marker3.png")}
      //         style={{
      //           width: 35,
      //           height: 35,
      //         }}
      //         onPress={() => alert("clicked")}
      //       />
      //     </Text>
      //   </TouchableOpacity>

      //   <MapboxGL.Callout
      //     title="Go to Profile"
      //   >
      //     <TouchableOpacity onPress={() => alert("clicked")}>
      //       <Text style={{ color: "#fff" }}>Hiii</Text>
      //     </TouchableOpacity>
      //   </MapboxGL.Callout>
      // </MapboxGL.PointAnnotation>
    );
  }
}

export default CustomMarker;
