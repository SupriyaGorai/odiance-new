import React from "react";
import { View, Text, TextInput, Slider, Keyboard } from "react-native";
import style from "./../HomeScreen/style";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { Animated } from "react-native";
import { HomeActions } from "../../../_actions";
import Datepicker from "./DatePicker";

class SearchComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distance: 50,
      minDistance: 50,
      maxDistance: 10000,
      searchtext: "",
      keyboardOpen : false,
      // search= true,
    };
    this.numFormatter = this.numFormatter.bind(this);
  }

  numFormatter = (num) => {
    return Math.abs(num) > 999999
      ? Math.sign(num) * (Math.abs(num) / 1000000).toFixed(1) + "M"
      : Math.abs(num) > 999
      ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + "k"
      : Math.sign(num) * Math.abs(num);
  };

  handleClick = () => {
    this.props.toggleSearch();
    Keyboard.dismiss();
  };

  onSliderChange = (value) => {
    this.setState({ distance: value });
    this.props.setSliderValue(value);
  };
  async componentDidMount()
  {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }
  _keyboardDidShow = () => {
    this.setState({keyboardOpen : true})
    }
    _keyboardDidHide = () => {
      this.setState({keyboardOpen : false})
    }
    componentWillUnmount () {
      this.keyboardDidShowListener.remove();
      this.keyboardDidHideListener.remove();
    }
  render() {
    const { enabled, navigation, status, slide_Out, position } = this.props;
    if (enabled) {
      this.props.slide_in();
    } else {
      this.props.slide_Out();
    }
    
    return (
      <View>
        <Animated.View
          style={this.state.keyboardOpen === true ? [
           {... style.SearchComponentView, bottom : 250},
            {
              transform: [
                {
                  translateY: position,
                },
              ],
            },
          ] : 
          [
            style.SearchComponentView,
            {
              transform: [
                {
                  translateY: position,
                },
              ],
            },
          ] }
        >
          <View style={{ position: "absolute", right: 15, top: 40 }}>
            {status.isCreator ? (
              <Icon
                name="cross"
                type="entypo"
                color={"#fdfdfd"}
                size={30}
                onPress={() => this.handleClick()}
              />
            ) : null}
          </View>

          <View
            style={{
              flexDirection: "column",
              position: "absolute",
              bottom: 30,
              padding: 10,
              left: 12,
              backgroundColor: "#fdfdfd",
              maxWidth: 400,
              width: "95%",
              height: 100,
              borderRadius: 20,
            }}
          >
            <View style={style.SearchTextInputView}>
              <TextInput
                placeholder="   ex: name, video, etc"
                style={style.SearchTextInput}
                onChangeText={(val) => this.props.setSearchText(val)}
              />
              <View style={style.searchIconView}>
                <Icon
                  name="search"
                  type="font-awesome"
                  color="#7f131e"
                  size={15}
                  onPress={() => this.props.searchPress()}
                />
              </View>
            </View>
            <View style={style.ShowRangeView}>
              <Text style={style.ShowRangeText}> Show Range </Text>

              <Text style={style.ResultDistance}>
                {this.numFormatter(this.state.distance)}
              </Text>
            </View>

            <View style={style.SliderAndDayFilterView}>
              <View style={{ flex: 0.6, top: -7 }}>
                <Slider
                  style={style.Slider}
                  step={1}
                  // left={30}
                  minimumValue={this.state.minDistance}
                  maximumValue={this.state.maxDistance}
                  value={this.state.distance}
                  onValueChange={(val) => this.onSliderChange(val)}
                  thumbTintColor="#80131e"
                  maximumTrackTintColor="#d3d3d3"
                  minimumTrackTintColor="#80131e"
                />
              </View>
              <View style={{ flex: 0.15 }} />
              <View style={{ flex: 0.25 }}>
                {/* <DaysFilter /> */}
                <Datepicker />
              </View>
            </View>
          </View>
        </Animated.View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    enabled: state.search.enabled,
    selectedDate: state.search.date,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    toggleSearch: () => dispatch(HomeActions.toggle()),
    setDate: (val) => dispatch(HomeActions.dateValue(val)),
    setSearchText: (val) => dispatch(HomeActions.searchText(val)),
    setSliderValue: (val) => dispatch(HomeActions.sliderValue(val)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchComponent);
