import React, { Component } from "react";
import {
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Animated,
  Modal,
} from "react-native";
import { Icon } from "react-native-elements";
import { Avatar } from "react-native-paper";
import Colors from "../../../Constants/Colors";
import { PostHorizontalScroll } from "../../Viewer/components/PostThumbnail";
import style from "../component/style";
import ModalDropdown from "react-native-modal-dropdown";
import * as Animatable from "react-native-animatable";
import PostModal from "./PostModal";
import { POST } from "./../../../_services/services";
import GetThePlayList from "../../../_services/GetThePlayList";
import get_the_video_list_of_a_specific_play_list from "../../../_services/get_the_video_list_of_a_specific_play_list";
import CreateMyNewPlaylistName from "../../../_services/CreateMyNewPlaylistName";
import NotificationList from "./../component/NotificationsList";

const iconSize = 14;

export class HistoryBanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      showSearch: false,
      sliderPosition: new Animated.Value(0),
      search: null,
      isCreator: null,
    };
  }
  slideIn() {
    Animated.spring(this.state.sliderPosition, {
      toValue: { x: 40, y: 0 },
    }).start();
    this.setState({ showSearch: !this.state.showSearch });
  }

  imageLink = () => {
    if (this.props.image.avatar !== undefined) {
      if (this.props.image.avatar[0] === "/") {
        console.log(this.props.image.avatar);
        return `http://111.93.169.90:8008${this.props.image.avatar}`;
      } else {
        return `${this.props.image.avatar}`;
      }
    }
  };

  render() {
    const { image } = this.props;
    const navigation = this.props.navigation;

    return (
      <View style={style.banner.mainView}>
        {this.state.showSearch ? (
          <Animatable.View
            animation="slideInRight"
            duration={800}
            style={style.banner.searchView}
          >
            <TextInput
              style={style.banner.searchBarStyle}
              placeholder="Search"
              placeholderTextColor={"#fff"}
              onChangeText={(text) => this.setState({ search: text })}
              onSubmitEditing={() =>
                this.setState({ showSearch: !this.state.showSearch })
              }
            />
          </Animatable.View>
        ) : null}
        <TouchableOpacity
          style={style.banner.searchIconBtn}
          onPress={() => this.slideIn()}
        >
          <Icon
            name="search1"
            type="antdesign"
            size={20}
            onPress={() =>
              this.setState({ showSearch: !this.state.showSearch })
            }
            iconStyle={style.banner.searchIconStyle}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={style.banner.avatarBtn}
          onPress={() => navigation.navigate("Settings")}
        >
          <Image
            source={{ uri: this.imageLink() }}
            style={style.banner.avatar}
          />

          {/* <Image
            source={
              image
                ? { uri: `${image.avatar}` }
                : require("../../../assets/icons/user_male2-512.png")
            }
            style={style.banner.avatar}
          /> */}
        </TouchableOpacity>
      </View>
    );
  }
}
export class RecentVideos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModal: false,
    };
  }

  onModalPress = () => {
    this.setState({
      isModal: !this.state.isModal,
    });
  };

  render() {
    const { navigate, videos } = this.props;

    return (
      <View style={style.recentVideos.mainViewContainer}>
        <View style={style.recentVideos.mainView}>
          <Icon name="videocam" size={iconSize} color={Colors.greyIcon} />
          <Text style={style.recentVideos.labelText}>Recent Videos</Text>
        </View>
        <PostHorizontalScroll
          toggleModal={this.onModalPress}
          content={videos}
          navigate={navigate}
        />
        {/* <DynamicHorizontalScroll navigate={navigate} content={videos} /> */}
        <View style={style.recentVideos.bottomBorder} />
        <PostModal
          toggleModal={this.onModalPress}
          handleModal={this.state.isModal}
          content={videos}
          navigate={navigate}
        />
      </View>
    );
  }
}
export class HistoryActions extends React.Component {
  render() {
    const historyItems = [
      {
        thumb: require("../../../assets/images/shakira.jpeg"),
        description: "Lorren Ipsum is dyummy text of printing",
        views: "2.5k",
        shares: "10",
        notCool: "10",
        cools: "100",
      },
      {
        thumb: require("../../../assets/images/Dwayne.jpg"),
        description: "Lorren Ipsum is dyummy text of printing",
        views: "2.5k",
        shares: "10",
        notCool: "10",
        cools: "100",
      },
      {
        thumb: require("../../../assets/images/bhai.png"),
        description: "Lorren Ipsum is dyummy text of printing",
        views: "2.5k",
        shares: "10",
        notCool: "10",
        cools: "100",
      },
      {
        thumb: require("../../../assets/images/shakira.jpeg"),
        description: "Lorren Ipsum is dyummy text of printing",
        views: "2.5k",
        shares: "10",
        notCool: "10",
        cools: "100",
      },
    ];
    return (
      <View style={style.historyAction.mainView}>
        <View style={style.historyAction.header}>
          <Icon name="access-time" size={iconSize} color={Colors.greyIcon} />
          <Text style={style.historyAction.labelText}>Notifications</Text>
        </View>
        <View>
          <ScrollView
            style={style.historyAction.itemsContainer}
            nestedScrollEnabled={true}
          >
            {this.props.notificationData.length !== 0 ? (
              this.props.notificationData.map((item, index) => {
                return <NotificationList data={item} key={index} />;
              })
            ) : (
              <View
                style={{
                  height: 250,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text style={{ color: "#979797" }}>No notifications</Text>
              </View>
            )}

            {/* {historyItems.map((data, key) => {
              return <HistoryComponent key={key} content={data} />;
              
            })} */}
            <View style={style.historyAction.LowerViewforMore}>
              <View style={style.historyAction.SpaceView} />
              {/* <View style={style.historyAction.MoreView}>
                <Text style={style.historyAction.MoreTextView}> more ...</Text>
              </View> */}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
export class PlayLists extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
     
      dropDownoptions: ["recently added", "A-Z", "Z-A"],
      modalVisible: false, //
      showImage: false,
      newPlaylist: "",
      focused: false,
      iconSelected: this.icons,
      index: 0,
      playlists: [
        { id: 1, name: "Inspirations" },
        { id: 2, name: "Likes" },
        { id: 3, name: "Favorite" },
      ],
      videoList: [],
      inspiration_list: [],
      like_list: [],
      favourite_list: [],
      NewCreatedPlayList : [],
    };
  }

  async componentDidMount() {
    const id = this.props.stats.id;
    const token = this.props.stats.token;
    const body = "";
    const thePlaylist = await GetThePlayList(token);

    // const url1 = `user/${id}/recentlyViewlist/`;
    // const recentVideosList = await POST(url1, token, body);
    // this.setState({ videoList: recentVideosList.data.views_list });

    // inspiration list
    const url2 = `user/${id}/inspirationlist/`;
    const inspVideosList = await POST(url2, token, body);
    this.setState({
      videoList: inspVideosList.data.inspiration_list,
      inspiration_list: inspVideosList.data.inspiration_list,
    });

    // like videos
    const url3 = `user/${id}/likelist/`;
    const likedVideosList = await POST(url3, token, body);
    console.log(likedVideosList);
    this.setState({ like_list: likedVideosList.data.like_list });

    // favourite videos
    const url4 = `user/${id}/favouritelist/`;
    const favVideosList = await POST(url4, token, body);
    this.setState({ favourite_list: favVideosList.data.favourite_list });
   
    this.setState({ NewCreatedPlayList: thePlaylist.full_list });
   
    for(let i = 0; i < this.state.NewCreatedPlayList.length ; i++){
      this.state.playlists.push(
        {id : this.state.NewCreatedPlayList[i].id , name : this.state.NewCreatedPlayList[i].name }
      )

    }

    console.log(this.state.NewCreatedPlayList,'newpl');

  }
  callGetVideo = async (id, token) =>{
    console.log(id,'iddd');
    const GetVideo = await get_the_video_list_of_a_specific_play_list(id, token)
      this.setState({ videoList: GetVideo.full_list })
  }

  icons = [
    { id: 1, name: require("../../../assets/icons/theater-svg(3).png") },
    { id: 2, name: require("../../../assets/icons/music-player-svg(2).png") },
    {
      id: 3,
      name: require("../../../assets/icons/couple-dancing-flamenco-svg(2).png"),
    },
  ];
  switchPlaylist = async (name, id, token) => {
    this.setState({ playlist: name });

    name === "Inspirations"
      ? this.setState({ videoList: this.state.inspiration_list })
      : name === "Likes"
      ? this.setState({ videoList: this.state.like_list })
      : name === "Favorite"
      ? this.setState({ videoList: this.state.favourite_list })
      // : id > 3
      // ? this.callGetVideo(id, token)
      :null
  };

  sortOnAsc = (property) => {
    return function(a, b) {
      if (a[property] < b[property]) {
        return -1;
      } else if (a[property] > b[property]) {
        return 1;
      } else {
        return 0;
      }
    };
  };
  sortOnDsc = (property) => {
    return function(a, b) {
      if (a[property] < b[property]) {
        return 1;
      } else if (a[property] > b[property]) {
        return -1;
      } else {
        return 0;
      }
    };
  };

  onChangeDropdown = (val) => {
    let objArray = this.state.playlists;

    if (val == 1) {
      objArray.sort(this.sortOnAsc("name"));
      this.setState({
        dropDownValue: String(this.state.dropDownoptions[val]),
        playlists: objArray,
      });
    } else if (val == 2) {
      objArray.sort(this.sortOnDsc("name"));
      this.setState({
        dropDownValue: String(this.state.dropDownoptions[val]),
        playlists: objArray,
      });
    }
  };
  newPlaylistFunction = async () =>{
    this.setState({
                        modalVisible: !this.state.modalVisible,
                        showImage: false,
                        index: 0,
                        focused: false,
                      })
                      const data = {
                        name : this.state.newPlaylist
                      };
                      const jsonData = JSON.stringify(data);
     await CreateMyNewPlaylistName(jsonData,this.props.stats.token)

  }

  render() {
    console.log(this.props,'com');
    const { navigate, posts, inspList, likeList, favList, videos } = this.props;
    // console.log(videos);
    // console.log("this.state.videoList");

    return (
      <View style={style.playlists.mainViewContainer}>
        <View style={style.playlists.mainView}>
          <TouchableOpacity
            style={style.playlists.createNewView}
            onPress={
              () => this.setState({ modalVisible: !this.state.modalVisible }) //
            }
          >
            <Icon
              name="pluscircle"
              type="antdesign"
              size={iconSize}
              color={"black"}
              containerStyle={{ alignSelf: "center" }}
            />
            <Text style={style.playlists.createNewText}>Create New</Text>
          </TouchableOpacity>

          <Modal
            animationType="slide"
            dismissable={true}
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() =>
              this.setState({
                modalVisible: !this.state.modalVisible,
                showImage: false,
                index: 0,
                focused: false,
              })
            }
          >
            <View
              style={style.Modal.ModalView}
              onStartShouldSetResponder={() =>
                this.setState({
                  modalVisible: !this.state.modalVisible,
                  showImage: false,
                  index: 0,
                  focused: false,
                })
              }
            >
              <View style={style.Modal.mainView}>
                <Text style={style.Modal.labelContainer}>New Playlist</Text>
                <View style={style.Modal.emptyView} />
                <View style={style.Modal.flexRow}>
                  <TextInput
                    style={style.Modal.inputF}
                    placeholder="Title"
                    placeholderTextColor={Colors.primary}
                    onChangeText={(text) => this.setState({ newPlaylist: text })}
                    onFocus={() => {
                      this.setState({ showImage: false, focused: true });
                    }}
                  />

                  <TouchableOpacity
                    onPress={() =>
                      this.setState({ showImage: !this.state.showImage })
                    }
                    style={
                      this.state.showImage
                        ? { ...style.Modal.box, borderBottomWidth: 0 }
                        : style.Modal.box
                    }
                  >
                    <Image
                      style={{ height: 20, width: 20, left: 15 }}
                      source={this.state.iconSelected[this.state.index].name}
                    />
                    <Icon
                      name="caret-down"
                      type="font-awesome"
                      size={14}
                      iconStyle={{ alignSelf: "center", left: 20 }}
                    />
                  </TouchableOpacity>
                </View>
                {this.state.showImage ? (
                  <ScrollView style={style.Modal.imageDropdown}>
                    <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                      <TouchableOpacity
                        style={style.Modal.iconsOpacity}
                        onPress={() =>
                          this.setState({ index: 0, showImage: false })
                        }
                      >
                        <Image
                          source={require("../../../assets/icons/theater-svg(3).png")}
                          style={style.Modal.iconImage}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={style.Modal.iconsOpacity}
                        onPress={() =>
                          this.setState({ index: 1, showImage: false })
                        }
                      >
                        <Image
                          source={require("../../../assets/icons/music-player-svg(2).png")}
                          style={style.Modal.iconImage}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={style.Modal.iconsOpacity}
                        onPress={() =>
                          this.setState({ index: 2, showImage: false })
                        }
                      >
                        <Image
                          source={require("../../../assets/icons/couple-dancing-flamenco-svg(2).png")}
                          style={style.Modal.iconImage}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={style.Modal.iconsOpacity}
                        onPress={() =>
                          this.setState({ index: 0, showImage: false })
                        }
                      >
                        <Image
                          source={require("../../../assets/icons/theater-svg(3).png")}
                          style={style.Modal.iconImage}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={style.Modal.iconsOpacity}
                        onPress={() =>
                          this.setState({ index: 1, showImage: false })
                        }
                      >
                        <Image
                          source={require("../../../assets/icons/music-player-svg(2).png")}
                          style={style.Modal.iconImage}
                        />
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={style.Modal.iconsOpacity}
                        onPress={() =>
                          this.setState({ index: 2, showImage: false })
                        }
                      >
                        <Image
                          source={require("../../../assets/icons/couple-dancing-flamenco-svg(2).png")}
                          style={style.Modal.iconImage}
                        />
                      </TouchableOpacity>
                    </View>
                  </ScrollView>
                ) : null}
                <View style={style.Modal.buttonRow}>
                  <TouchableOpacity
                    onPress={() =>
                      this.newPlaylistFunction()
                      
                    }
                  >
                    <Text style={style.Modal.buttonText}>Create</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ left: 20 }}
                    onPress={() =>
                      this.setState({
                        modalVisible: !this.state.modalVisible,
                        showImage: false,
                        index: 0,
                        focused: false,
                      })
                    }
                  >
                    <Text style={style.Modal.buttonText}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <View style={style.playlists.sortView}>
              <Text style={style.playlists.sortText}>Sort </Text>
            </View>

            <ModalDropdown
              options={this.state.dropDownoptions}
              defaultValue={this.state.dropDownoptions[1]}
              dropdownStyle={{ height: 100 }}
              onSelect={(value) => this.onChangeDropdown(value)}
              textStyle={{ fontWeight: "bold", textAlign: "center" }}
            >
              <View style={style.playlists.typeView}>
                <Text>{this.state.dropDownValue}</Text>
                <Icon
                  name="caretdown"
                  type="antdesign"
                  // color="#80131e"
                  size={7}
                  margin={5}
                />
              </View>
            </ModalDropdown>
          </View>
        </View>
        {this.state.playlists.map((data) => {
          return (
            <PlayListTab
            token = {this.props.stats.token}
              navigate={navigate}
              key={data.id}
              expanded={data.name === this.state.playlist}
              switchPlaylist={this.switchPlaylist}
              content={data}
              posts={this.state.videoList}
            />
          );
        })}
      </View>
    );
  }
}
export class PlayListTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalOpen: false,
    };
  }

  onPressMore = () => {
    this.setState({
      isModalOpen: !this.state.isModalOpen,
    });
  };

  render() {
    const { content, switchPlaylist, navigate, posts,token } = this.props;
    console.log(this.props,'content');
    const { expanded } = this.props;

    return (
      <View style={style.playlistTab.mainView}>
        <View style={expanded && style.playlistTab.labelContainer}>
          <TouchableOpacity
            onPress={() => {
              switchPlaylist(content.name,content.id, token);
            }}
            style={expanded && style.playlistTab.label}
          >
            <Text style={style.playlistTab.labelText}>{content.name}</Text>
          </TouchableOpacity>
          <View style={style.playlistTab.fill} />
        </View>
        {expanded && (
          <PostHorizontalScroll
            navigate={navigate}
            // content={this.getData(content.name, content.id)}
            content={posts}
            toggleModal={this.onPressMore}
          />
        )}
        <PostModal
          toggleModal={this.onPressMore}
          handleModal={this.state.isModalOpen}
          content={posts}
          navigate={navigate}
        />
      </View>
    );
  }
}
// export class HistoryComponent extends React.Component {
//   render() {
//     const { content } = this.props;
//     return (
//       <View style={style.historyComponent.HistoryComponentMainView}>
//         <View style={style.historyComponent.ItemsMainView}>
//           <Avatar.Image style={{}} source={content.thumb} />

//           <View style={style.historyComponent.DescriptionAndIconsView}>
//             <View style={style.historyComponent.DescriptionAnd3dotsIconView}>
//               <Text style={style.historyComponent.DescriptionTextStyle}>
//                 {content.description}
//               </Text>
//               <View
//                 style={
//                   style.historyComponent
//                     .middlespaceOfDescriptionAnd3dotsIconView
//                 }
//               />

//               <Icon name="more-horiz" color={Colors.primary} />
//             </View>
//             <View style={style.historyComponent.LowerIconsView}>
//               <View style={style.historyComponent.LowerIcons1st3View}>
//                 <View style={style.historyComponent.LowerIconWithContentView}>
//                   <Icon
//                     name="thumbs-up"
//                     type="font-awesome"
//                     size={15}
//                     //source={require('../../../assets/icons/eye.png')}
//                     iconStyle={style.historyComponent.LowerImageIconStyle}
//                     color="#8F9092"
//                   />
//                   <Text style={style.historyComponent.ContentTextStyle}>
//                     {" "}
//                     {content.views}
//                   </Text>
//                 </View>
//                 <View
//                   style={style.historyComponent.SpaceBetweenLoweIcons1st3View}
//                 />

//                 <View style={style.historyComponent.LowerIconWithContentView}>
//                   <Icon
//                     name="thumbs-down"
//                     type="font-awesome"
//                     size={15}
//                     //source={require('../../../assets/icons/not-cool.png')}
//                     iconStyle={style.historyComponent.LowerImageIconStyle}
//                     color="#8F9092"
//                   />
//                   <Text style={style.historyComponent.ContentTextStyle}>
//                     {content.cools}
//                   </Text>
//                 </View>
//                 <View
//                   style={style.historyComponent.SpaceBetweenLoweIcons1st3View}
//                 />
//                 <View style={style.historyComponent.LowerIconWithContentView}>
//                   <Image
//                     source={require("../../../assets/icons/history_mid.png")}
//                     style={style.historyComponent.LowerImageIconStyle}
//                   />
//                   <Text style={style.historyComponent.ContentTextStyle}>
//                     {content.notCool}
//                   </Text>
//                 </View>
//                 <View
//                   style={style.historyComponent.SpaceBetweenLoweIcons1st3View}
//                 />
//                 <View style={style.historyComponent.LowerIconWithContentView}>
//                   <Icon
//                     name="share-alt"
//                     type="font-awesome"
//                     size={15}
//                     color="#8F9092"
//                     iconStyle={style.historyComponent.LowerImageIconStyle}
//                   />
//                   <Text style={style.historyComponent.ContentTextStyle}>
//                     {" "}
//                     {content.shares}
//                   </Text>
//                 </View>
//               </View>

//               <View
//                 style={
//                   style.historyComponent
//                     .SpaceBetweenLoweIcons1st3AndRemoveIconView
//                 }
//               />
//               <Image
//                 source={require("../../../assets/icons/remove2.png")}
//                 style={style.historyComponent.LowerImageIconStyle}
//               />
//             </View>
//           </View>
//         </View>
//       </View>
//     );
//   }
// }
