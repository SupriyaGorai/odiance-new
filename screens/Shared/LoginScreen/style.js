import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
export default {
  mainView: {
   flex:1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  view2: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  img: {
    maxWidth: 300,
    maxHeight: 70,
    width: wp(70),
    height: hp(20),
    marginVertical: hp(5),
    marginHorizontal: wp(10),
    resizeMode : 'contain'
  },
  text: {
    //  fontWeight: 'bold',
    fontSize: 15,
    left:-20
    //  marginHorizontal: wp(4),
   
  },
  text1: {
    fontWeight: 'bold',
    fontSize: 15,
    marginHorizontal: wp(3),
    color: 'white',
  },
  text2: {
    // fontWeight: 'bold',
     left:-20,
    fontSize: 15,
    // marginHorizontal: wp(4),
    color: 'white',
  },

  googleBtn: {
    maxWidth: 270,
    maxHeight: 55,
    backgroundColor: '#f4f4f4',
    height: hp(13),
    color: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: wp(7),
    width: wp(75),
    marginVertical: hp(0.5),
  },
  facebookBtn1: {
    maxWidth: 270,
    maxHeight: 55,
    backgroundColor: '#385392',
    height: hp(13),
    color: '#fff',
    justifyContent: 'center',
     alignItems: 'center',
    borderRadius: wp(7),
    width: wp(75),
    marginVertical: hp(0.5),
    
  },
 
  emailBtn2: {
    maxWidth: 270,
    maxHeight: 55,
    backgroundColor: '#c8231b',
    height: hp(13),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: wp(7),
    width: wp(75),
    marginVertical: hp(0.5),
  },
  googleframe: {
    left:-25,
    // right: 5,
    borderRadius: 20,
    padding: 10,
    shadowColor: "#000",
    elevation: 2,
    backgroundColor: '#f8fdf9',
    borderWidth: 2,
    borderColor: '#d9e3ea',
    aspectRatio: 1,
  },
  facebookframe: {
    position : "absolute",
     left: 20,
     top:10,
     borderRadius: 20,
    
    shadowColor: "#000",
    elevation: 10,
    backgroundColor: '#f8fdf9',
    width:40,
    height:40,
    justifyContent:'center',
    
    borderWidth: 2,
    borderColor: '#d9e3ea',
  },
  emailframe: {
    left:-30,
    borderRadius: 20,
    padding: 10,
    shadowColor: "#000",
    elevation: 2,
    backgroundColor: '#f8fdf9',
    borderWidth: 2,
    borderColor: '#d9e3ea',
  },
  imageCircle: {
    resizeMode: 'cover',
  }

};
