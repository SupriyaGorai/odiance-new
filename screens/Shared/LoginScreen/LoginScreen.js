import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  ToastAndroid,
} from "react-native";
import { StoreUser } from "../../../_storage/Storage";
import AsyncStorage from "@react-native-community/async-storage";

// import { LoginButton, AccessToken } from 'react-native-fbsdk';
import PropTypes from "prop-types";
var { FBLogin, FBLoginManager } = require("react-native-facebook-login");
import googleLogin from "../../../_services/googleLoginServices";
import PushNotification from "react-native-push-notification";


import FaceBookLogin from "../../../_services/faceBookLoginServices";
import ProfileHeader from "../../Creator/components/ProfileHeader";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from "@react-native-community/google-signin";
import { connect } from "react-redux";

import style from "./style";

import { Icon } from "react-native-elements";

export class LoginScreen extends React.Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     info: [],
  //     deviceId: null,
  //     deviceType:null,
  //   };
  // }
  state = {
    deviceId: null,
    deviceType: null,
  };
  componentDidMount() {
    var _this = this;
    PushNotification.configure({
      senderID: "105952880220",

      onRegister: function(token) {
        console.log("TOKEN:", token.token);
        if (token) {
           _this.setState({ deviceId: token.token, deviceType: token.os });
          
           console.log(_this.state.deviceId, _this.state.deviceType);
        }
      },

      onNotification: function(notification) {
        console.log("NOTIFICATION:", notification);
      },
      onAction: function(notification) {
        console.log("ACTION:", notification);
      },
      onRegistrationError: function(err) {
        console.error("err.message", err);
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
    GoogleSignin.configure({
      scopes: ["https://www.googleapis.com/auth/drive.readonly"], // what API you want to access on behalf of the user, default is email and profile
      webClientId:
        "250802093474-m5cipn2b70u4qop4e165lmgo1g6uom1a.apps.googleusercontent.com", // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      //hostedDomain: '', // specifies a hosted domain restriction
      //loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
      //accountName: '', // [Android] specifies an account name on the device that should be used
      //iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });
   
  }
  StoreToken = async (token) => {
    try {
      console.log(token);
      const stored = await AsyncStorage.setItem("token", token);
    } catch (error) {
      console.log("failed to store token", error.message);
    }
  };
  async removeItemValue(key) {
    try {
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch(exception) {
        return false;
    }
}

  async signIn() {
    console.log("jjjj");
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log(userInfo, "info");
    
      const googleLoginInfo = await googleLogin(userInfo,this.state.deviceId,this.state.deviceType);
      console.log(googleLoginInfo, "hello");
      console.log(this.props.status, "hhhhhh");
      if (googleLoginInfo.success == true) {
        ToastAndroid.show("Your Google email_id is registered & you logged in successfully", 0.5);
        await StoreToken(googleLoginInfo.token);
        await StoreUser(googleLoginInfo.user);
        this.props.status.loggedIn = true;
        this.props.status.token = googleLoginInfo.token;
        this.props.status.id = googleLoginInfo.user.id;
        this.props.navigation.push("Viewer");
      } else {
        ToastAndroid.show("getting error", 0.5);
        await GoogleSignin.signOut();
        await removeItemValue(googleLoginInfo.email)
      }

      // this.setState({ userInfo });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
      console.log(error);
    }
  }
  async forFaceBookSignIn(data) {
    const FBres = await FaceBookLogin(data.profile);
    console.log(FBres, "fb regis");
    console.log(this.props.status, "status");
    if (FBres.data.success == true) {
      ToastAndroid.show(FBres.data.msg, 0.5);
      await StoreToken(FBres.data.token);
      await StoreUser(FBres.data.user);
      this.props.status.id = FBres.data.user.id;
      this.props.status.loggedIn = true;
      this.props.status.token = FBres.data.token;
      this.props.navigation.push("Viewer");
    } else {
      ToastAndroid.show("getting error", 0.5);
    }
  }

  render() {
    
    
    const { status,search } = this.props;
    console.log(search);
    // if(!status.loggedIn && !search)
    // {
    //   this.props.navigation.navigate('Home')
    // }
    var _this = this;
    const { navigation } = this.props;
    return (
      <ScrollView style={{ flex: 1, backgroundColor: "black" }}>
      <ProfileHeader header="Login" navigation={navigation} />
       <View style={style.mainView}>
          <Image
            source={require("../../../assets/logo-no-border-odiance.png")}
            style={style.img}
          />

          {/* <GoogleSigninButton
            style={{ width: 192, height: 48 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={() => this.signIn()}
             /> */}
          <TouchableOpacity
            style={style.googleBtn}
            onPress={() => this.signIn()}
          >
            <View style={style.view2}>
              <View style={style.googleframe}>
                <Image
                  source={require("../../../assets/icons/googleLogo.png")}
                  style={{ width: 20, height: 21 , }}
                />
              </View>
              <Text style={style.text}> Sign in with Google </Text>
            </View>
          </TouchableOpacity>

         
          <View>
            <View style={style.facebookframe}>
              <Icon
                name="facebook-f"
                type="font-awesome"
                color="#1d4eb2"
                size={17}
              />
            </View>
            <FBLogin
              style={style.facebookBtn1}
              ref={(fbLogin) => {
                this.fbLogin = fbLogin;
              }}
              permissions={["email", "user_friends"]}
              loginBehavior={FBLoginManager.LoginBehaviors.Native}
              onLogin={function(data) {
                console.log("Logged in!");
                console.log(data, "fb info");
                _this.forFaceBookSignIn(data);

                _this.setState({ user: data.credentials });
              }}
              onLogout={function() {
                console.log("Logged out.");
                _this.setState({ user: null });
              }}
              onLoginFound={function(data) {
                console.log("Existing login found.");
                console.log(data);
                _this.forFaceBookSignIn(data);

                _this.setState({ user: data.credentials });
              }}
              onLoginNotFound={function() {
                console.log("No user logged in.");
                _this.setState({ user: null });
              }}
              onError={function(data) {
                console.log("ERROR");
                console.log(data);
              }}
              onCancel={function() {
                console.log("User cancelled.");
              }}
              onPermissionsMissing={function(data) {
                console.log("Check permissions!");
                console.log(data);
              }}
            />
          </View>

         

          <TouchableOpacity
            style={style.emailBtn2}
            onPress={() => navigation.jumpTo("EmailLogin")}
          >
            <View style={style.view2}>
              <View style={style.emailframe}>
                <Icon
                  name="envelope"
                  type="font-awesome"
                  color="#80131e"
                  size={15}
                />
              </View>
              <Text style={style.text2}> Sign in with Email </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}
function mapStatetoProps(state) {
  return {
    status: state.user,
    search: state.search.enabled,
  };
}
const mapDispatchToProps = (dispatch) => {
  return {
    // dispatching plain actions
    //increment: () => dispatch({ type: 'INCREMENT' }),
    ViewerRegisterGoogle: (userInfo) =>
      dispatch(ViewerRegisterGoogle(userInfo)),
  };
};
// const actionCreatorsgoogle = {
//   ViewerRegisterGoogle: ViewerRegisterGoogle,
// };
export default connect(
  mapStatetoProps,
  // actionCreatorsgoogle,
  mapDispatchToProps
)(LoginScreen);
