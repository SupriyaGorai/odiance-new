import React from "react";
import {
  View,
  Text,
  PermissionsAndroid,
  Keyboard,
  ToastAndroid,
  ActivityIndicator,
  Modal,
  Image,
  TouchableOpacity
} from "react-native";
import MapboxGL from "@react-native-mapbox-gl/maps";
import { CommonActions, NavigationAction } from "@react-navigation/native";
import Config from "../../../config";
import style from "./style";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { Animated } from "react-native";
import { HomeActions } from "../../../_actions";
// import CustomMarker from "./../component/CustomMarker";
import exampleIcon from "./../../../assets/icons/marker2.png";
import SearchComponent from "./../component/SearchComponent";
import { POST2 } from "./../../../_services/services";
import Geolocation from "@react-native-community/geolocation";
import PushNotification from "react-native-push-notification";
import GetPostDetails from "../../../_services/GetPostDetails";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
// import { TouchableOpacity } from "react-native-gesture-handler";
MapboxGL.setAccessToken(Config.accessToken);

const styles = {
  icon: {
    iconImage: exampleIcon,
    iconAllowOverlap: true,
    iconSize: 0.5,
  },
};

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      screen: "Home",
      sliderPosition: new Animated.Value(200),
      latitude: 0,
      longitude: 0,
      timestamp: null,
      coords: [],
      isLoading: true,
      viewHide: false,
      modalVisible: false,
      creatorDetaits : [],
      post_id:'',
      creator_id: '',
    };
  }

  reset = (name) => {
    const resetAction = CommonActions.reset({
      index: 0,
      routes: [{ name: name }],
    });
    this.props.navigation.dispatch(resetAction);
  };

  slideIn = () => {
    this.props.navigation.closeDrawer();
    Animated.spring(this.state.sliderPosition, {
      toValue: 0,
    }).start();
  };

  slideOut = () => {
    Animated.spring(this.state.sliderPosition, {
      toValue: 200,
      tension: 2,
      velocity: 0.01,
    }).start();
  };

  onUserLocationUpdate = (location) => {
    this.setState({
      timestamp: location.timestamp,
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
      zoom: 12,
    });
    //console.log('homescreen: ',location.coords.latitude,location.coords.longitude);
  };

  async componentDidMount() {
    try {
      await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        ],
        {
          title: "Odiance needs location permission.",
          message: "Please allow Odiance to read your current location.",
        }
      );

      Geolocation.getCurrentPosition((info) => {
        this.setState(
          {
            latitude: info.coords.latitude,
            longitude: info.coords.longitude,
          },
          async () => {
            const url = "user/video_search/";

            const body = {
              search_value: "",
              latitude: this.state.latitude,
              longitude: this.state.longitude,
              radius: "0",
              date_value: "",
            };
            let feature = [];

            let response = await POST2(url, body);

            if (response.data.list.length !== 0) {
              response.data.list.map((item, i) => {
                let coord = [];
                coord.push(item.post_long);
                coord.push(item.post_lat);

                let searchData = {
                  type: "Feature",
                  id: item.id,
                  creator_id: item.creator_id,
                  geometry: {
                    type: "Point",
                    coordinates: coord,
                  },
                };

                feature.push(searchData);
                console.log(searchData, "searchData home screen");
              });

              this.setState({ coords: feature, isLoading: false });
            } else {
              this.setState({ isLoading: false });
              ToastAndroid.show("No data available", ToastAndroid.SHORT);
            }
          }
        );
      });
    } catch (e) {
      console.log(e);
    }
  }

  navigate(id) {
    this.props.navigation.navigate("postVideoNav", {
      screen: "postVideo",
      params: { post_id: id },
    });
  }

  openDrawer() {
    this.slideOut();
    this.props.navigation.openDrawer();
  }
 
  onMarkerPress = async (id, creatorId) => {
    const creatorDetaits = await GetPostDetails(creatorId,id);
    console.log(creatorDetaits,'commm');
     this.setState({ creatorDetaits: creatorDetaits[0].creator[0] });
     this.setState({ modalVisible: !this.state.modalVisible });
     this.setState({ post_id: id });
     this.setState({ creator_id:  creatorId});
    // console.log(id,'idd');
    // this.props.navigation.navigate("postVideoNav",

    // { screen: 'postVideo',params:{post_id:id  ,creator_id: creatorId,} }
    // );
  };

  convertDate = (str) => {
    var date = new Date(str);
    var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
    var day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  };

  onSearchPress = async () => {
    this.setState({ isLoading: true, coords: [] });
    const url = "user/video_search/";
    let dateValue = this.props.selectedDate;
    if (dateValue === "") {
      const date = new Date();
      dateValue = this.convertDate(date);
    }

    const body = {
      search_value: this.props.searchedText,
      latitude: this.state.latitude,
      longitude: this.state.longitude,
      radius: this.props.selectedrange,
      date_value: dateValue,
    };

    let feature = [];

    let response = await POST2(url, body);

    if (response.data.list.length !== 0) {
      response.data.list.map((item, i) => {
        let coord = [];
        coord.push(item.post_long);
        coord.push(item.post_lat);

        let searchData = {
          type: "Feature",
          id: item.id,
          creator_id: item.creator_id,
          geometry: {
            type: "Point",
            coordinates: coord,
          },
        };
        feature.push(searchData);
      });

      this.setState({ coords: feature, isLoading: false });
    } else {
      this.setState({ isLoading: false });
      ToastAndroid.show("No data available", ToastAndroid.SHORT);
    }
  };
  gotoVideoScreen= ()=>{
    console.log('jkkk');
    this.props.navigation.navigate("postVideoNav",

    { screen: 'postVideo',params:{post_id:this.state.post_id ,creator_id: this.state.creator_id,} }
    )
    this.setState({ modalVisible: !this.state.modalVisible });
  }

  render() {
    
    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#00000050",
          }}
        >
          <ActivityIndicator color="#fff" size="large" />
        </View>
      );
    } else {
      const { enabled, navigation, status, selectedDate } = this.props;

      if (enabled) {
        this.slideIn();
      } else {
        this.slideOut();
      }
      console.log(this.state.creatorDetaits,'ccccc');
      return (
        // <Backhandle onBack={this.onBack}>
        <View style={{ flex: 1 }}>
           <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() =>
                  this.setState({
                    modalVisible: !this.state.modalVisible,
                    showImage: false,
                    index: 0,
                    focused: false,
                  })
                }
                presentationStyle="overFullScreen"
              >
                <View
                  style={{ flex: 1, backgroundColor: "rgba(0,0,0,0.5)" }}
                  onStartShouldSetResponder={() =>
                    this.setState({
                      modalVisible: !this.state.modalVisible,
                      showImage: false,
                      index: 0,
                      focused: false,
                    })
                  }
                >
                  <View
                  style={{position: "absolute",top:hp(35),left:wp(35),justifyContent:'center'}}>
                    <TouchableOpacity
                    onPress={() =>this.gotoVideoScreen()}>
                  <Image
        style={{width:100,height:100 ,aspectRatio: 1,
          borderRadius: 50,
          borderWidth: 5,
          borderColor: "white",
        margin:5}}
        source={
          
          this.state.creatorDetaits === null
                      ? require("../../../assets/images/avatar.png")
                      : { uri: this.state.creatorDetaits.avatar }
        }
         
      />
      </TouchableOpacity>
      <View style={{flexDirection:'row',}}>
        <Text style={{color:'white'}}>{this.state.creatorDetaits.first_name}</Text>
        <View style={{width:7}}></View>
        <Text style={{color:'white'}}>{this.state.creatorDetaits.last_name}</Text>
      </View>
                  </View>
                  

                </View>
              </Modal>
                     
          <MapboxGL.MapView
            styleURL={MapboxGL.StyleURL.Dark}
            style={{ flex: 1 }}
          >
            
            <MapboxGL.Camera
              zoomLevel={6}
              animationMode={"flyTo"}
              animationDuration={8000}
              centerCoordinate={[this.state.longitude, this.state.latitude]}
              onStartShouldSetResponder={() => Keyboard.dismiss()}
            />
            <MapboxGL.UserLocation
              ref={(d) => (this._location = d)}
              visible={true}
              onUpdate={this.onUserLocationUpdate}
            />

            

            {/* markers */}
            {this.state.coords.length !== 0
            
              ? this.state.coords.map((item, index) => (
                  <MapboxGL.ShapeSource
                    key={index}
                    id="symbolLocationSource"
                    hitbox={{ width: 20, height: 20 }}
                    onPress={() => this.onMarkerPress(item.id, item.creator_id)}
                    shape={item}
                  >
                    

                    <MapboxGL.SymbolLayer
                      id="symbolLocationSymbols"
                      minZoomLevel={5}
                      style={styles.icon}
                    />
                  </MapboxGL.ShapeSource>
                ))
              : null}
            {/* {this.state.coords.map((item, index) => (
          <CustomMarker
            key={index}
            coordinate={this.state.coords}
            navigation={navigation}
          />
          ))} */}
          </MapboxGL.MapView>

          <SearchComponent
            position={this.state.sliderPosition}
            slide_in={this.slideIn}
            slide_Out={this.slideOut}
            status={status}
            dateVal={selectedDate}
            lat={this.state.latitude}
            long={this.state.longitude}
            searchPress={this.onSearchPress}
          />

          {status.isCreator && (
            <Icon
              name="search1"
              type="antdesign"
              size={35}
              color={"white"}
              containerStyle={{ position: "absolute", bottom: 35, right: 20 }}
              onPress={() => this.props.toggleSearch()}
            />
          )}
          {!status.isCreator && (
            <Icon
              name="menu"
              onPress={() => navigation.openDrawer()}
              color="white"
              containerStyle={style.hamburgerMenuFloating}
            />
          )}
          {/* {
          <SearchComponent
            position={this.state.sliderPosition}
            slide_in={this.slideIn}
            slide_Out={this.slideOut}
            status={status}
            dateVal={selectedDate}
            lat={this.state.latitude}
            long={this.state.longitude}
            searchPressed={this.searchPressed}
          />
        } */}
          {!status.isCreator && (
            <Icon
              name="menu"
              onPress={() => this.openDrawer()}
              color="white"
              underlayColor="rgba(255,255,255,0)"
              containerStyle={style.hamburgerMenuFloating}
            />
          )}
          {/* {status ? (
          <SearchComponent
            position={this.state.sliderPosition}
            slide_in={this.slideIn}
            slide_Out={this.slideOut}
            status={status}
            dateVal={selectedDate}
            lat={this.state.latitude}
            long={this.state.longitude}
            searchPressed={this.searchPressed}
          />
        ) : null} */}
        </View>
        // </Backhandle>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    enabled: state.search.enabled,
    status: state.user,
    selectedDate: state.search.date,
    searchedText: state.search.searchText,
    selectedrange: state.search.sliderValue,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    toggleSearch: () => dispatch(HomeActions.toggle()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);
