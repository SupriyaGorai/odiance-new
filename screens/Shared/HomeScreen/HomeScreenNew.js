import React from "react";
import {
  View,
  Text,
  PermissionsAndroid,
  StyleSheet,
  Dimensions,
  Keyboard,
  ToastAndroid,
  ActivityIndicator,
  Modal,
  Image,
  Platform
} from "react-native";
import { CommonActions, NavigationAction } from "@react-navigation/native";
import style from "./style";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import { Animated } from "react-native"; 
import { HomeActions } from "../../../_actions";
import exampleIcon from "./../../../assets/icons/marker2.png";
import SearchComponent from "./../component/SearchComponent";
import { POST2 } from "./../../../_services/services";
import Geolocation from "@react-native-community/geolocation";
import GetPostDetails from "../../../_services/GetPostDetails";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
// import MapView from "react-native-map-clustering";
import MapView,{ Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { NavigationActions, StackActions } from '@react-navigation/native';
const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
           flex : 1
    },
    map: {
      ...StyleSheet.absoluteFillObject,
      flex : 1,
      height : '100%',
      width : '100%',
      backgroundColor : '#0000'
    },
    hamburgerMenuFloating: {
      position: "absolute",
      top: 10,
      left: 5,
      height: 50,
      width: 50,
      zIndex: 1000,
    },
   });
  const mapStyle = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#212121"
        }
      ]
    },
    {
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#212121"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "administrative.country",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#9e9e9e"
        }
      ]
    },
    {
      "featureType": "administrative.land_parcel",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "administrative.locality",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#bdbdbd"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#181818"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#1b1b1b"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#2c2c2c"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#8a8a8a"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#373737"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#3c3c3c"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#4e4e4e"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#616161"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#757575"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#000000"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#3d3d3d"
        }
      ]
    }
  ]   


class HomeScreenNew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude : null,
      averagelat : 0,
      averagelong : 0,
      longitude : null,
      searchData : null,
      isLoading : false,
      markers : null,
      sliderPosition: new Animated.Value(200),
      keyboardOpen : false,


    };
  }

  getGeolocation = ()=>{
    Geolocation.getCurrentPosition(
      async position => {
        this.setState(
          {  isLoading : true,
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          },
          async () => {
            const url = "user/video_search/";

            const body = {
              search_value: "",
              latitude: this.state.latitude,
              longitude: this.state.longitude,
              radius: "0",
              date_value: "",
            };

            let response = await POST2(url, body);
            console.log(response,'from api')

            if (response.data.list.length !== 0) {
              this.setState({searchData : response.data.list})
              let coords = [];
              let avgLat = 0;
                let avgLong = 0; 
              this.state.searchData.map((item,i) =>
              { 
                const obj = {
                latitude : item.post_lat,
                longitude : item.post_long,
                thumbnail  : item.thumbnail,
                id : item.id,
                creator_id : item.creator_id,
              }
              avgLat = avgLat + item.post_lat;
              avgLong = avgLong + item.post_long; 
              
               coords.push(obj)
              })
              console.log(response.data.list.length);
              this.setState({markers : coords});
              this.setState({averagelat : avgLat/(response.data.list.length) });
              this.setState({averagelong : avgLong/(response.data.list.length) })       
              this.setState({isLoading: false });
            }
          }
        )
      },
      error => {
        console.log(error.code, error.message);
      },
      {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
    );
  }

  defaultGeolocation = ()=>{
      this.setState(
        {  isLoading : true,
          latitude: '-33.8952763',
          longitude: '151.2722256',
        },
        async () => {
          const url = "user/video_search/";

          const body = {
            search_value: "",
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            radius: "0",
            date_value: "",
          };

          let response = await POST2(url, body);
          console.log(response,'from api')

          if (response.data.list.length !== 0) {
            this.setState({searchData : response.data.list})
            let coords = [];
            let avgLat = 0;
              let avgLong = 0; 
            this.state.searchData.map((item,i) =>
            { 
              const obj = {
              latitude : item.post_lat,
              longitude : item.post_long,
              thumbnail  : item.thumbnail,
              id : item.id,
              creator_id : item.creator_id,
            }
            avgLat = avgLat + item.post_lat;
            avgLong = avgLong + item.post_long; 
            
             coords.push(obj)
            })
            console.log(response.data.list.length);
            this.setState({markers : coords});
            this.setState({averagelat : avgLat/(response.data.list.length) });
            this.setState({averagelong : avgLong/(response.data.list.length) })       
            this.setState({isLoading: false });
          }
        }
      )

  }

  async componentDidMount() {
    try {

      if(Platform.OS == 'ios'){
        Geolocation.requestAuthorization();
        const res = await check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
        if (res == 'granted') {
          this.getGeolocation()
        }else if (res == 'denied') {
          const res2 = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
          if (res2 == 'granted') {
            this.getGeolocation()
          } else {
           this.defaultGeolocation()
          }
        } else if (res == 'unavailable') {
          this.defaultGeolocation()
        } else {
          this.defaultGeolocation()
        }

      }else{
      await PermissionsAndroid.requestMultiple(
        [
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        ],
        {
          title: "Odiance needs location permission.",
          message: "Please allow Odiance to read your current location.",
        }
      );
        this.getGeolocation()
    }}
    catch (e) {
      console.log(e);
    }
    }
  slideIn = () => {
    Animated.spring(this.state.sliderPosition, {
      toValue: 0,
    }).start();
  };
  slideOut = () => {
    const height =  Dimensions.get('window').height;
    Animated.spring(this.state.sliderPosition, {
      toValue: hp(92),
      tension: 2,
      velocity: 0.01,
    }).start();
  };
  convertDate = (str) => {
    var date = new Date(str);
    var mnth = ("0" + (date.getMonth() + 1)).slice(-2);
    var day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  };

  onSearchPress = async () => {
    this.setState({ isLoading: true});
    const url = "user/video_search/";
    let dateValue = this.props.selectedDate;
    if (dateValue === "") {
      const date = new Date();
      dateValue = this.convertDate(date);
    }

    const body = {
      search_value: this.props.searchedText,
      latitude: this.state.latitude,
      longitude: this.state.longitude,
      radius: this.props.selectedrange,
      date_value: dateValue,
    };
    let response = await POST2(url, body);

    if (response.data.list.length !== 0) {
      this.setState({searchData : response.data.list})
      let coords = [];
      this.state.searchData.map((item,i) =>
      { const obj = {
        latitude : item.post_lat,
        longitude : item.post_long,
        thumbnail  : item.thumbnail,
        id : item.id,
        creator_id : item.creator_id,
      }
       coords.push(obj)
      })
      this.setState({markers : coords})       
      this.setState({isLoading: false });
    }
    else {
      this.setState({ isLoading: false });
      ToastAndroid.show("No data available", ToastAndroid.SHORT);
    }
  };
   reset = (name,param) => {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [
      StackActions.push("postVideoNav",

      { screen: 'postVideo',params:param} 
      )
    ]
  })
  this.props.navigation.dispatch(resetAction);
}
  mapMarkers = () => {
    if(this.state.markers)
    {
      if(this.state.markers.length !== 0)
      {
       return this.state.markers.map((item,index) => <Marker
       key={item.id}
       coordinate={{ latitude: item.latitude, longitude: item.longitude }}
       onPress = {()=>this.gotoVideoScreen(item.id,item.creator_id)}
       tracksViewChanges = {true}
     >
     <Image source = {{uri : `${item.thumbnail}`}} style = {{height : 60,width : 60 , borderRadius : 30}}/>
     </Marker >)
      }
    }
   }
  gotoVideoScreen (post_id,creator_id)
  {
    console.log(post_id);
    this.props.navigation.navigate("postVideoNav",

    { screen: 'postVideo',params:{post_id:post_id ,creator_id: creator_id,updateScreen : true}} 
    )
  }
  render() {

    if (this.state.isLoading) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#00000050", 
          }}
        >
          <ActivityIndicator color="#fff" size="large" />
        </View>
      );
        }
      else {
    const { enabled, navigation, status, selectedDate } = this.props;
    console.log(enabled);
    console.log(this.state.keyboardOpen,'keyboard');
    const width =  Dimensions.get('window').width;
    const height = Dimensions.get('window').height;

    return (
        <View style={styles.container}>
        { this.state.latitude ? 
          <MapView
          provider={PROVIDER_GOOGLE}
          ref={ref => {
            this.map = ref;
              }}     // remove if not using Google Maps
          style={styles.map}
          // mapType = 'terrain'
          // tintColor = '#0000'
          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
             latitudeDelta: 0.9007,
            longitudeDelta:  0.9007 ,
          }}
          customMapStyle = {mapStyle}
          // loadingEnabled = {true}
          showsUserLocation = {true}
        >

        {this.mapMarkers()}
        </MapView>
         : null }
         <SearchComponent
            position={this.state.sliderPosition}
            slide_in={this.slideIn}
            slide_Out={this.slideOut}
            status={status}
            dateVal={selectedDate}
            lat={this.state.latitude}
            long={this.state.longitude}
            searchPress={this.onSearchPress}
          />
           {status.isCreator && (
            <Icon
              name="search1"
              type="antdesign"
              size={35}
              color={"white"}
              containerStyle={{ position: "absolute", bottom: 35, right: 20 }}
              onPress={() => this.props.toggleSearch()}
            />
          )}
          {!status.isCreator && (
            <Icon
              name="menu"
              onPress={() => this.props.navigation.openDrawer()}
              color="white"
              containerStyle={styles.hamburgerMenuFloating}
            />
          )}
      </View>
    );
  }
}
  }

  function mapStateToProps(state) {
    return {
      enabled: state.search.enabled,
      status: state.user,
      selectedDate: state.search.date,
      searchedText: state.search.searchText,
      selectedrange: state.search.sliderValue,
    };
  }
  function mapDispatchToProps(dispatch) {
    return {
      toggleSearch: () => dispatch(HomeActions.toggle()),
    };
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(HomeScreenNew);
  
