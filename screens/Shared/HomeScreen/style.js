export default {
  hamburgerMenuFloating: {
    position: "absolute",
    top: 10,
    left: 5,
    height: 50,
    width: 50,
    zIndex: 1000,
  },

  SearchComponentView: {
    flex: 1,
    // flexDirection: 'column',
    position: "absolute",
    bottom: 10,
    // padding: 10,
    // left: 12,
    //  backgroundColor: '#fdfdfd',
    maxWidth: 400,
    width: "100%",
    height: 200,
    // borderRadius: 20,
  },
  SearchTextInputView: {
    flex: 0.3,
    flexDirection: "row",
  },
  SearchTextInput: {
    width: "100%",
    height: 30,
    fontSize: 10,
    padding: 0,
    paddingLeft: 15,
    paddingRight: 35,

    backgroundColor: "#e0e0e0",
    borderRadius: 20,
  },
  searchIconView: {
    zIndex: 200,
    position: "absolute",
    right: 6,
    top: 6,
  },
  ShowRangeView: {
    flex: 0.3,
    top: 10,
    left: 105,
  },
  ShowRangeText: {
    fontWeight: "bold",
  },
  SliderAndDayFilterView: {
    flex: 0.3,
    flexDirection: "row-reverse",
    top: 10,
    //backgroundColor : 'red',
    width: "100%",
  },

  SliderText: {
    color: "black",
  },
  DaysFilterView: {
    width: 100,
    flexDirection: "row",
    justifyContent: "space-between",
    top: -4,
  },
  LeftArrow: {
    flexDirection: "row",
    //  backgroundColor:"#ff0000",
    // justifyContent : 'space-between',
    // width: 20,

    //width : 150,
    //paddingLeft : 70
    //marginLeft : 50
  },
  RightArrow: {
    // backgroundColor: "#0000ff",
    alignItems: "center",
    left: 43,
    // top:-4,
    marginRight: 10,
  },
  DayText: {
    // bottom: 17,
    // left: 30,
    alignSelf: "center",
    fontSize: 12,
    fontWeight: "bold",
  },
  inputD: {
    //bottom : hp(50),
    borderColor: "#dcdcdc",
    //left : 50,
    //color : Colors.primary,
    //height: 50,
  },

  ResultDistance: {
    left: 100,
    color: "#7f131e",
    bottom: 19,
    fontWeight: "bold",
  },
};
