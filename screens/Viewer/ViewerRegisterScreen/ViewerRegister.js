import React from "react";
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  StatusBar,
  ToastAndroid,
  ScrollView,
  PermissionsAndroid,
  Modal,
} from "react-native";
import style from "./style.js";
import { Icon, CheckBox } from "react-native-elements";
import DatePicker from "react-native-datepicker";
import {
  listenOrientationChange as loc,
  removeOrientationListener as rol,
} from "react-native-responsive-screen";
import { connect } from "react-redux";
import ViewerRegister from "../../../_actions/ViewerRegistrationActions";
//import ImagePicker from 'react-native-image-picker';
import { ActivityIndicator } from "react-native-paper";
import MapboxGL from "@react-native-mapbox-gl/maps";
import Config from "../../../config";
import ImagePicker from "react-native-image-crop-picker";
import DocumentPicker from "react-native-document-picker";

import urls from "../../../Constants/urls";
import Colors from "../../../Constants/Colors";
import axios from "axios";
import PushNotification from "react-native-push-notification";
MapboxGL.setAccessToken(Config.accessToken);

export class ViewerRegisterScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fname: null,
      lname: null,
      email: null,
      password: null,
      conpassword: null,
      gender: "NA",
      CheckM: false,
      isCheckF: false,
      isCheckT: false,
      myColor: "#dcdcdc",
      photo: null,
      latitude: null,
      longitude: null,
      check: false,
      token: null,
      modalVisible: false,
      wrongEmail: false,
      deviceId: null,
      deviceType: null,
    };
    this.getUserLocation = this.getUserLocation.bind(this);
  }
  componentDidMount() {
    // this codes asks for user permission for location.
    PermissionsAndroid.requestMultiple(
      [
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      ],
      {
        title: "Odiance needs location permission.",
        message: "Please allow Odiance to read your current location.",
      }
    )
      .then((onGranted) => {
        console.log("permission request granted", onGranted);
      })
      .catch((err) => {
        console.log("permission request rejected", err);
      });
    var _this = this;
    PushNotification.configure({
      senderID: "105952880220",

      onRegister: function(token) {
        console.log("TOKEN:", token.token);
        if (token) {
          _this.setState({ deviceId: token.token, deviceType: token.os });
          console.log(_this.state.deviceId, _this.state.deviceType);
        }
      },

      onNotification: function(notification) {
        console.log("NOTIFICATION:", notification);
      },
      onAction: function(notification) {
        console.log("ACTION:", notification);
      },
      onRegistrationError: function(err) {
        console.error("err.message", err);
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
  }

  componentWillUnMount() {
    rol();
  }
  doClear(fieldname) {
    let textInput = this.refs[fieldname];
    textInput.clear();
  }
  validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      this.setState({ email: text });
      this.setState({ wrongEmail: true });
      return false;
    } else {
      this.setState({ email: text });
      this.setState({ wrongEmail: false });
      console.log("Email is Correct");
    }
  };

  validatePassword() {
    const maxLength =
      this.state.password.length > 15
        ? ToastAndroid.show(
            "Password Must be 15 characters or less",
            ToastAndroid.SHORT
          )
        : undefined;
    const minLength =
      this.state.password.length < 6
        ? ToastAndroid.show(
            "Password Must be 8 characters or more",
            ToastAndroid.SHORT
          )
        : undefined;
    const required = this.state.password
      ? undefined
      : ToastAndroid.show("please enter password", ToastAndroid.SHORT);
    this.passwordError = required
      ? required
      : maxLength
      ? maxLength
      : minLength;
  }
  // |[0-9]|[.,/#!$%^&*;:{}=-_`~()])'
  validateName() {
    const namePatter = new RegExp(
      // '(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]|[0-9]|[.,/#!$%^&*;:{}=-_`~()])',
      // 'g',
      /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
    );

    const required = this.state.fname
      ? undefined
      : ToastAndroid.showWithGravity(
          "Please enter Name",
          ToastAndroid.LONG,
          ToastAndroid.CENTER
        );
    this.nameError = required
      ? required
      : // : namePatter.test(this.state.fname)
        // ? ToastAndroid.showWithGravity(
        // 'PLease enter valid Name!',
        // ToastAndroid.LONG,
        // ToastAndroid.CENTER,
        // )
        undefined;
  }
  validateLname() {
    const namePatter = new RegExp(
      // '(\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]|[0-9]|[.,/#!$%^&*;:{}=-_`~()])',
      // 'g',
      /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
    );

    const required = this.state.lname
      ? undefined
      : ToastAndroid.showWithGravity(
          "Please Enter Last Name",
          ToastAndroid.LONG,
          ToastAndroid.CENTER
        );
    this.nameError = required
      ? required
      : namePatter.test(this.state.lname)
      ? ToastAndroid.showWithGravity(
          "Please Enter valid Last Name!",
          ToastAndroid.LONG,
          ToastAndroid.CENTER
        )
      : undefined;
  }
  choosePhotofromGallery = async () => {
    this.setState({ modalVisible: !this.state.modalVisible });

    ImagePicker.openPicker({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then((response) => {
      // console.log(response);
      this.setState({ photo: response });
      console.log(this.state.photo);
    });
  };
  choosePhotofromCamera() {
    this.setState({ modalVisible: !this.state.modalVisible });

    ImagePicker.openCamera({
      freeStyleCropEnabled: true,
      width: 300,
      height: 300,
      cropping: true,
    }).then((response) => {
      //console.log(response);
      this.setState({ photo: response });
      // console.log(this.state.photo);
    });
  }
  handleClick() {
    this.state.isCheckT = !this.state.isCheckT;
    console.log(this.state.isCheckT);
    if (this.state.isCheckT === true) {
      this.setState({ myColor: "black" });
    } else {
      this.setState({ myColor: "#dcdcdc" });
    }
  }
  getUserLocation = async (location) => {
    this.setState({
      timestamp: location.timestamp,
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    });
    // console.info("register screen: ",location);

    // const featureCollection = MapboxGL.geoUtils.makePoint([location.coords.longitude, location.coords.latitude]);
    var result = await this._map.queryRenderedFeaturesAtPoint(location);
    // .then(onGranted => {
    // console.log('granted', onGranted);

    // }).catch(err => {
    // console.log('rejected', err);

    // });
    console.log(result);
  };

 async _onSubmit() {
    if (this.state.fname === null || this.state.lname === null) {
      ToastAndroid.show(
        "You must fill in all of the fields",
        ToastAndroid.SHORT
      );
    } else {
      if (this.state.email === null) {
        ToastAndroid.show(
          "Please enter your email address",
          ToastAndroid.SHORT
        );
      } else {
        if (this.state.password === null) {
          ToastAndroid.show("Please enter your password", ToastAndroid.SHORT);
        } else {
          this.validatePassword();
          if (this.state.password !== this.state.conpassword) {
            ToastAndroid.show("Password doesnot match", ToastAndroid.SHORT);
          } else {
            //console.log(this.state.ischeckM);
            //console.log(this.state.isCheckF);
            var male, Female;
            this.setState({ ischeckM: male });
            this.setState({ isCheckF: Female });
            if ((male = true)) {
              this.state.gender = "ML";
              console.log(this.state.gender);
            } else {
              if (Female == true) {
                this.state.gender = "FM";
              }
            }
            if (
              this.state.isCheckC === false ||
              this.state.isCheckT === false
            ) {
              ToastAndroid.show("Please accept the Terms and Conditions", 0.5);
            } else {
             const registerRes = this.props.ViewerRegister(this.state, this.props.navigation);
             console.log(registerRes,'registerResss')
              this.setState({ check: true });
            }
          }
        }
      }
    }
  }
  async uploadPhoto() {
    const profile_pic = {
      name: "photo.jpg",
      type: "image/jpg",
      uri: this.state.photo.path,
    };
    console.log("hereee");
    const formData = new FormData();
    console.log(formData, "ggg");
    formData.append("avatar", profile_pic);
    formData.append("image_type", "profile");
    fetch(`${urls.base}${urls.avatar}`, {
      method: "POST",
      headers: {
        //   'Content-Type': 'multipart/form-data',
        Authorization: `Token ${this.state.token}`,
        // 'enctype' : 'multipart/form-data',
      },
      body: formData,
    })
      .then((response) => {
        console.log(response, "response photo creator");
        this.setState({ profile_photoData: response });
        // console.log(profile_photoData,'kkkk');
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    const { status } = this.props;
    const { navigation } = this.props;
    if (status.loggedIn === false && status.loggingIn === false) {
      if (status.msg) {
        // ToastAndroid.show(status.msg, 0.5);
      }
    }
    if (status.loggedIn === true && status.isCreator === false) {
      this.state.token = status.token;
      // navigation.navigate('Home');

      if (this.state.photo !== null) {
        this.uploadPhoto();
      }
    }
    return (
      <ScrollView>
        <View style={style.mainView}>
          <View>
            <StatusBar backgroundColor="#7B0C1F" barStyle="light-content" />
          </View>
          <View style={style.headerContainer}>
            <View
              style={style.imageView}
              onStartShouldSetResponder={() =>
                this.setState({ modalVisible: !this.state.modalVisible })
              }
            >
              {!this.state.photo && (
                <Image
                  source={require("../../../assets/icons/user_male2-512.png")}
                  //source = {{uri : this.state.photo.uri}}
                  style={style.imageCircle}
                />
              )}

              {this.state.photo && (
                <Image
                  // source={require('../../../assets/icons/user_male2-512.png')}
                  source={{ uri: this.state.photo.path }}
                  style={style.imageCircle}
                />
              )}
              <TouchableOpacity
                style={style.pencilView}
                onPress={() =>
                  this.setState({ modalVisible: !this.state.modalVisible })
                }
              >
                <Icon
                  name="pencil"
                  type="font-awesome"
                  size={16}
                  color="#ffffff"
                  onPress={() =>
                    this.setState({ modalVisible: !this.state.modalVisible })
                  }
                  containerStyle={style.Iconp}
                />
              </TouchableOpacity>
              <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() =>
                  this.setState({ modalVisible: !this.state.modalVisible })
                }
              >
                <View
                  style={{ flex: 1, backgroundColor: "rgba(0,0,0,0.5)" }}
                  onStartShouldSetResponder={() =>
                    this.setState({ modalVisible: !this.state.modalVisible })
                  }
                >
                  <View
                    style={{
                      flex: 0.3,
                      backgroundColor: Colors.white,
                      top: "25%",
                      marginHorizontal: "10%",
                      padding: "5%",
                    }}
                  >
                    <TouchableOpacity
                      style={{ flex: 0.25 }}
                      onPress={() => this.choosePhotofromCamera()}
                    >
                      <Text style={{ fontSize: 20 }}>Take Photo</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ flex: 0.25 }}
                      onPress={() => this.choosePhotofromGallery()}
                    >
                      <Text style={{ fontSize: 20 }}>
                        Choose Photo from Library
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{ alignSelf: "flex-end", flex: 0.5 }}
                      onPress={() =>
                        this.setState({
                          modalVisible: !this.state.modalVisible,
                        })
                      }
                    >
                      <Text style={{ fontSize: 20, right: "20%", top: "50%" }}>
                        Cancel
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
            </View>

            <Text style={style.textHeader}>Information Required</Text>
          </View>
          <View style={style.attachContainer}>
            <MapboxGL.MapView
              styleURL={MapboxGL.StyleURL.Dark}
              ref={(c) => (this._map = c)}
            >
              <MapboxGL.UserLocation
                ref={(d) => (this._location = d)}
                visible={true}
                onUpdate={this.getUserLocation}
              />
            </MapboxGL.MapView>
            <View style={style.boder}>
              <View style={style.inputName}>
                <Text style={style.labelContainer}>First name *</Text>
                <TextInput
                  ref="textInput"
                  style={style.inputF}
                  placeholder="ex Jhone"
                  onChangeText={(text) => {
                    this.setState({ fname: text });
                  }}
                />
                <Icon
                  name="cancel"
                  color="#DFDFDF"
                  containerStyle={style.iconF}
                  onPress={() => this.doClear("textInput")}
                />
              </View>
              <View style={style.boderL}>
                <Text style={style.labelContainer}>Last name *</Text>
                <TextInput
                  ref="textInput1"
                  style={style.inputL}
                  placeholder="ex dio"
                  onChangeText={(text) => {
                    this.setState({ lname: text });
                  }}
                />
                <Icon
                  name="cancel"
                  color="#DFDFDF"
                  containerStyle={style.iconL}
                  onPress={() => this.doClear("textInput1")}
                />
              </View>
            </View>
            <View
              style={
                this.state.wrongEmail
                  ? { ...style.boderE, borderColor: "#FF0000" }
                  : { ...style.boderE, borderColor: "#dcdcdc" }
              }
            >
              <Text style={style.labelContainer}>Email *</Text>

              <TextInput
                ref="textInput2"
                style={style.input}
                placeholder="ex info@gmail.com"
                onChangeText={(text) => this.validate(text)}
                value={this.state.email}
              />
              {/* <Text style={style.inputT}>{this.state.email}</Text>  */}
              <Icon
                name="cancel"
                color="#DFDFDF"
                containerStyle={style.icon}
                onPress={() => this.doClear("textInput2")}
              />
            </View>
            <View style={style.boderP}>
              <Text style={style.labelContainer}>Password *</Text>
              <TextInput
                ref="textInput3"
                style={style.input}
                placeholder="ex password"
                onChangeText={(text) => {
                  this.setState({ password: text });
                }}
                secureTextEntry={true}
              />
              <Icon
                name="cancel"
                color="#DFDFDF"
                containerStyle={style.icon}
                onPress={() => this.doClear("textInput3")}
              />
            </View>
            <View style={style.boderCp}>
              <Text style={style.labelContainer}>Confirm Password *</Text>
              <TextInput
                ref="textInput4"
                style={style.input}
                placeholder="ex confirm password"
                onChangeText={(text) => {
                  this.setState({ conpassword: text });
                }}
                secureTextEntry={true}
              />
              <Icon
                name="cancel"
                color="#DFDFDF"
                containerStyle={style.iconCp}
                onPress={() => this.doClear("textInput4")}
              />
            </View>
          </View>
          <View style={style.radioButton}>
            <View style={style.male}>
              <CheckBox
                disabled={this.state.isCheckF}
                center
                title="Male"
                textStyle={style.test}
                size={20}
                uncheckedIcon="square"
                checked={this.state.CheckM}
                onPress={() => this.setState({ CheckM: !this.state.CheckM })}
                checkedColor="#131313"
                containerStyle={style.checkBox}
              />
              <CheckBox
                disabled={this.state.CheckM}
                center
                title="Female"
                size={20}
                uncheckedIcon="square"
                checked={this.state.isCheckF}
                onPress={() =>
                  this.setState({ isCheckF: !this.state.isCheckF })
                }
                checkedColor="#131313"
                textStyle={style.test}
                containerStyle={style.checkBox}
              />
            </View>
            <View style={style.terms}>
              <CheckBox
                left
                size={20}
                title="I agree to the following Terms and Conditions"
                uncheckedIcon="square"
                checked={this.state.isCheckT}
                onIconPress={() => this.handleClick()}
                onPress={() => this.handleClick()}
                checkedColor="black"
                textStyle={{ color: this.state.myColor }}
                containerStyle={style.checkBoxTerms}
              />
            </View>
          </View>
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              bottom: "5%",
            }}
          >
            <TouchableOpacity
              style={style.submit}
              disabled={status.loggingIn}
              onPress={() => this._onSubmit()}
            >
              {status.loggingIn ? (
                <ActivityIndicator color="#fff" />
              ) : (
                <Text style={style.submitText}>CREATE</Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}
function mapStatetoProps(state) {
  return {
    status: state.user,
  };
}
const actionCreators = {
  ViewerRegister: ViewerRegister,
};
export default connect(
  mapStatetoProps,
  actionCreators
)(ViewerRegisterScreen);
