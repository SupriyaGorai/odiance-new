import React from 'react';
import {PostVerticalScroll} from '../../Creator/InspirationScreen/inspirationScreen';
import {Appbar} from 'react-native-paper';
import {View} from 'react-native';
import NotificationHeader from '../components/NotificationHeader'
import Colors from '../../../Constants/Colors';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import { heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

class PlaylistScreen extends React.Component{
    constructor(props){
        super(props);
    }
    navigate = (id) => {
        const {navigation} = this.props;
        navigation.navigate('post', {id: id});
    }
    render(){
        const videos = [
            { thumb: require('../../../assets/images/bhai.png'), views: '12.5k', },
            { thumb: require('../../../assets/images/shakira.jpeg'), views: '30.5k', },
            { thumb: require('../../../assets/images/Dwayne.jpg'), views: '22.5k', },
            { thumb: require('../../../assets/images/john.jpeg'), views: '2.5k', },
            { thumb: require('../../../assets/images/selina.jpeg'), views: '2k', },
            { thumb: require('../../../assets/images/selina.jpeg'), views: '2k', },
            { thumb: require('../../../assets/images/selina.jpeg'), views: '2k', },
            { thumb: require('../../../assets/images/selina.jpeg'), views: '2k', },
            { thumb: require('../../../assets/images/selina.jpeg'), views: '2k', },
            { thumb: require('../../../assets/images/selina.jpeg'), views: '2k', },
            { thumb: require('../../../assets/images/selina.jpeg'), views: '2k', },
            { thumb: require('../../../assets/images/selina.jpeg'), views: '2k', },
            { thumb: require('../../../assets/images/bhai.png'), views: '12.5k', },
            { thumb: require('../../../assets/images/shakira.jpeg'), views: '30.5k', },
            { thumb: require('../../../assets/images/Dwayne.jpg'), views: '22.5k', },
            { thumb: require('../../../assets/images/john.jpeg'), views: '2.5k', },
            { thumb: require('../../../assets/images/selina.jpeg'), views: '2k', },
        ];
        const {id} = this.props.route.params;
        const {navigation} = this.props;
        const map = new Map([[1, 'Inspirations'], [2, 'Likes'], [3, 'Favorites']]);
        
        return (
            <View>
                {/* <Appbar.Header style={{backgroundColor: Colors.black, height:heightPercentageToDP(5)}}>
                
                    <Appbar.BackAction
                    style={{width:16}}
                    
                        onPress={() => { navigation.goBack()}}
                    />
                    <Appbar.Content
                        title={id?map.get(id):''}
                        titleStyle={{
                            
                            fontSize: hp(2),}}
                            iconTheme={{size:10}}
                    />
                    
                </Appbar.Header> */}
                <NotificationHeader header= {id?map.get(id):''} navigation = {navigation}  />
                <PostVerticalScroll post={videos} navigate={this.navigate} />
            </View>
        );
    }
}
export default PlaylistScreen;