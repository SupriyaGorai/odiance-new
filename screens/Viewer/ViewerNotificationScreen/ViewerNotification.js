import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail,  Left, Body, Right, Button } from 'native-base';
import Colors from '../../../Constants/Colors';
import ProfileHeader from '../../Creator/components/ProfileHeader'
import {POST } from '../../../_services/services';
import { connect } from "react-redux";
import { getInitialState } from "../../../_actions/LoginAction";
import { color } from 'react-native-reanimated';
import { ActivityIndicator, Title } from 'react-native-paper';
import urls from '../../../Constants/urls';
export  class ViewerNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: [],
      arrayholder: [],
      loading : false,
    };

  }
  async componentDidMount()
  {
    const url1 = `notification/list/`;
    const apiBody = {
      received_by: this.props.status.id,
      notification_id: 0,
    };

    const notificationList = await POST(url1, this.props.status.token, apiBody);
    console.log(notificationList);
    console.log("apiBody");
    if (notificationList.status === 200) {
      this.setState({
        loading : true,
        notifications: [
          ...this.state.notifications,
          ...notificationList.data.notification,
        ],
        arrayholder: notificationList.data.notification,
        loading : false,
      });
    }
  }

  render() {
    const {navigation} = this.props
    console.log(this.state.arrayholder,'2');
    if(this.state.loading === true)
    {
      return <ActivityIndicator/>
    }
    else 
    {
    return (
        
              <Container>
              <Header style = {{backgroundColor : Colors.primary}} androidStatusBarColor = {Colors.greyIcon}>
                <Title style = {{ color : Colors.white, fontSize : 25,alignSelf : 'center', right : 70}}>Notification</Title>
              </Header>
              <Content>

                  {
                    this.state.notifications.length !== 0 
                    ?
                    this.state.notifications.map((item,index) => {
                   return <NotificationCard data = {item} key = {index} navigation = {this.props.navigation}/> 
                      
                    })
                   : <Text style = {{alignSelf : 'center',fontSize : 25}}> No Notifications
                   </Text>
                   }
                </Content>
              </Container>
            );
          }
        }
      }
        export function NotificationCard(props) {
         return (
                    <List>
                    <ListItem thumbnail>
                      
                      <Left>
                        { props.data.sent_by_avatar.charAt(0) === 'h' ? 
                        <Thumbnail round source={{ uri: `${props.data.sent_by_avatar}` }} />
                        : 
                        <Thumbnail round source={{ uri: `${urls.avatarbase}${props.data.sent_by_avatar}` }} />
                        }
                      </Left>
                      <Body>
                    <Text>{props.data.sent_by_name}</Text>
                        <Text note numberOfLines={1}>{props.data.notification_text}</Text>
                      </Body>
                      <Right>
                        <Button transparent onPress = {()=>props.navigation.navigate("postVideoNav", {screen: "postVideo",
                        params: { post_id: props.data.postid} })}>
                          <Text>View</Text>
                        </Button>
                      </Right>
                    </ListItem>
                    </List>
            )
        }
        function mapStatetoProps(state) {
          return {
            status: state.user,
          };
        }
        
        // const actionsHistory = {
        //   getInitialState: getInitialState,
        // };
        
        export default connect(
          mapStatetoProps,
          // actionsHistory
        )(ViewerNotification);
        