import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {Icon, colors} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {TextInput} from 'react-native-gesture-handler';

export default class EmptyScreen extends Component {
  render() {
    const navigation = this.props.navigation;
    return (
      <View style={{flex: 1, backgroundColor: '#2b2b2b'}}>
        <View
          style={{
            left: 0,
            position: 'absolute',
            marginLeft: 20,
            marginVertical: 10,
          }}>
          <Icon
            name="left"
            type="antdesign"
            size={15}
            // marginLeft={20}
            // marginTop={10}

            color={'white'}
            onPress={() => navigation.navigate('postVideo')}
            // top={2}
          />
        </View>
        <View style={{flexDirection: 'column', marginTop: 25}}>
          <View
            style={{
              height: 100,
              paddingLeft: 35,
              paddingVertical: 10,
              paddingRight: 35,
            }}>
            <View style={{flexDirection: 'column'}}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={require('../../../assets/images/prabhudeva.jpeg')}
                  style={{
                    width: 40,
                    height: 'auto',
                    aspectRatio: 1,
                    borderRadius: wp(10),
                    borderWidth: wp(0.7),
                    borderColor: '#3f3f3d',
                    marginLeft: 10,
                  }}
                />
                <View
                  style={{
                    // backgroundColor: 'black',
                    width: '80%',
                    marginHorizontal: 10,
                    borderRadius: 10,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 12,
                      marginHorizontal: 8,
                      marginVertical: 5,
                    }}>
                    {' '}
                    Salman Khan
                  </Text>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 10,
                      marginHorizontal: 8,
                      marginBottom: 5,
                    }}>
                    {/* {this.state.text} */}
                    good
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginHorizontal: 20,
                  marginVertical: 5,
                }}>
                <View style={{width: '10%'}} />
                <Icon
                  type="antdesign"
                  name="like1"
                  color="#fff"
                  size={15}

                  // onPress={() => navigation.navigate('Settings')}
                />
                <Icon
                  type="antdesign"
                  name="dislike1"
                  color="#fff"
                  size={15}

                  // onPress={() => navigation.navigate('Settings')}
                />
                <Icon
                  type="font-awesome"
                  name="share"
                  color="#fff"
                  size={15}

                  // onPress={() => navigation.navigate('Settings')}
                />
                <View style={{width: '40%'}} />
              </View>
              <View
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: '#3e3e3e',
                  height: 5,
                  //   paddingTop: 10,
                  marginRight: 20,
                  marginLeft: 60,
                }}
              />
            </View>
          </View>
          {/*  */}

          <View
            style={{
              height: 100,
              paddingLeft: 75,
              paddingVertical: 10,
              paddingRight: 35,
            }}>
            <View style={{flexDirection: 'column'}}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={require('../../../assets/images/prabhudeva.jpeg')}
                  style={{
                    width: 40,
                    height: 'auto',
                    aspectRatio: 1,
                    borderRadius: wp(10),
                    borderWidth: wp(0.7),
                    borderColor: '#3f3f3d',
                    marginLeft: 10,
                  }}
                />
                <View
                  style={{
                    // backgroundColor: 'black',
                    width: '80%',
                    marginHorizontal: 10,
                    borderRadius: 10,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 12,
                      marginHorizontal: 8,
                      marginVertical: 5,
                    }}>
                    {' '}
                    Salman Khan
                  </Text>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 10,
                      marginHorizontal: 8,
                      marginBottom: 5,
                    }}>
                    {/* {this.state.text} */}
                    good
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginHorizontal: 20,
                  marginVertical: 5,
                }}>
                <View style={{width: '10%'}} />
                <Icon
                  type="antdesign"
                  name="like1"
                  color="#fff"
                  size={15}

                  // onPress={() => navigation.navigate('Settings')}
                />
                <Icon
                  type="antdesign"
                  name="dislike1"
                  color="#fff"
                  size={15}

                  // onPress={() => navigation.navigate('Settings')}
                />
                <Icon
                  type="font-awesome"
                  name="share"
                  color="#fff"
                  size={15}

                  // onPress={() => navigation.navigate('Settings')}
                />
                <View style={{width: '40%'}} />
              </View>
              <View
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: '#3e3e3e',
                  height: 5,
                  //   paddingTop: 10,
                  marginRight: 20,
                  marginLeft: 60,
                }}
              />
            </View>
          </View>

          {/*  */}
          <View
            style={{
              height: 100,
              paddingLeft: 75,
              paddingVertical: 10,
              paddingRight: 35,
            }}>
            <View style={{flexDirection: 'column'}}>
              <View style={{flexDirection: 'row'}}>
                <Image
                  source={require('../../../assets/images/prabhudeva.jpeg')}
                  style={{
                    width: 40,
                    height: 'auto',
                    aspectRatio: 1,
                    borderRadius: wp(10),
                    borderWidth: wp(0.7),
                    borderColor: '#3f3f3d',
                    marginLeft: 10,
                  }}
                />
                <View
                  style={{
                    // backgroundColor: 'black',
                    width: '80%',
                    marginHorizontal: 10,
                    borderRadius: 10,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 12,
                      marginHorizontal: 8,
                      marginVertical: 5,
                    }}>
                    {' '}
                    Salman Khan
                  </Text>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: 10,
                      marginHorizontal: 8,
                      marginBottom: 5,
                    }}>
                    {/* {this.state.text} */}
                    good
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginHorizontal: 20,
                  marginVertical: 5,
                }}>
                <View style={{width: '10%'}} />
                <Icon
                  type="antdesign"
                  name="like1"
                  color="#fff"
                  size={15}

                  // onPress={() => navigation.navigate('Settings')}
                />
                <Icon
                  type="antdesign"
                  name="dislike1"
                  color="#fff"
                  size={15}

                  // onPress={() => navigation.navigate('Settings')}
                />
                <Icon
                  type="font-awesome"
                  name="share"
                  color="#fff"
                  size={15}

                  // onPress={() => navigation.navigate('Settings')}
                />
                <View style={{width: '40%'}} />
              </View>
              <View
                style={{
                  borderBottomWidth: 1,
                  borderBottomColor: '#3e3e3e',
                  height: 5,
                  //   paddingTop: 10,
                  marginRight: 20,
                  marginLeft: 60,
                }}
              />
            </View>
          </View>
        </View>
        <View
          style={{
            height: 100,
            paddingLeft: 35,
            paddingVertical: 10,
            paddingRight: 35,
          }}>
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('../../../assets/images/prabhudeva.jpeg')}
                style={{
                  width: 40,
                  height: 'auto',
                  aspectRatio: 1,
                  borderRadius: wp(10),
                  borderWidth: wp(0.7),
                  borderColor: '#3f3f3d',
                  marginLeft: 10,
                }}
              />
              <View
                style={{
                  // backgroundColor: 'black',
                  width: '80%',
                  marginHorizontal: 10,
                  borderRadius: 10,
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 12,
                    marginHorizontal: 8,
                    marginVertical: 5,
                  }}>
                  {' '}
                  Salman Khan
                </Text>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 10,
                    marginHorizontal: 8,
                    marginBottom: 5,
                  }}>
                  {/* {this.state.text} */}
                  good
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: 20,
                marginVertical: 5,
              }}>
              <View style={{width: '10%'}} />
              <Icon
                type="antdesign"
                name="like1"
                color="#fff"
                size={15}

                // onPress={() => navigation.navigate('Settings')}
              />
              <Icon
                type="antdesign"
                name="dislike1"
                color="#fff"
                size={15}

                // onPress={() => navigation.navigate('Settings')}
              />
              <Icon
                type="font-awesome"
                name="share"
                color="#fff"
                size={15}

                // onPress={() => navigation.navigate('Settings')}
              />
              <View style={{width: '40%'}} />
            </View>
            <View
              style={{
                borderBottomWidth: 1,
                borderBottomColor: '#3e3e3e',
                height: 5,
                //   paddingTop: 10,
                marginRight: 20,
                marginLeft: 60,
              }}
            />
          </View>
        </View>

        {/*  */}
        <View
          style={{
            bottom: 0,
            position: 'absolute',
            flexDirection: 'row',
            borderColor: '#595959',
            borderRadius: 20,
            padding: 1,
            // color: 'white',
            borderWidth: 1,
            width: '94%',
            height: 40,
            paddingHorizontal: 20,
            paddingRight: 40,
            marginHorizontal: 10,
            // marginBottom: 40,
            margin: 10,
          }}>
          <TextInput
            placeholder={'Type a comment'}
            placeholderTextColor={'#595959'}
            style={{color: 'white'}}
          />
        </View>
        <View
          style={{
            bottom: 0,
            right: 0,
            position: 'absolute',
            marginRight: 25,
            marginBottom: 15,
            aspectRatio: 1,
            backgroundColor: '#66001a',
            borderRadius: 20,
            width: 25,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Icon
            type="font-awesome"
            name="send"
            color="#fff"
            size={15}

            // onPress={() => navigation.navigate('Settings')}
          />
        </View>
      </View>
    );
  }
}
