import React from "react";
import { ScrollView, Image } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Colors from "../../../Constants/Colors";
import loadPosts from "../../../_services/LoadPosts";
import LoadUserDetails from "../../../_services/UserDetails";
import Details from "./../../Shared/component/Details";
import {
  PivateDraftVideoTab,
  PostCreditTab,
} from "./../../Shared/component/FeaturedVideos";
import { connect } from "react-redux";
import { POST } from "../../../_services/services";
import { ActivityIndicator } from "react-native-paper";

const Tab = createMaterialTopTabNavigator();

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      creator_id: null,
      posts: [],
      response: null,
      user_data: [],
      id: null,
      following: 0,
      follower: 0,
    };
  }

  navigate = (id) => {
    const { navigation } = this.props;
    navigation.navigate("postVideo", { post_id: id });
  };

  async componentDidMount() {
    
    const userId = this.props.status.id;
    const creator_id = this.props.route.params.creatorId;
    console.log(creator_id,'pro');
    const user = userId !== null ? userId : creator_id;

    const user_data = await LoadUserDetails(creator_id);
    if (user_data) {
      this.setState({ user_data: user_data });
    }

    const posts = await loadPosts(creator_id);

    if (posts) {
      this.setState({ posts: posts });
    }

    let res = await POST(
      `user/${creator_id}/followlist/`,
      this.props.status.token
    );
    console.log("res", res);
    if (res.status == 200) {
      this.setState({
        follower: res.data.follower_count,
        following: res.data.following_count,
      });
    }
  }
  render() {
    const { status, Post_status, posts, navigation } = this.props;
   
    const creator_id = this.props.route.params.creatorId;
    console.log(creator_id,'creator_id');
    const user = status.id !== null ? status.id : creator_id;
    // const navigation = this.props.navigation;
    if(this.state.user_data === null)
    {
      return <ActivityIndicator/>
    }
    else {
    return (
      <ScrollView style={{ flex: 1, flexDirection: "column" }}>
        {this.state.user_data ? (
          <Details
            navigation={navigation}
            image={this.state.user_data}
            profile_photo={this.state.profile_photo}
            follower={this.state.follower}
            following={this.state.following}
            inPostProfileScreen={true}
          />
        ) : null}

        {this.props.status.id || creator_id ? (
          <Tab.Navigator
            initialRouteName="Posts"
            tabBarOptions={{
              indicatorStyle: { backgroundColor: Colors.primary, height: 5 },
              showIcon: true,
              iconStyle: { width: 30, height: 30 },
              showLabel: false,
            }}
          >
            <Tab.Screen
              name="Posts"
              component={PostCreditTab}
              initialParams={{
                navigate: this.navigate,
                id: creator_id,
              }}
              options={{
                tabBarIcon: () => (
                  <Image
                    source={require("../../../assets/icons/grid.png")}
                    style={{ width: 25, height: 25 }}
                  />
                ),
              }}
            />
            <Tab.Screen
              name="Credits"
              component={PivateDraftVideoTab}
              initialParams={{
                navigate: this.navigate,
              }}
              options={{
                tabBarIcon: () => (
                  <Image
                    source={require("../../../assets/icons/profile(1).png")}
                    style={{ width: 25, height: 25 }}
                  />
                ),
              }}
            />
          </Tab.Navigator>
        ) : null}
      </ScrollView>
    );
  }
}
}
function mapStatetoProps(state) {
  return {
    status: state.user,
    Post_status: state.post,
  };
}
export default connect(mapStatetoProps)(ProfileScreen);
