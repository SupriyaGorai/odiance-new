import React from "react";
import {
  ScrollView,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Share,
  Modal,
  Keyboard,
  ToastAndroid,
  Alert,
  Dimensions,
} from "react-native";
import { Icon, CheckBox } from "react-native-elements";
import { connect } from "react-redux";
import GetPostDetails from "../../../_services/GetPostDetails";
import PostComments from "../../../_services/CommentsService";
import GetComments from "../../../_services/GetComments";
import AddFollower from "./../../../_services/AddFollowerApi";
import AddToViewList from "./../../../_services/AddToViewList";
import AddToFavouriteList from "./../../../_services/AddToFavouriteList";
import WatchLater from "./../../../_services/WatchLater";


import { getInitialState } from "../../../_actions/LoginAction";
import style from "./style";
import Video from "react-native-video";
import CommentList from "../components/Comment_list/CommentList";
import ActivityIndicator from "./../components/ActivityIndicator";
import LikeDislikeApi from "./../../../_services/LikeDislikeApi";
import InspirationalApi from "../../../_services/InspirationalApi";
import Indicator from "../components/Indicator";
import { POST } from "./../../../_services/services";
import Comments from "../../../_actions/CommentsAction";
// import Orientation from "react-native-orientation";
class PostVideoScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      creator_id: null,
      Comments: null,
      commentData: [],
      response: null,
      text: "",
      isLoading: false,
    };
  }
  navigate = (id) => {
    const { navigation } = this.props;
    navigation.navigate("postVideo");
  };

  async componentDidMount() {
    try {
      await this.getPostData();
      await this.getCommentData();
    } catch (e) {
      console.log(e);
      console.log("err in post video screen");
    }
  }
  async componentDidUpdate(prevProps) {
    if(this.props.route.params.post_id!==prevProps.route.params.post_id)
    { 
    try {
      await this.getPostData();
      await this.getCommentData();
    } catch (e) {
      console.log(e);
      console.log("err in post video screen");
    }
  }
}
  getPostData = async () => {
    console.log(this.props.status, "lll");
    console.log(this.props.route.params.creator_id, "kkk");
    console.log("post video screen");
    const userId = this.props.status.id;

    const creatorId = this.props.route.params.creator_id;

    const user = userId ? userId : creatorId;

    const postId = this.props.route.params.post_id;
    console.log(user, "h");
    console.log(postId, "k");

    const Comments = await GetPostDetails(user, postId);
    console.log(Comments, "props");
    this.setState({ Comments: Comments[0] }, () => {
      if (Comments.is_like_by_me === true) {
        this.setState({ isLiked: true });
      }
      if (Comments.is_dislike_by_me === true) {
        this.setState({ isDisliked: true });
      }
    });
  };

  getCommentData = async () => {
    this.setState({ isLoading: true });
    const userId = this.props.status.id;

    const creatorId = this.props.route.params.creator_id;

    // issue, what if user not logged in
    const user = userId ? userId : creatorId;

    const postId = this.state.Comments.id;
    const commentData = await GetComments(user, postId);
    console.log(commentData, "commentData");
    this.setState({
      commentData: commentData,
      isLoading: false,
    });
  };

  editCommentList = () => {
    const commentId = this.props.commentId;
    const commentArr = this.state.commentData;
    const text = this.props.commentText;

    const index = commentArr.findIndex((comment) => comment.id === commentId);

    commentArr[index].text = text;

    this.setState({ commentData: commentArr });
  };
  editReplyCommentList = () => {
    const commentId = this.props.replyCommentId;
    const commentText = this.props.replyCommentText;
    const commentArr = this.state.commentData;
    const postId = this.state.Comments.id;

    const index = commentArr.findIndex((comment) => comment.id === commentId);

    const newComment = {
      audio: "",
      commented_by: this.props.status.id,
      commented_by_avatar: commentArr[index].commented_by_image,
      created_on: "",
      id: commentArr[index].id,
      is_audio: false,
      post_id: postId,
      text: commentText,
    };

    const commentList = commentArr[index].child_comments;
    commentList.push(newComment);

    this.setState({ commentData: commentArr });
  };

  resetCommentList = () => {
    const commentId = this.props.commentId;
    const commentArr = this.state.commentData;

    const index = commentArr.findIndex((comment) => comment.id === commentId);

    commentArr.splice(index, 1);
    this.setState({ commentData: commentArr });
  };

  sendComment = async () => {
    if (this.state.text === "") {
      ToastAndroid.showWithGravity(
        "Please enter something",
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM
      );
    } else {
      Keyboard.dismiss();
      this.setState({ text: "" });

      const text = this.state.text;
      const token = this.props.status.token;
      const postId = this.state.Comments.id;

      const data = {
        text: text,
        is_audio: "0",
        audio: "",
        parent_comment: 0,
      };
      const sendData = JSON.stringify(data);

      let response = await PostComments(sendData, token, postId);

      if (response.success === true) {
        const commentedByName =
          response.data.commented_by.first_name +
          " " +
          response.data.commented_by.last_name;
        const newData = {
          is_like_by_me: false,
          is_dislike_by_me: false,
          like: 0,
          dislike: 0,
          commented_by_image: response.data.commented_by.avatar,
          text: response.data.text,
          id: response.data.id,
          commented_by_name: commentedByName,
          commented_by: this.props.status.id,
          child_comments: [],
        };

        const commentArray = this.state.commentData;
        console.log(response, "resssssss");
        commentArray.push(newData);

        this.setState({ commentData: commentArray });
      }
    }
  };

  render() {
    const { status, Comment_status, navigation } = this.props;
    const creatorId = this.props.route.params.creator_id;
    const updateData = this.props.route.params.updateScreen;
    // if(updateData)
    // {
    //   this.componentDidUpdate()
    // }
    if (this.state.Comments === null || this.state.isLoading === true) {
      return <ActivityIndicator />;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <ScrollView
            style={style.mainView}
            keyboardShouldPersistTaps={"handled"}
          >
            <PostVideoScreenBanner
              comment={this.state.Comments}
              navigation={navigation}
              userId={status.id}
              token={status.token}
              creator_id={creatorId}
              status={status}
            />
            <PostVideoScreenUploadPart
              comment={this.state.Comments}
              userId={status.id}
              token={status.token}
              navigation={navigation}
            />

            {this.state.isLoading ? (
              <Indicator />
            ) : (
              this.state.commentData.map((item) => (
                <CommentList
                  key={item.id}
                  userId={status.id}
                  data={item}
                  postId={this.state.Comments.id}
                  token={this.props.status.token}
                  resetComment={this.resetCommentList}
                  editComment={this.editCommentList}
                  editReplyComment={this.editReplyCommentList}
                />
              ))
            )}
          </ScrollView>

          {status.id ? (
            <View style={{ backgroundColor: "#2b2b2b" }}>
              <View style={style.commentInputContainer}>
                <TextInput
                  style={style.commentInput}
                  placeholder="Type a Comment"
                  placeholderTextColor={"#595959"}
                  value={this.state.text}
                  onChangeText={(text) => this.setState({ text })}
                />
                <TouchableOpacity onPress={this.sendComment}>
                  <Icon type="ionicons" name="send" color="#fff" size={25} />
                </TouchableOpacity>

                {/* {this.state.text === "" ? (
                <TouchableOpacity
                  onPressIn={this.onStartRecord}
                  onPressOut={this.onStopRecord}
                  onPress={() => alert("hi")}
                >
                  <Icon
                    type="font-awesome"
                    name="microphone"
                    color="#fff"
                    size={22}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity onPress={this.sendComment}>
                  <Icon type="ionicons" name="send" color="#fff" size={25} />
                </TouchableOpacity>
              )} */}
              </View>
            </View>
          ) : null}
        </View>
      );
    }
  }
}

class PostVideoScreenBanner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showView: false,
      Icon_Color: "#fff",
      isItMe: false,
      isFollowing: false,
    };
  }

  componentDidMount() {
    console.log(this.props.comment, "jj");
    if (this.props.comment.is_followed_by_me === false) {
      this.setState({ isFollowing: false });
    } else {
      this.setState({ isFollowing: true });
    }
    const myId = this.props.userId;
    const videoCreeatorId = this.props.comment.creator[0].id;

    if (myId === videoCreeatorId) {
      this.setState({ isItMe: true });
    } else {
      this.setState({ isItMe: false });
    }
  }

  toggleView() {
    this.setState({
      showView: !this.state.showView,
    });
  }
  onPressFollow = async () => {
    const myId = this.props.userId;
    console.log(this.props.comment.creator[0].id, "vvvid");
    if (myId === null) {
      Alert.alert(
        "Please login to follow the creator",
        "",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => this.props.navigation.navigate("Login"),
          },
        ],
        { cancelable: false }
      );
    } else {
      const videoCreeatorId = this.props.comment.creator[0].id;
      const tokenId = this.props.token;

      if (!this.state.isFollowing) {
        this.setState({ isFollowing: true });
        const data = {
          follow_to: videoCreeatorId,
          follow_type: "",
        };
        const jsonData = JSON.stringify(data);

        const AddFollowerRes = await AddFollower(jsonData, tokenId, myId);
        console.log(AddFollowerRes, "AddFollowerRes");
        //  if(AddFollowerRes.status === 200){
        //   this.props.comment.is_inspired_to_me = true

        //  }
      } else {
        this.setState({ isFollowing: false });
        const data = {
          follow_to: videoCreeatorId,
          follow_type: "remove",
        };
        const jsonData = JSON.stringify(data);

        const removeFollowerRes = await AddFollower(jsonData, tokenId, myId);
        console.log(removeFollowerRes, "removeFollowerRes");
        //  if(removeFollowerRes.status === 200){
        //   this.props.comment.is_inspired_to_me = false
        //  }
      }
    }
  };

  onPressReport = async () => {
    const userId = this.props.userId;
    const postId = this.props.comment.id;
    const url = `user/${userId}/post/${postId}/report/`;
    const tokenId = this.props.token;

    const body = {
      reported_by: userId,
    };

    const res = await POST(url, tokenId, body);

    if (res.status === 200) {
      ToastAndroid.showWithGravity(
        res.data.msg,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM
      );
    }
  };
  render() {
    const { comment, creator_id } = this.props;
    const videoCreatorId = this.props.comment.creator[0].id;
    console.log(creator_id, "creator_id");
    console.log(videoCreatorId);

    return (
      <View>
        {this.state.showView ? (
          <View style={style.banner.reportView}>
            <Icon
              name="list-ul"
              type="font-awesome"
              size={12}
              marginLeft={5}
              color={"#66001a"}
              // top={2}
            />
            <TouchableOpacity onPress={this.onPressReport}>
              <Text style={style.banner.reportText}>Report content</Text>
            </TouchableOpacity>
          </View>
        ) : null}

        <View>
          <View style={style.banner.postVideoHeader}>
            <View style={style.banner.detailCardView}>
              <TouchableOpacity
                style={{ padding: 5 }}
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon
                  name="arrowleft"
                  type="antdesign"
                  size={18}
                  color={"#fff"}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("PostVideoProfile", {
                    creatorId: creator_id,
                  })
                }
              >
                <Image
                  source={ comment.creator[0].avatar ?   
                    comment.creator[0].avatar.charAt(0) === 'h' ? 
                       { uri: comment.creator[0].avatar }
                      : require("../../../assets/images/avatar.png")
                      
                  :  require("../../../assets/images/avatar.png")
                }
                  style={style.banner.avatar}
                />
              </TouchableOpacity>

              <View style={style.banner.nameView}>
                <Text
                  style={style.banner.nameText}
                  onPress={() =>
                    this.props.navigation.navigate("PostVideoProfile", {
                      creatorId: creator_id,
                    })
                  }
                >
                  {comment.creator[0].first_name} {comment.creator[0].last_name}
                </Text>

                <View style={{ flexDirection: "column" }}>
                  <Text
                    style={style.banner.designationText}
                    onPress={() =>
                      this.props.navigation.navigate("PostVideoProfile", {
                        creatorId: creator_id,
                      })
                    }
                  >
                    {comment.creator[0].alumni}
                  </Text>
                </View>
              </View>
            </View>

            <TouchableOpacity onPress={() => this.toggleView()}>
              <View>
                <Icon
                  type="entypo"
                  name="dots-three-vertical"
                  color={this.state.showView ? "#ccc" : "#fff"}
                  size={20}
                  padding={5}
                />
              </View>
            </TouchableOpacity>
          </View>

          <View style={style.banner.captionView}>
            <View style={{ flexDirection: "column" }}>
              <Text style={{ color: "white", fontSize: 13 }}>
                {comment.caption === "" ? "Caption" : comment.caption}
              </Text>
              <Text style={{ color: "white", fontSize: 8, paddingTop: 5 }}>
                {comment.views_count} views
              </Text>
            </View>
            {this.state.isItMe ? null : (
              <TouchableOpacity
                style={style.banner.followBtn}
                onPress={this.onPressFollow}
              >
                <Text style={style.banner.followBtnText}>
                  {this.state.isFollowing ? "Following" : "Follow"}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    );
  }
}

class PostVideoScreenUploadPart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      playvideo: false, //
      isLiked: false,
      isInspirational: true,
      isDisliked: false,
      likeCount: 0,
      inspirationalCount: 0,
      dislikeCount: 0,
      isFavouritee: false,
      isMarkedForWatchLater : false,
      modalOpen : false,
    };
  }

  componentDidMount() {
    this.setState({
      isLiked: this.props.comment.is_like_by_me,
      likeCount: this.props.comment.likes_count,
      isDisliked: this.props.comment.is_dislike_by_me,
      isInspirational : this.props.comment.is_inspired_to_me,
      inspirationalCount: this.props.comment.inspiration_count,
      dislikeCount: this.props.comment.dislikes_count,
      isFavouritee : this.props.comment.is_favourite
    });
  }

  onPressLike = async (id) => {
    const user_id = this.props.userId;

    if (user_id) {
      let like = this.state.likeCount;
      let dislike = this.state.dislikeCount;

      if (!this.state.isLiked) {
        this.setState({
          isLiked: true,
          isDisliked: false,
        });
        // updating like count locally

        if (this.state.isDisliked) {
          this.setState({
            likeCount: like + 1,
            dislikeCount: dislike === 0 ? null : dislike - 1,
          });
        } else {
          if (this.state.isLiked) {
            return null;
          } else {
            this.setState({
              likeCount: like + 1,
              dislikeCount: dislike,
            });
          }
        }

        // like api call
        const Body = {
          comment_id: 0,
          is_like: 1,
          typelike: ""
        };
        const apiBody = JSON.stringify(Body);
        const postId = id;
        const tokenId = this.props.token;
        const likeRes = await LikeDislikeApi(postId, apiBody,tokenId);
        console.log(likeRes, "likeRes");
      } else {
        this.setState({
          isLiked: false,
          likeCount: like === 0 ? null : like - 1,
          dislikeCount: dislike,
        });

        // like api call
        const Body = {
          comment_id: 0,
          is_like: 1,
          typelike: "remove"
        };
        const apiBody = JSON.stringify(Body);
        const postId = id;

        await LikeDislikeApi(postId, apiBody);
      }
    } else {
      Alert.alert(
        "Please login to like the video",
        "",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => this.props.navigation.navigate("Login"),
          },
        ],
        { cancelable: false }
      );
    }
  };
  onPressInspirational = async (id) =>{
    const user_id = this.props.userId;

    if (user_id) {
      let inspirational = this.state.inspirationalCount;
      // let dislike = this.state.dislikeCount;

      if (!this.state.isInspirational) {
        this.setState({
          isInspirational: true,
          inspirationalCount: inspirational + 1,
          // isDisliked: false,
        });
        // updating inspirational count locally

        // if (this.state.isInspirational) {
        //   this.setState({
        //     inspirationalCount: inspirational + 1,
        //   });
        // } 

        // inspirational api call
        const Body = {
            inspired_by: user_id,
            typeinspire:""
        };
        const apiBody = JSON.stringify(Body);
        const postId = id;
        const tokenId = this.props.token;
        const inspirationalRes = await InspirationalApi(postId,user_id ,apiBody,tokenId);
        console.log(inspirationalRes, "inspirationalRes");
      } else {
        this.setState({
          isInspirational: false,
          inspirationalCount: inspirational === 0 ? null : inspirational - 1,
          
        });

        // inspirational api call
        const Bodyy = {

          inspired_by: user_id,
          typeinspire:"remove",
         
        };
        const apiBodyy = JSON.stringify(Bodyy);
        const postIdd = id;
        const tokenIdd = this.props.token;
         await InspirationalApi(postIdd,user_id,apiBodyy,tokenIdd);
        
      }
    } else {
      Alert.alert(
        "Please login to react the video",
        "",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => this.props.navigation.navigate("Login"),
          },
        ],
        { cancelable: false }
      );
    }
  }

  onPressDislike = async (id) => {
    const user_id = this.props.userId;

    if (user_id) {
      let like = this.state.likeCount;
      let dislike = this.state.dislikeCount;

      if (!this.state.isDisliked) {
        this.setState({
          isDisliked: true,
          isLiked: false,
        });

        // updating dislike count locally

        if (this.state.isLiked) {
          this.setState({
            likeCount: like === 0 ? null : like - 1,
            dislikeCount: dislike + 1,
          });
        } else {
          if (this.state.isDisliked) {
            return null;
          } else {
            this.setState({
              likeCount: like,
              dislikeCount: dislike + 1,
            });
          }
        }

        const Body = {
          comment_id: 0,
          is_like: 0,
          typelike: "",
        };
        const apiBody = JSON.stringify(Body);
        const postId = id;
        const tokenId = this.props.token;
        await LikeDislikeApi(postId, apiBody,tokenId);
      } else {
        this.setState({
          isDisliked: false,
          likeCount: like,
          dislikeCount: dislike === 0 ? null : dislike - 1,
        });

        const Body = {
          comment_id: 0,
          is_like: 0,
          typelike: "remove",
        };
        const apiBody = JSON.stringify(Body);
        const postId = id;
        const tokenId = this.props.token;
        await LikeDislikeApi(postId, apiBody,tokenId);
      }
    } else {
      Alert.alert(
        "Please login to dislike the video",
        "",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => this.props.navigation.navigate("Login"),
          },
        ],
        { cancelable: false }
      );
    }
  };

  onSharePress = async (videoLink) => {
    const user_id = this.props.userId;

    if (user_id) {
      try {
        const result = await Share.share({
          message: `Share With Friends : ${videoLink}`,
          title: "Share With Friends",
          url: videoLink,
        });

        if (result.action === Share.sharedAction) {
          console.log("Post Shared");
        } else if (result.action === Share.dismissedAction) {
          // dismissed
          console.log("Post cancelled");
        }
      } catch (error) {
        alert(error.message);
      }
    } else {
      Alert.alert(
        "Please login to share the video",
        "",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => this.props.navigation.navigate("Login"),
          },
        ],
        { cancelable: false }
      );
    }
  };

  onPressPlay = async () => {
    this.setState({ playvideo: !this.state.playvideo });
    const user_id = this.props.userId;
    console.log(user_id, "user_id");
    if (user_id) {
      const post_id = this.props.comment.id;
      const data = {
        viewed_by: user_id,
      };
      const jsonData = JSON.stringify(data);
      const tokenId = this.props.token;
      await AddToViewList(user_id, post_id, jsonData,tokenId);
    } else {
      return null;
    }
  };

  onPressAdd = () => {
    const user_id = this.props.userId;

    if (user_id) {
      this.setState({ modalVisible: !this.state.modalVisible });
    } else {
      Alert.alert(
        "Please login to add to favourites",
        "",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => this.props.navigation.navigate("Login"),
          },
        ],
        { cancelable: false }
      );
    }
  };
  AddWatchLater = async () => {
    this.state.isMarkedForWatchLater = !this.state.isMarkedForWatchLater;
    this.setState({ isMarkedForWatchLater: this.state.isMarkedForWatchLater })
    console.log(this.state.isMarkedForWatchLater, "isMarkedForWatchLater");
    const user_id = this.props.userId;
    const tokenId = this.props.token;
    
    if (user_id) {
      const post_id = this.props.comment.id;
      console.log(user_id,post_id,tokenId, "user_id");
      const data = ''
      await WatchLater(user_id,post_id,data,tokenId);
    } else {
      return null;
    }
  }
  AddFavourite = async () => {
    //  this.setState({ isFavourite: !this.state.isFavourite });
    this.state.isFavouritee = !this.state.isFavouritee;
    this.setState({ isFavouritee: this.state.isFavouritee })
    console.log(this.state.isFavouritee, "tog");
    const user_id = this.props.userId;
    const tokenId = this.props.token;
    console.log(user_id, "user_id");
    if (user_id) {
      const post_id = this.props.comment.id;
      const data = {
        favourite_by: user_id,
        is_favourite: this.state.isFavouritee,
      };
      const jsonData = JSON.stringify(data);

      await AddToFavouriteList(user_id, post_id, jsonData, tokenId);
    } else {
      return null;
    }
  };
  ModalOpenForAddPL = async () => {
    this.setState({ modalVisible: !this.state.modalVisible });
    this.setState({ modalOpen: !this.state.modalOpen });
  }
  render() {
    console.log(this.props, "po");
    const { comment } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <View>
        <Modal
               animationType="slide"
               transparent={true}
               visible={this.state.modalOpen}
               onRequestClose={() =>
                 this.setState({
                   modalOpen: !this.state.modalOpen,
                   
                 })
               }
               presentationStyle="overFullScreen"
              >
                <View
                  style={{ flex: 1, backgroundColor: "rgba(0,0,0,0.5)", alignItems: "center",justifyContent:'center' }}
                  onStartShouldSetResponder={() =>
                    this.setState({
                      modalOpen: !this.state.modalOpen,
                      
                    })
                  }
                >

                
                <View 
                style={{ backgroundColor: "white",
               
                height: "75%",
                width: "85%",
                }}
                >

                </View>
                </View>

              </Modal>
              </View>
        <View
          style={{
            height: 200,
            width: "100%",
            marginVertical: 3,
          }}
        >
          {comment ? (
            !this.state.playvideo ? (
              <Image
                source={{ uri: `${comment.thumbnail}` }}
                style={{ height: "100%", width: "100%", resizeMode: "contain" }}
              />
            ) : null
          ) : null}
          <TouchableOpacity
            style={{
              alignSelf: "center",
              top: 75,
              position: "absolute",
              width: 60,
              height: 60,
              backgroundColor: "rgba(255,255,255,0.5)",
              borderRadius: 50,
              alignItems: "center",
              justifyContent: "center",
              paddingLeft: 10,
            }}
            // onPress={() => this.setState({ playvideo: !this.state.playvideo })}
            onPress={this.onPressPlay}
          >
            <Icon name="play" type="font-awesome" size={52} color="#3f3f3d" />
          </TouchableOpacity>
          {comment ? (
            this.state.playvideo ? (
              <Video
                source={{ uri: `${comment.video_url}` }} // Can be a URL or a local file.
                ref={(ref) => {
                  this.player = ref;
                }}
                tapAnywheretoPause={true}
                paused={!this.state.playvideo}
                controls={this.state.playvideo}
                resizeMode="contain" // Store reference
                onBuffer={this.onBuffer} // Callback when remote video is buffering
                onError={this.videoError}
                // Callback when video cannot be loaded
                style={{ height: "100%", width: "100%" }}
              />
            ) : // <TouchableOpacity>
            //   <Image  source = />
            // </TouchableOpacity>
            null
          ) : null}
        </View>

        <View
          style={{
            flexDirection: "column",
            paddingHorizontal: 30,
            paddingVertical: 10,
          }}
        >
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <View style={{ flexDirection: "column" }}>
              <TouchableOpacity onPress={() => this.onPressLike(comment.id)}>
                <Icon
                  type="antdesign"
                  name="like1"
                  color={this.state.isLiked ? "#66001a" : "#fff"}
                  size={20}
                />
                <Text style={{ fontSize: 12, color: "#fff" }}>
                  {this.state.likeCount}
                </Text>
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: "column" }}>
              <TouchableOpacity onPress={() => this.onPressDislike(comment.id)}>
                <Icon
                  type="antdesign"
                  name="dislike1"
                  color={this.state.isDisliked ? "#66001a" : "#fff"}
                  size={20}
                />
                <Text style={{ fontSize: 12, color: "#fff" }}>
                  {this.state.dislikeCount}
                </Text>
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: "column", justifyContent:'center' }}>
              <TouchableOpacity
              onPress={() => this.onPressInspirational(comment.id)}
              >
                {this.state.isInspirational ?
                <Image
                source={require("../../../assets/icons/cool.png")}
                 style={{left:5}}
              /> :
              <Image
                  source={require("../../../assets/icons/history_mid.png")}
                   style={{left:5}}
                />

                }

                <Text style={{ fontSize: 12, color: "#fff", left: 5 }}>
                {this.state.inspirationalCount}
                </Text>
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: "column" }}>
              <TouchableOpacity
                style={{ padding: 5 }}
                onPress={() => this.onSharePress(comment.video_url)}
              >
                <Icon type="entypo" name="share" color="#fff" size={20} />
              </TouchableOpacity>

              {/* <Text style={{ fontSize: 12, color: "#fff" }}>225k</Text> */}
            </View>

            <View style={{ flexDirection: "column" }}>
              <TouchableOpacity onPress={this.onPressAdd}>
                <Icon
                  type="antdesign"
                  name="pluscircleo"
                  color={this.state.modalVisible ? "#66001a" : "#fff"}
                  size={20}
                />
                <Text style={{ fontSize: 12, color: "#fff" }}>Add</Text>
              </TouchableOpacity>
              
              <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() =>
                  this.setState({
                    modalVisible: !this.state.modalVisible,
                    showImage: false,
                    index: 0,
                    focused: false,
                  })
                }
                presentationStyle="overFullScreen"
              >
                <View
                  style={{ flex: 1, backgroundColor: "rgba(0,0,0,0.5)" }}
                  onStartShouldSetResponder={() =>
                    this.setState({
                      modalVisible: !this.state.modalVisible,
                      showImage: false,
                      index: 0,
                      focused: false,
                    })
                  }
                >
                  <View
                    style={{
                      width: "100%",
                      height: 120,
                      backgroundColor: "white",
                      bottom: 0,
                      position: "absolute",
                    }}
                  >
                    <View style={{ height: 15 }}>
                      <View
                        style={{
                          alignSelf: "flex-end",
                          paddingHorizontal: 20,
                          paddingVertical: 5,
                        }}
                      >
                        <TouchableOpacity
                        onPress ={()=>
                          this.ModalOpenForAddPL()
                        }
                        >
                        <View
                          style={{ alignItems: "center", flexDirection: "row" }}
                        >
                          
                          <Icon
                            type="antdesign"
                            name="pluscircle"
                            color={"#66001a"}
                            size={15}
                          />
                          <View style={{ width: 5 }} />
                          <Text style={{ fontSize: 15, color: "grey" }}>
                            Add to playlist
                          </Text>
                          
                        </View>
                        </TouchableOpacity>
                        
                        
                      </View>
                      
                    </View>
                    <View
                      style={{
                        borderBottomWidth: 1,
                        borderBottomColor: "#e5e5e5",
                        height: 10,
                        paddingTop: 15,
                        // marginHorizontal: 5,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      alignSelf: "flex-start",
                      paddingHorizontal: 10,
                      marginBottom: 15,
                      flexDirection: "column",
                      position: "absolute",
                      bottom: -20,

                      // justifyContent:'space-between'
                    }}
                  >
                    <View
                      style={{ alignItems: "center", flexDirection: "row" }}
                    >
                      <CheckBox
                        left
                        size={20}
                        uncheckedIcon="square"
                        checked={this.state.isMarkedForWatchLater}
                        onIconPress={() => this.AddWatchLater()}
                        checkedColor="#131313"
                      />
                      <View style={{ width: 10, height: 0 }} />
                      <Text style={{ fontSize: 15 }}>Watch Later</Text>
                    </View>
                    {/* <View style={{ height: 10 }} /> */}

                    <View
                      style={{ alignItems: "center", flexDirection: "row" }}
                    >
                      <CheckBox
                        left
                        size={20}
                        uncheckedIcon="square"
                        checked={this.state.isFavouritee}
                        onIconPress={() => this.AddFavourite()}
                        checkedColor="#131313"
                      />
                      <View style={{ width: 10, height: 0 }} />
                      <Text style={{ fontSize: 15 }}>Add to Favorite</Text>
                    </View>
                    {/* <View style={{ height: 10 }} /> */}
                  </View>
                </View>
              </Modal>
            </View>
          </View>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "#3e3e3e",
              height: 5,
              paddingTop: 10,
              // marginHorizontal: 5,
            }}
          />
        </View>
      </View>
    );
  }
}

function mapStatetoProps(state) {
  return {
    status: state.user,
    Comment_status: state.postVideo,
    commentId: state.comment.commentId,
    commentText: state.comment.newCommentText,
    replyCommentId: state.comment.replyCommentId,
    replyCommentText: state.comment.replyCommentText,

    // code: state.code,
  };
}

const actionsComment = {
  getInitialState: getInitialState,
};

export default connect(
  mapStatetoProps,
  actionsComment
)(PostVideoScreen);
