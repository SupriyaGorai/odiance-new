import Colors from "../../../Constants/Colors";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const mainMargin = 5;

export default (style = {
  
  mainView: {
    flex: 1,
    backgroundColor: "#2b2b2b",
  },
  commentInputContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#2b2b2b",
    borderColor: "#595959",
    borderRadius: 30,
    borderWidth: 1,
    margin: 10,
    height: 45,
  },
  commentInput: {
    paddingHorizontal: 20,
    color: "white",
    width: "90%",
  },
  banner: {
    backgroundView: {
      height: 100,
      width: "100%",
      // backgroundColor: '#3f3f3d',
    },
    backgroundImage: {
      width: "auto",
      resizeMode: "cover",
      height: "auto",
      borderRadius: 3,
      flex: 1,
    },
    mainView: {
      flexDirection: "column",
      position: "absolute",
      // backgroundColor: '#red',
      width: "100%",
      height: 10,
      marginVertical: hp(2),
    },
    toolsHeader: {
      // flex: 0.2,
      flexDirection: "row",
      justifyContent: "space-between",
      // marginHorizontal: wp(3),
      marginVertical: hp(2),
    },
    toolsLower: {
      flex: 0.2,
      flexDirection: "row",
      justifyContent: "space-between",
      marginHorizontal: wp(3),
      //  marginVertical: hp(2),
    },
    backButton: {
      flexDirection: "row",
    },
    headerText: {
      color: Colors.white,
      marginLeft: 5,
    },
    searchBarStyle: {
      borderColor: Colors.white,
      borderRadius: hp(50),
      paddingLeft: 10,
      paddingRight: 20,
      marginRight: 5,
      padding: 1,
      color: Colors.white,
      borderWidth: 0.5,
      width: 200,
    },
    searchIconStyle: {
      position: "absolute",
      top: 3,
      right: 10,
      color: Colors.white,
    },

    detailView: {
      paddingHorizontal: 15,
      // width: '100%',
      // height: 'auto',
      // flexDirection: 'row',
    },
    postVideoHeader: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
      paddingHorizontal: 10,
      paddingVertical: 12,
      backgroundColor: "#66001a",
    },
    detailCardView: {
      flexDirection: "row",
      alignItems: "center",
    },

    reportView: {
      flexDirection: "row",
      alignItems: "center",
      position: "absolute",
      top: 35,
      right: 20,
      alignSelf: "flex-end",
      zIndex: 10,
      backgroundColor: "#fff",
      padding: 8,
    },
    reportText: {
      fontSize: 12,
      fontWeight: "bold",
      marginHorizontal: 5,
    },
    captionView: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
      padding: 10,
      paddingTop: 20,
    },
    avatar: {
      width: 30,
      height: "auto",
      aspectRatio: 1,
      borderRadius: wp(10),
      borderWidth: wp(0.7),
      borderColor: "#3f3f3d",
    },
    nameView: {
      paddingLeft: 8,
    },
    nameText: {
      fontWeight: "bold",
      fontSize: 13,
      color: "#ffffff",
    },
    designationText: {
      fontSize: 10,
      color: "#d5d5d5",
    },
    designationText2: {
      fontSize: 10,
      color: "white",
    },
    followBtn: {
      borderWidth: 1,
      borderColor: "#d5d5d5",
      paddingVertical: 4,
      paddingHorizontal: 10,
      borderRadius: 8,
    },
    followBtnText: {
      color: "#d5d5d5",
      fontSize: 10,
    },
  },
  recentVideos: {
    mainViewContainer: {
      flex: 1,
      margin: mainMargin,
    },
    mainView: {
      marginLeft: mainMargin,
      flex: 0.2,
      flexDirection: "row",
      alignItems: "center",
    },
    labelText: {
      marginLeft: mainMargin,
      fontWeight: "300",
    },
    bottomBorder: {
      marginHorizontal: mainMargin,
      borderBottomWidth: 1,
      borderBottomColor: Colors.lightGrey,
    },
  },
  UploadPart: {},
});
