import React, { Component } from 'react';
import { Text} from 'react-native';
import { Header } from 'react-native-elements';
import styles from './Styles';
import { View } from 'native-base';
import {Icon} from 'react-native-elements';


class TextHeader extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const txt = this.props.header;
        return <Text style={styles.headerText}>{txt}</Text>;
    }
}

export default class NotificationHeader extends Component {
    constructor(props) {
        super(props);
      }
    render() {
        const navigation = this.props.navigation;
        const header = this.props.header;
        
        return (
            <Header
                containerStyle={styles.notificationHeader}
                centerComponent={<TextHeader header={header} />}
                centerContainerStyle={styles.menuIcon}
                leftComponent={
                    // icon: 'arrowleft',
                    // type: 'antdesign',
                    // color: '#FFF',
                    // size: 17,
                    // onPress : {()=> {navigation.navigate('main')}},
                    <View>
                    <Icon name='arrowleft' type ='antdesign' color = '#fff' size={17} onPress = {()=> {navigation.navigate('main')}}/>
                    </View>
                }
                leftContainerStyle={styles.menuIcon}
            />
        );
    }
}
