import { StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  modalContainer: {
    width: "100%",
    height: 100,
    backgroundColor: "#2b2b2b",
    bottom: 0,
    position: "absolute",
  },
  editContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  commentInputContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#2b2b2b",
    borderColor: "#595959",
    borderRadius: 30,
    borderWidth: 1,
    margin: 10,
    height: 45,
    width: "80%",
  },
  userImage: {
    width: 40,
    height: "auto",
    aspectRatio: 1,
    borderRadius: wp(10),
    borderWidth: wp(0.7),
    borderColor: "#3f3f3d",
    marginLeft: 10,
  },
  commentInput: {
    paddingHorizontal: 10,
    color: "white",
    width: "86%",
  },
  deleteContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 5,
  },
  deleteText: {
    color: "#ccc",
    paddingLeft: 5,
  },
});

export default styles;
