import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Modal,
  Image,
  TouchableWithoutFeedback,
  Alert,
  TextInput,
} from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import {
  setCommentId,
  setNewCommentText,
} from "../../../../_actions/CommentsAction";
import styles from "./style";
import EditCommentApi from "../../../../_services/EditCommentServices";
import DeleteCommentApi from "../../../../_services/DeleteCommentApi";

function EditCommentModal(props) {
  const [commentVal, setCommentVal] = useState("comment text");
  useEffect(() => {
    let mounted = true;

    if (mounted) {
      setCommentVal(props.commentText);
    }

    return () => (mounted = false);
  }, []);

  const onPressEdit = async () => {
    const token_id = props.token;
    const post_id = props.postId;
    const comment_id = props.commentId;
    const comment = commentVal;

    const data = {
      text: comment,
      is_audio: 0,
      audio: "",
      parent_comment: comment_id,
    };

    const jsonData = JSON.stringify(data);

    const response = await EditCommentApi(
      jsonData,
      token_id,
      post_id,
      comment_id
    );

    if (response) {
      props.setId(comment_id);
      props.setCommentText(comment);
      props.editCommentList();
    }
    props.toggleModal();
  };

  const onPressDelete = async () => {
    const token_id = props.token;
    const post_id = props.postId;
    const comment_id = props.commentId;

    Alert.alert(
      "Are you sure",
      "",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: async () => {
            // api call
            const deleteRes = await DeleteCommentApi(
              comment_id,
              token_id,
              post_id
            );
            // console.log(deleteRes.length);
            if (deleteRes === 200) {
              props.setId(comment_id);
              props.resetCommentList();
            }

            props.toggleModal();
          },
        },
      ],
      { cancelable: false }
    );
  };

  const imageLink = (props) => {
    if (props.image !== undefined) {
      if (props.image[0] === "/") {
        return `http://111.93.169.90:8008${props.image}`;
      } else {
        return `${props.image}`;
      }
    }
  };

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="fade"
        transparent={true}
        visible={props.modalVisible}
        onRequestClose={props.toggleModal}
      >
        <TouchableWithoutFeedback onPress={props.toggleModal}>
          <View style={styles.modalView}>
            <View style={styles.modalContainer}>
              <View style={styles.editContainer}>
                <Image
                  source={{ uri: imageLink(props) }}
                  style={styles.userImage}
                />
                <View style={styles.commentInputContainer}>
                  <TextInput
                    style={styles.commentInput}
                    placeholder="Type a Comment"
                    placeholderTextColor={"#595959"}
                    value={commentVal}
                    onChangeText={(text) => setCommentVal(text)}
                  />

                  <TouchableOpacity
                    onPress={onPressEdit}
                    style={{ padding: 5 }}
                  >
                    <Icon type="ionicons" name="send" color="#fff" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity
                style={styles.deleteContainer}
                onPress={onPressDelete}
              >
                <Icon type="ionicons" name="delete" color="#ccc" size={15} />
                <Text style={styles.deleteText}>Delete comment</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    setId: (val) => dispatch(setCommentId(val)),
    setCommentText: (val) => dispatch(setNewCommentText(val)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(EditCommentModal);
