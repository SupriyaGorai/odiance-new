
import { heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

export default styles = {
    homeHeader: {
        height: hp(5),
        backgroundColor: '#ffff',
    },
    menuIcon: {
        marginTop: hp(50),
        marginBottom: hp(50),
        marginLeft: wp(1),
        position: 'absolute',
    },
    notificationHeader: {
        height: hp(5),
        backgroundColor: 'black',
        width : '100%'
    },
    headerText: {
        color: '#ffff',
        marginLeft: wp(6),
        fontSize: hp(2),
    },
   
};