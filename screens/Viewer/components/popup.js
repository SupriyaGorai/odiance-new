import React,{Component} from 'react';
import {View,Text} from 'react-native';

export default class Popup extends React.Component {
    constructor(props) {
      super(props);
    }
    render() {
      const {
        iconStyle,
        name,
        label,
        type,
        labelStyle,
        source,
        navStyle,
        onPressButton,
      } = this.props;
      return (
        <View style={style.navStyle}>
          <Image source={source} style={iconStyle} />
          <Text style={labelStyle}>{label}</Text>
        </View>
      );
    }
  }