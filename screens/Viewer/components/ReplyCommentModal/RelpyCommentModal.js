import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Modal,
  Image,
  TextInput,
  ToastAndroid,
  ScrollView,
} from "react-native";
import { Icon } from "react-native-elements";
import { connect } from "react-redux";
import {
  setReplyCommentId,
  setReplyCommentText,
} from "../../../../_actions/CommentsAction";
import styles from "./Styles";
import DeleteCommentApi from "../../../../_services/DeleteCommentApi";
import { POST } from "./../../../../_services/services";

class ReplyCommentModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commentVal: [],
      inputText: "",
    };
  }

  componentDidMount() {
    this.setState({ commentVal: this.props.data });
  }

  onPressEdit = async () => {
    const token_id = this.props.token;
    const post_id = this.props.postId;
    const comment_id = this.props.commentId;
    const comment = this.state.inputText;
    const url = `post/${post_id}/comment/`;
    console.log(url);

    if (comment === "") {
      ToastAndroid.showWithGravity(
        "Please enter something",
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM
      );
    } else {
      const body = {
        text: comment,
        is_audio: 0,
        audio: "",
        parent_comment: comment_id,
      };
      console.log(body);

      const response = await POST(url, token_id, body);

      if (response.status === 200) {
        this.props.setCommentId(comment_id);
        this.props.setCommentText(comment);
        this.props.editReplyCommentList();
        this.setState({ inputText: "" });
      }
    }
  };
  imageLink = (image) => {
    if (image !== undefined) {
      if (image[0] === "/") {
        return `http://111.93.169.90:8008${image}`;
      } else {
        return `${image}`;
      }
    }
  };
  render() {
    return (
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.modalVisible}
          onRequestClose={this.props.toggleModal}
        >
          <View style={styles.modalView}>
            <View style={styles.modalContainer}>
              {this.state.commentVal.length !== 0 ? (
                <ScrollView>
                  {this.state.commentVal.map((comment, i) => (
                    <View key={i} style={styles.childCommentList}>
                      <Image
                        resizeMode="contain"
                        source={{
                          uri: this.imageLink(comment.commented_by_avatar),
                        }}
                        style={styles.avatar}
                      />
                      <Text style={styles.childCommentText}>
                        {comment.text}
                      </Text>
                    </View>
                  ))}
                </ScrollView>
              ) : null}

              <View style={styles.editContainer}>
                <Image
                  source={{ uri: this.props.image }}
                  style={styles.userImage}
                />

                <View style={styles.commentInputContainer}>
                  <TextInput
                    style={styles.commentInput}
                    placeholder="Type a Comment"
                    placeholderTextColor={"#595959"}
                    value={this.state.inputText}
                    onChangeText={(text) => this.setState({ inputText: text })}
                  />

                  <TouchableOpacity
                    onPress={this.onPressEdit}
                    style={{ padding: 5 }}
                  >
                    <Icon type="ionicons" name="send" color="#fff" size={25} />
                  </TouchableOpacity>
                </View>
              </View>
              {/* <TouchableOpacity
                  style={styles.deleteContainer}
                  onPress={onPressDelete}
                >
                  <Icon type="ionicons" name="delete" color="#ccc" size={15} />
                  <Text style={styles.deleteText}>Delete comment</Text>
                </TouchableOpacity> */}
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setCommentId: (val) => dispatch(setReplyCommentId(val)),
    setCommentText: (val) => dispatch(setReplyCommentText(val)),
  };
};

export default connect(
  null,
  mapDispatchToProps
)(ReplyCommentModal);
