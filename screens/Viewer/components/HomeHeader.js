import {Header} from 'react-native-elements';
import React from 'react';
import styles from './Styles';

export default class HomeHeader extends React.Component{
    constructor(props){
        super(props);        
    }
    render(){
        const {navigation,name} = this.props.navigation;
        return(
            <Header
                containerStyle={styles.homeHeader}
                leftContainerStyle={styles.menuIcon}
                centerComponent={{ text: {name}, style: { color: '#fff' } }}
                placement='left'
                leftComponent={{icon: 'caret-left', type : 'font-awesome', color: '#fff', onPress:  () => {/*navigation.ju('Home')*/} }}
            />


        );
    }
}