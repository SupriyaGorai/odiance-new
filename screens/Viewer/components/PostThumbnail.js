import React from "react";
import { View, Image, Text, ScrollView } from "react-native";
import { Icon } from "react-native-elements";
import { TouchableOpacity } from "react-native-gesture-handler";
import Video from "react-native-video";
import { string } from "prop-types";

export class PostThumbNail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: this.props.image,
    };
  }

  render() {
    const { image, views, id, navigate, isPost, video } = this.props;

    return (
      <>
        <TouchableOpacity
          style={{ marginHorizontal: 5, aspectRatio: 1 }}
          onPress={() => {
            navigate(id);
          }}
        >
          <Image
            source={{ uri: String(this.state.url) }}
            style={{
              height: "100%",
              aspectRatio: 1,
              alignSelf: "center",
              resizeMode: "cover",
              borderRadius: 3,
            }}
          />
          {views && (
            <View
              style={{
                position: "absolute",
                flexDirection: "row",
                alignItems: "center",
                bottom: "1%",
                left: "3%",
              }}
            >
              <Icon
                name="eye"
                type="font-awesome"
                color={Colors.white}
                size={10}
              />
              {/* <Text style={{ color: Colors.white, fontSize: 9 }}> {views}</Text> */}
            </View>
          )}
        </TouchableOpacity>
      </>
    );
  }
}

export class DynamicThumbNail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: this.props.image,
    };
  }

  render() {
    const { image, views, id, navigate, isPost, video } = this.props;
    // console.log(typeof this.state.url,'postthumbnail');
    return (
      <TouchableOpacity
        style={{ marginHorizontal: 5, aspectRatio: 1 }}
        onPress={() => {
          navigate(id);
        }}
      >
        <Image
          source={{ uri: String(this.state.url) }}
          style={{
            height: "100%",
            aspectRatio: 1,
            alignSelf: "center",
            resizeMode: "cover",
            borderRadius: 3,
          }}
        />
        {views && (
          <View
            style={{
              position: "absolute",
              flexDirection: "row",
              alignItems: "center",
              bottom: "1%",
              left: "3%",
            }}
          >
            <Icon
              name="eye"
              type="font-awesome"
              color={Colors.white}
              size={10}
            />
            {/* <Text style={{ color: Colors.white, fontSize: 9 }}> {views}</Text> */}
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

export class PostHorizontalScroll extends React.Component {
  render() {
    const { content, navigate } = this.props;
    // console.log(content);
    // console.log("content");

    return (
      <ScrollView horizontal={true} style={{ height: 60 }}>
        {content.length == 0 ? (
          <Text
            style={{
              color: "#979797",
              alignSelf: "center",
              paddingLeft: 30,
            }}
          >
            No videos...
          </Text>
        ) : (
          <>
            {content.map((data, index) => {
              if (index < 5) {
                return (
                  <PostThumbNail
                    key={index}
                    // isPost={data.post[0].isPost}
                    id={data.post[0].id}
                    video={data.post[0].video_url}
                    image={data.post[0].thumbnail}
                    navigate={navigate}
                  />
                );
              }
            })}
            <TouchableOpacity
              style={{ marginHorizontal: 5, aspectRatio: 1 }}
              onPress={this.props.toggleModal}
            >
              <Image
                source={require("../../../assets/images/more.png")}
                style={{
                  height: "100%",
                  aspectRatio: 1,
                  alignSelf: "center",
                  resizeMode: "cover",
                  borderRadius: 30,
                }}
              />
            </TouchableOpacity>
          </>
        )}
      </ScrollView>
    );
  }
}
export class DynamicHorizontalScroll extends React.Component {
  render() {
    const { content, navigate } = this.props;

    return (
      <ScrollView horizontal={true} style={{ height: 90 }}>
        {content.map((data, key) => {
          //console.log(data.thumbnail,'posthorizontalscroll')
          return (
            <DynamicThumbNail
              key={key}
              isPost={data.isPost}
              id={data.id}
              video={data.video_url}
              image={data.thumbnail}
              navigate={navigate}
            />
          );
        })}
      </ScrollView>
    );
  }
}
