import React, { useState } from "react";
import { View, Text, ActivityIndicator } from "react-native";

export default function Indicator() {
  const [isIndicatorOn, setIsIndicatorOn] = useState(true);

  setTimeout(() => {
    setIsIndicatorOn(false);
  }, 5000);

  if (isIndicatorOn) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "transparent",
        }}
      >
        <ActivityIndicator color={"#fff"} size={"small"} />
      </View>
    );
  }else{
    return null;
  }
}
