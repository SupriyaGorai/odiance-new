import { StyleSheet } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const styles = StyleSheet.create({
  commentContainer: {
    maxHeight: 180,
    flexDirection: "row",
    paddingHorizontal: 5,
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: "#3e3e3e",
  },
  imageView: {
    width: "18%",
    paddingTop: 5,
  },
  userImage: {
    width: 40,
    height: "auto",
    aspectRatio: 1,
    borderRadius: wp(10),
    borderWidth: wp(0.7),
    borderColor: "#3f3f3d",
    marginLeft: 10,
  },
  commentView: {
    width: "82%",
    alignItems: "flex-start",
  },
  userNameText: {
    color: "#ccc",
    fontSize: 10,
    marginVertical: 5,
  },
  commentText: {
    color: "white",
    fontSize: 12,
    marginVertical: 5,
    paddingRight: 5,
  },
  likeShareBtnSection: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 12,
    marginBottom: 5,
  },
  iconBtns: {
    marginRight: 15,
    padding: 5,
    paddingLeft: 0,
    flexDirection: "row",
    alignItems: "center",
  },
  iconTextColor: {
    color: "#ccc",
    fontSize: 16,
    paddingHorizontal: 5,
  },
  childCommentView: {
    width: "100%",
    paddingBottom: 10,
  },
  childCommenText: {
    color: "#ccc",
    fontSize: 12,
    paddingLeft: 10,
    // marginVertical: 5,
  },
});

export default styles;
