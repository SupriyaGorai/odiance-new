import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { Icon } from "react-native-elements";
import LikeDislikeApi from "../../../../_services/LikeDislikeApi";
import Modal from "../EditCommentModal/EditCommentModal";
import ReplyCommentModal from "../ReplyCommentModal/RelpyCommentModal";

import styles from "./listStyle";

export default function CommentList(props) {
  const [isliked, setIsLiked] = useState(false);
  const [isDisliked, setIsDisLiked] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [replyModal, setReplyModal] = useState(false);
  const [likeDislikeCount, setLikeDislikeCount] = useState({
    like: 0,
    dislike: 0,
  });
  const [childCommentList, setChildCommentList] = useState([]);

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      // if (props.data.child_comments.length !== 0) {
      //   setChildCommentList(props.data.child_comments);
      // }

      if (props.data.is_like_by_me === true) {
        setIsLiked(true);
      }
      if (props.data.is_dislike_by_me === true) {
        setIsDisLiked(true);
      }
      setLikeDislikeCount({
        like: props.data.like,
        dislike: props.data.dislike,
      });
    }

    return () => (mounted = false);
  }, []);

  const likeHandler = async (commentId) => {
    if (!isliked) {
      setIsLiked(true);
      setIsDisLiked(false);

      // updating like count locally
      if (isDisliked) {
        setLikeDislikeCount({
          like: likeDislikeCount.like + 1,
          dislike:
            likeDislikeCount.dislike === 0
              ? null
              : likeDislikeCount.dislike - 1,
        });
      } else {
        if (isliked) {
          return null;
        } else {
          setLikeDislikeCount({
            like: likeDislikeCount.like + 1,
            dislike: likeDislikeCount.dislike,
          });
        }
      }

      // like api call
      const Body = {
        comment_id: commentId,
        is_like: 1,
        typelike: "",
      };
      const apiBody = JSON.stringify(Body);
      const postId = props.postId;

      await LikeDislikeApi(postId, apiBody);
    } else {
      setIsLiked(false);
      setLikeDislikeCount({
        like: likeDislikeCount.like === 0 ? null : likeDislikeCount.like - 1,
        dislike: likeDislikeCount.dislike,
      });

      // like api call
      const Body = {
        comment_id: commentId,
        is_like: 1,
        typelike: "remove",
      };
      const apiBody = JSON.stringify(Body);
      const postId = props.postId;

      await LikeDislikeApi(postId, apiBody);
    }
  };

  const dislikeHandler = async (commentId) => {
    if (!isDisliked) {
      setIsDisLiked(true);
      setIsLiked(false);

      // updating dislike count locally
      if (isliked) {
        setLikeDislikeCount({
          like: likeDislikeCount.like === 0 ? null : likeDislikeCount.like - 1,
          dislike: likeDislikeCount.dislike + 1,
        });
      } else {
        if (isDisliked) {
          return null;
        } else {
          setLikeDislikeCount({
            like: likeDislikeCount.like,
            dislike: likeDislikeCount.dislike + 1,
          });
        }
      }

      // dislike api call
      const Body = {
        comment_id: commentId,
        is_like: 0,
        typelike: "",
      };
      const apiBody = JSON.stringify(Body);
      const postId = props.postId;

      await LikeDislikeApi(postId, apiBody);
    } else {
      setIsDisLiked(false);
      setLikeDislikeCount({
        like: likeDislikeCount.like,
        dislike:
          likeDislikeCount.dislike === 0 ? null : likeDislikeCount.dislike - 1,
      });

      // dislike api call
      const Body = {
        comment_id: commentId,
        is_like: 0,
        typelike: "remove",
      };
      const apiBody = JSON.stringify(Body);
      const postId = props.postId;

      await LikeDislikeApi(postId, apiBody);
    }
  };

  const toggleModal = () => {
    if (props.userId === props.data.commented_by) {
      setModalVisible(!modalVisible);
    } else {
      null;
    }
  };
  const toggleReplyModal = () => {
    setReplyModal(!replyModal);
  };

  const imageLink = (props) => {
    if (props.data.commented_by_image !== undefined) {
      if (props.data.commented_by_image[0] === "/") {
        return `http://111.93.169.90:8008${props.data.commented_by_image}`;
      } else {
        return `${props.data.commented_by_image}`;
      }
    }
  };

  return (
    <TouchableOpacity delayLongPress={500} onLongPress={toggleModal}>
      <View style={styles.commentContainer}>
        {/* user image view */}
        <Modal
          modalVisible={modalVisible}
          toggleModal={toggleModal}
          image={props.data.commented_by_image}
          commentText={props.data.text}
          postId={props.postId}
          commentId={props.data.id}
          token={props.token}
          resetCommentList={props.resetComment}
          editCommentList={props.editComment}
        />
        <ReplyCommentModal
          modalVisible={replyModal}
          toggleModal={toggleReplyModal}
          image={props.data.commented_by_image}
          data={props.data.child_comments}
          postId={props.postId}
          commentId={props.data.id}
          token={props.token}
          editReplyCommentList={props.editReplyComment}
        />
        <View style={styles.imageView}>
          <Image source={{ uri: imageLink(props) }} style={styles.userImage} />
          {/* <Image
            source={
              props.data.commented_by_image === undefined
                ? require("./../../../../assets/images/avatar.png")
                : { uri: props.data.commented_by_image }
            }
            style={styles.userImage}
          /> */}
        </View>
        {/* user comment view */}
        <View style={styles.commentView}>
          <Text style={styles.userNameText}>
            {props.data.commented_by_name}
          </Text>

          <Text style={styles.commentText}>{props.data.text}</Text>
          {/* like share button section */}
          {props.userId ? (
            <View style={styles.likeShareBtnSection}>
              <TouchableOpacity
                onPress={() => likeHandler(props.data.id)}
                style={styles.iconBtns}
              >
                <Icon
                  type="antdesign"
                  name="like1"
                  color={isliked ? "#66001a" : "#ccc"}
                  size={16}
                />
                <Text style={styles.iconTextColor}>
                  {likeDislikeCount.like}
                </Text>
                {/* <Text style={styles.iconTextColor}>{props.data.like}</Text> */}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => dislikeHandler(props.data.id)}
                style={styles.iconBtns}
              >
                <Icon
                  type="antdesign"
                  name="dislike1"
                  color={isDisliked ? "#66001a" : "#ccc"}
                  size={16}
                  style={{ transform: [{ rotateY: "180deg" }] }}
                />
                <Text style={styles.iconTextColor}>
                  {likeDislikeCount.dislike}
                </Text>
                {/* <Text style={styles.iconTextColor}>{props.data.dislike}</Text> */}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={toggleReplyModal}
                style={styles.iconBtns}
              >
                <Icon
                  type="font-awesome"
                  name="share"
                  color={false ? "#66001a" : "#ccc"}
                  size={16}
                />
              </TouchableOpacity>
            </View>
          ) : null}

          <TouchableOpacity
            style={styles.childCommentView}
            onPress={() => setReplyModal(true)}
          >
            {props.data.child_comments.length !== 0 ? (
              <Text numberOfLines={1} style={styles.childCommenText}>
                ({props.data.child_comments.length}) Replies
              </Text>
            ) : null}
          </TouchableOpacity>
        </View>
      </View>
    </TouchableOpacity>
  );
}
