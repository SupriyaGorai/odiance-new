import React from "react";
import { View, Text, ActivityIndicator } from "react-native";

export default function Indicator() {
  return (
    <View
      style={{
        height: '100%',
        width:'100%',
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#00000050",
      }}
    >
      <ActivityIndicator color={"#fff"} size={"large"} />
    </View>
  );
}
