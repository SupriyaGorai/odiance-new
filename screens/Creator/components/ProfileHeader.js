import React, { Component } from 'react';
import { Text } from 'react-native';
import { Header } from 'react-native-elements';
import styles from './style';
import { View } from 'native-base';
import { Icon } from 'react-native-elements';

class TextHeader extends Component {
  constructor(props) {
    super(props);
  }
  render() {

    const txt = this.props.header;
    return <Text style={styles.headerText}>{txt}</Text>;
  }
}

export default class ProfileHeader extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const navigation = this.props.navigation;
    const header = this.props.header;
    return (
      <Header
        containerStyle={styles.notificationHeader}
        centerComponent={<TextHeader header={header} />}
        centerContainerStyle={styles.menuIcon}
        leftComponent={
          <View style={styles.icon} onPress={()=>alert("hi")}>
            <Icon
              name='arrowleft' 
              type='antdesign'
              size={20}
              color='#fff'
              
              // onPress={() => { navigation.goBack() } /*{navigation.navigate('profile')}*/} 
              />
          </View>
        }
        leftContainerStyle={styles.menuIcon}
      />
    );
  }
}
