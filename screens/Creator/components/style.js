
import { heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {Platform} from 'react-native'
export default styles = {
    homeHeader: {
        height: hp(5),
        backgroundColor: '#ffff',
    },
    menuIcon: {
        marginTop: hp(50),
        marginBottom: hp(50),
        marginLeft: wp(3),
        position: 'absolute'
        // backgroundColor: "#ff0000"
        
    },
    notificationHeader: {
        height: Platform.OS=="ios" ? 90 : 60,
        backgroundColor: '#66001a',
        width : '100%'
    },
    headerText: {
        color: '#ffff',
        marginLeft: wp(12),
        fontSize: hp(3),
        
    },
    icon : {
        justifyContent:'center',
        width : wp('10%'),
        right:8,
       
        // top:5,
    },
};

