import React from 'react';
import {View, Image, Text, ScrollView, Dimensions} from 'react-native';
import {
  PostThumbNail,
  DynamicThumbNail,
} from '../../Viewer/components/PostThumbnail';
import style from './style';
import ProfileHeader from '../components/ProfileHeader';
const {height, width} = Dimensions.get('window');
import {
  listenOrientationChange as loc,
  removeOrientationListener as rol,
} from 'react-native-responsive-screen';

export default class InspirationScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    loc(this);
  }

  componentWillUnMount() {
    rol();
  }

  render() {
    const videos = [
      {thumb: require('../../../assets/images/bhai.png'), views: '12.5k'},
      {thumb: require('../../../assets/images/shakira.jpeg'), views: '30.5k'},
      {thumb: require('../../../assets/images/Dwayne.jpg'), views: '22.5k'},
      {thumb: require('../../../assets/images/john.jpeg'), views: '2.5k'},
      {thumb: require('../../../assets/images/selina.jpeg'), views: '2k'},
      {thumb: require('../../../assets/images/bhai.png'), views: '12.5k'},
      {thumb: require('../../../assets/images/shakira.jpeg'), views: '30.5k'},
      {thumb: require('../../../assets/images/Dwayne.jpg'), views: '22.5k'},
      {thumb: require('../../../assets/images/john.jpeg'), views: '2.5k'},
      {thumb: require('../../../assets/images/selina.jpeg'), views: '2k'},
      {thumb: require('../../../assets/images/selina.jpeg'), views: '2k'},
      {thumb: require('../../../assets/images/selina.jpeg'), views: '2k'},
      {thumb: require('../../../assets/images/selina.jpeg'), views: '2k'},
      {thumb: require('../../../assets/images/selina.jpeg'), views: '2k'},
      {thumb: require('../../../assets/images/selina.jpeg'), views: '2k'},
      {thumb: require('../../../assets/images/selina.jpeg'), views: '2k'},
    ];
    const navigation = this.props.navigation;
    return (
      <ScrollView>
        <ProfileHeader header="Inspiration" navigation={navigation} />
        <PostVerticalScroll post={videos} />
      </ScrollView>
    );
  }
}
export class PostVerticalScroll extends React.Component {
  componentDidMount() {
    loc(this);
  }

  componentWillUnMount() {
    rol();
  }

  render() {
    const {post, navigate} = this.props;
    // console.log(post,'postverticalscroll')

    return (
      <ScrollView>
        <View style={style.mainView}>
          {post.map((data, key) => {
            return (
              <View key={key} style={style.allimageView}>
                <View style={style.imageView}>
                  <DynamicThumbNail
                    image={data.thumbnail}
                    navigate={navigate}
                    id={data.id}
                  />
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
    );
  }
}
