import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  PermissionsAndroid,
  Animated,
  ToastAndroid,
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import { Icon, ThemeConsumer } from 'react-native-elements';
import AnimatedProgressWheel from 'react-native-progress-wheel';
import { getInitialState } from '../../../_actions/LoginAction';
import { connect } from 'react-redux';
import Video from 'react-native-video';
import { ActivityIndicator } from 'react-native-paper';
import { CommonActions } from '@react-navigation/native';
import ImagePicker from 'react-native-image-crop-picker';
const flashModeOrder = {
  off: 'on',
  on: 'torch',
  auto: 'auto',
  torch: 'off',
};
const wbOrder = {
  auto: 'sunny',
  sunny: 'cloudy',
  cloudy: 'shadow',
  shadow: 'fluorescent',
  fluorescent: 'incandescent',
  incandescent: 'auto',
};

const landmarkSize = 2;
import uploadVideo from '../../../_services/PostVideoServices'
import { createThumbnail } from "react-native-create-thumbnail";
export class CameraScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      flash: 0,
      zoom: 0,
      autoFocus: 'on',
      autoFocusPoint: {
        normalized: { x: 0.5, y: 0.5 }, // normalized values required for autoFocusPointOfInterest
        drawRectPosition: {
          x: Dimensions.get('window').width * 0.5 - 32,
          y: Dimensions.get('window').height * 0.5 - 32,
        },
      },
      depth: 0,
      curTime: 0,
      type: 'back',
      whiteBalance: 'auto',
      ratio: '16:9',
      recordOptions: {
        mute: false,
        maxDuration: 100,
        quality: RNCamera.Constants.VideoQuality['288p'],
        path: null,
      },
      video: null,
      isRecording: false,
      timerStart: false,
      stopwatchStart: false,
      totalDuration: 100,
      timerReset: false,
      stopwatchReset: false,
      timerOn: false,
      timerStarted: 0,
      timerTime: 0,
      isRecorded: false,
      videoUrl: null,
      thumbnail: null,
      serverUrl: null,
      time: 0,
      paused: true,
      isUploading: false,
      resetTo: false,
    };
    this.toggleTimer = this.toggleTimer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
    this.toggleStopwatch = this.toggleStopwatch.bind(this);
    this.resetStopwatch = this.resetStopwatch.bind(this);
    this.takeVideo = this.takeVideo.bind(this);
  }
  startTimer = () => {
    this.setState({
      timerOn: true,
      timerTime: this.state.timerTime,
      timerStart: Date.now() - this.state.timerTime
    });
    this.timer = setInterval(() => {
      this.setState({
        timerTime: Date.now() - this.state.timerStarted
      });
    }, 10);
  };
  toggleFacing() {
    this.setState({
      type: this.state.type === 'back' ? 'front' : 'back',
    });
  }

  toggleFlash() {
    console.log(this.state.flash);
    console.log(RNCamera.Constants.FlashMode)
    let tstate = this.state.flash;
    if (tstate == RNCamera.Constants.FlashMode.off) {
      tstate = RNCamera.Constants.FlashMode.torch;
    } else {
      tstate = RNCamera.Constants.FlashMode.off;
    }
    this.setState({ flash: tstate })
  }

  toggleWB() {
    this.setState({
      whiteBalance: wbOrder[this.state.whiteBalance],
    });
  }

  toggleFocus() {
    this.setState({
      autoFocus: this.state.autoFocus === 'on' ? 'off' : 'on',
    });
  }
  //   getFormattedTime() {
  //     this.state.time = this.currentTime;
  // };
  touchToFocus(event) {
    const { pageX, pageY } = event.nativeEvent;
    const screenWidth = Dimensions.get('window').width;
    const screenHeight = Dimensions.get('window').height;
    const isPortrait = screenHeight > screenWidth;

    let x = pageX / screenWidth;
    let y = pageY / screenHeight;
    // Coordinate transform for portrait. See autoFocusPointOfInterest in docs for more info
    if (isPortrait) {
      x = pageY / screenHeight;
      y = -(pageX / screenWidth) + 1;
    }

    this.setState({
      autoFocusPoint: {
        normalized: { x, y },
        drawRectPosition: { x: pageX, y: pageY },
      },
    });
  }
  reset = (name) => {
    const resetAction = CommonActions.reset({
      index: 0,
      routes: [
        { name: name }
      ],
    });
    this.props.navigation.dispatch(resetAction);

  }
  stopTimer = () => {
    //this.setState({ timerOn: false });
    clearInterval(this.timer);
  };
  resetTimer1 = () => {
    this.setState({
      timerStarted: 0,
      timerTime: 0
    });
  };
  resetDuration() {
    this.setState({ curTime: 0 });
  }

  zoomOut() {
    this.setState({
      zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1,
    });
  }

  zoomIn() {
    this.setState({
      zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1,
    });
  }
  async getPermmissions() {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          {
            title: 'Permissions for read access',
            message: 'Give permission to your storage to read a file',
            buttonPositive: 'ok',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the storage');
        } else {
          console.log('permission denied');
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }
  
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Permissions for write access',
            message: 'Give permission to your storage to write a file',
            buttonPositive: 'ok',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the storage');
        } else {
          console.log('permission denied');
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
          {
            title: 'Permissions for read access',
            message: 'Give permission to your storage to read a file',
            buttonPositive: 'ok',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the storage');
        } else {
          console.log('permission denied');
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Permissions for write access',
            message: 'Give permission to your storage to write a file',
            buttonPositive: 'ok',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera');
        } else {
          console.log('permission denied');
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }

    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
          {
            title: 'Permissions for write access',
            message: 'Give permission to your storage to write a file',
            buttonPositive: 'ok',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera');
        } else {
          console.log('permission denied');
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }
  }
    // const path = Platform.select({
    //   ios: 'hello.m4a',
    //   android: 'sdcard/hello.mp4',
    // });
    // this.setState({
    //   recordOptions: {
    //     path: path,
    //   },
    // });
    async takeVideo() {
   const image = await  ImagePicker.openCamera({
      mediaType: 'video',
      compressVideoPreset : 'low'
    })
    if(image)
    {
      this.setState({videoUrl : image})
    }
    // }).then(image => {
    //   this.setState({videoUrl : image})
    // });
    console.log(this.state.videoUrl);
    return this.state.videoUrl ;
  }

    // if (this.camera) {
    //   try {
    //     const promise = this.camera.recordAsync(this.state.recordOptions);

    //     if (promise) {
    //       this.setState({ isRecording: true });
    //       const data = await promise;
    //       this.setState({ isRecording: false });
    //       this.setState({ videoUrl: data });
    //       console.log(this.state.videoUrl, 'sssss');
    //       return this.state.videoUrl;
    //     }
    //   } catch (e) {
    //     console.error(e);
    //   }
    // }
  toggleTimer() {
    this.setState({ timerStart: !this.state.timerStart, timerReset: false });
  }

  resetTimer() {
    this.setState({ timerStart: false, timerReset: true });
  }

  toggleStopwatch() {
    this.setState({
      stopwatchStart: !this.state.stopwatchStart,
      stopwatchReset: false,
    });
  }
  async onButtonPress(status) {
   // if (!this.state.isRecording) {
     // if (this.state.curTime < 120) {
     await this.getPermmissions() ;
        const video = await this.takeVideo();
       // this.toggleStopwatch();
        this.setState({ video: video });
        console.log(this.state.video, 'response');
        //this.updateTimer();
     // }
   // }
    if (this.state.isRecording) {
      this.camera.stopRecording();
      this.toggleStopwatch();
      this.setState({ isRecorded: true });
      this.resetTimer();
    }

  }

  resetStopwatch() {
    this.setState({ stopwatchStart: false, stopwatchReset: true });
  }
  if(time) {
    getFormattedTime(time)
    {
      this.currentTime = time;
      console.log(time, 'total time');
    }
  }
  if(time) {
    getMsecs(time)
    {
      console.log(time);
    }

  }
  updateTimer() {
    setInterval(() => {
      this.setState({
        curTime: this.state.curTime + 1
      })
    }, 1000)
    console.log(this.state.curTime);
  }

  renderCamera(status) {
    const drawFocusRingPosition = {
      top: this.state.autoFocusPoint.drawRectPosition.y - 32,
      left: this.state.autoFocusPoint.drawRectPosition.x - 32,
    };
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
          justifyContent: 'space-between',
        }}
        type={this.state.type}
        flashMode={this.state.flash}
        autoFocus={this.state.autoFocus}
        autoFocusPointOfInterest={this.state.autoFocusPoint.normalized}
        zoom={this.state.zoom}
        whiteBalance={this.state.whiteBalance}
        ratio={this.state.ratio}
        focusDepth={this.state.depth}

        faceDetectionLandmarks={
          RNCamera.Constants.FaceDetection.Landmarks
            ? RNCamera.Constants.FaceDetection.Landmarks.all
            : undefined
        }>
        <View style={{
          position: 'absolute',
          bottom: 0,
          width: '100%',
          height: 100,
          backgroundColor: '#00000050',
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'row'
        }}>
          <TouchableOpacity onPress={() => this.toggleFlash()} style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
            <Image source={require('../../../assets/icons/flash.png')}
              style={{ height: 30, width: 30 }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.toggleWB()} style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
          <Image source={require('../../../assets/icons/wb.png')} style={{ height: 40, width: 40 }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.onButtonPress(status)} style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
            <Image source={require('../../../assets/icons/middle-button.png')} style={{ height: 60, width: 60 }} />
            {this.state.isRecording ?
              <View style={{ position: 'absolute', top: -12 }}>
                <AnimatedProgressWheel
                  progress={100}
                  animateFromValue={0}
                  duration={15000}
                  size={84}
                  width={10}
                  height={10}
                  color={'yellow'}
                  fullColor={'yellow'}
                  style={{ alignSelf: 'center' }}
                />
              </View>
              : null}
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}></TouchableOpacity>
          <TouchableOpacity onPress={() => this.toggleFacing()} style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }}>
          <Image source={require('../../../assets/icons/flip_camera.png')} style={{ height: 40, width: 40 }} />
          </TouchableOpacity>
        </View>
      </RNCamera>
    );
  }

  async uploadMaterial(status) {
    this.setState({ isUploading: true });
    const response = await uploadVideo(this.state.video, status.token, 'draft')
    if (response) {
      console.log(response);
      this.setState({ serverUrl: response.link });
      this.setState({ isUploading: false })
    }
  }
  async uploadVideoServices(status, navigation) {
    // this.setState({isUploading : true});
    // const response = await uploadVideo(this.state.video,status.token,'video');
    // if(response)
    // { console.log(response);
    //   this.setState({serverUrl : response.link});
    // }
    //   this.setState({isUploading : false})
    navigation.navigate('Post', { videoUrl: this.state.video })
  }
  renderVideo(status, navigation) {
    return (
      <View>
        <Video source={{ uri: `${this.state.video.path}` }}   // Can be a URL or a local file.
          ref={(ref) => {
            this.player = ref
          }}
          tapAnywheretoPause={true}
          paused={this.state.paused}
          resizeMode='contain'                                     // Store reference
          onBuffer={this.onBuffer}                // Callback when remote video is buffering
          onError={this.videoError}
          onEnd={() => this.setState({ paused: true })}
          style={{ height: '100%', width: '100%' }}>
        </Video>
        {this.state.paused ?
          <TouchableOpacity style={{ alignSelf: 'center', top: 350, position: 'absolute', width: 60, }}
            onPress={() => this.setState({ paused: !this.state.paused })}  >
            <Icon name='play' type='font-awesome' size={50} color='#ffff' />
          </TouchableOpacity>
          : null}
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            width: '100%',
            height: 100,
            backgroundColor: '#00000060',
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingHorizontal:50
          }}>
          <TouchableOpacity onPress={() => this.uploadMaterial(status)}>
            <Icon name='download' type='font-awesome' color='#ffff' size={30} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.uploadVideoServices(status, navigation)}>
            <Icon name='paper-plane' type='font-awesome' color='#ffff' size={30}
              onPress={() => this.uploadVideoServices(status, navigation)} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.setState({ resetTo: true })}>
            <Icon name='trash-o' type='font-awesome' color='#ffff' size={30}
              onPress={() => this.reset('Camera')} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  render() {
    const { status, navigation } = this.props;
    return (
      <View style={styles.container}>
        {!this.state.video ?
          this.renderCamera(status)
          : this.renderVideo(status, navigation)
        }
      </View>
    );
  }
}
function mapStatetoProps(state) {
  return {
    status: state.user,
  };
}

export default connect(
  mapStatetoProps
)(CameraScreen);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: '#000',
  },
  flipButton: {
    flexDirection: 'row',
    height: 40,
    width: 40,
    alignSelf: 'flex-end',
    borderRadius: 40,
    borderColor: 'white',
    borderWidth: 1,
    padding: 5,
    top: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  autoFocusBox: {
    position: 'absolute',
    height: 64,
    width: 64,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: 'white',
    opacity: 0.4,
  },
  flipText: {
    color: 'white',
    fontSize: 15,
  },
  zoomText: {
    position: 'absolute',
    bottom: 70,
    zIndex: 2,
    left: 2,
  },
  picButton: {
    backgroundColor: 'darkseagreen',
  },
  facesContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
  },
  progress: {
    bottom: 10,
    position: 'absolute',
    left: 0,
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  face: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#FFD700',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  landmark: {
    width: landmarkSize,
    height: landmarkSize,
    position: 'absolute',
    backgroundColor: 'red',
  },
  faceText: {
    color: '#FFD700',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
    backgroundColor: 'transparent',
  },
  text: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#F00',
    justifyContent: 'center',
  },
  textBlock: {
    color: '#F00',
    position: 'absolute',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
  backgroundVideo: {
    // height : '100%',
    // width : '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },


});
const options = {
  container: {
    backgroundColor: '#000',
    padding: 5,
    borderRadius: 5,
    width: 220,
  },
  text: {
    fontSize: 30,
    color: '#FFF',
    marginLeft: 7,
  }
};
