import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput,
  PermissionsAndroid,
} from 'react-native';
import ProfileHeader from '../components/ProfileHeader';
import { CommonActions } from '@react-navigation/native';
import Colors from '../../../Constants/Colors';
import style from './style.js';
import { Icon } from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { connect } from 'react-redux';
import post from '../../../_actions/PostAction';
import { getInitialState } from '../../../_actions/LoginAction';
import MapboxGL from '@react-native-mapbox-gl/maps';
import Config from '../../../config';
import { createThumbnail } from "react-native-create-thumbnail";
import uploadVideo from '../../../_services/PostVideoServices'
import { GET, POST } from '../../../_services/services';
import urls from '../../../Constants/urls';
import ActivityIndicator from '../../Viewer/components/ActivityIndicator';
import ImagePicker from 'react-native-image-crop-picker';
MapboxGL.setAccessToken(Config.accessToken)
class PostScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      caption: "",
      latitude: null,
      longitude: null,
      video_url: null,
      thumbnail: null,
      post_credits: [],
      isUploading: false,
      userDetails: [],
      hashtag: ''
    };
    this.getUserLocation = this.getUserLocation.bind(this);
  }
  componentDidMount = async () => {
    PermissionsAndroid.requestMultiple(
      [
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
      ],
      {
        title: "Odiance needs location permission.",
        message: "Please allow Odiance to read your current location."
      }
    ).then(onGranted => {
      console.log('permission request granted', onGranted);
    }).catch(err => {
      console.log('permission request rejected', err);
    });
    console.log("->", this.props.status);

    await GET(`user/${this.props.status.id}`, this.props.status.token).then((response) => {
      console.log(response);
      this.setState({ userDetails: response.data })
    });
  }

  reset = (name) => {
    const resetAction = CommonActions.reset({
      index: 0,
      routes: [
        { name: name }
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }

  videoPost = async (thumbnail, videoUrl) => {
    var formData = {
      creator: this.props.status.id,
      caption: this.state.caption,
      latitude: this.state.latitude,
      longitude: this.state.longitude,
      video_url: videoUrl,
      video_id: "",
      thumbnail: thumbnail,
      post_credits: this.state.post_credits,
      hashtag: this.state.hashtag,
      credits: "",
      is_private: 0
    };
    console.log(formData);
    await POST(`${urls.post}`, this.props.status.token, formData).then((response) => {
      console.log(response);
      if (response.status === 200) {
        alert("Video Uploaded Successfully")
        this.props.navigation.navigate('Home');
      }
    });
  }

  uploadThumb = async (image, videoUrl) => {
    const Photo_pic = {
      name: `${new Date().getTime()}.jpeg`,
      type: 'image/jpeg',
      uri: image,
    };
    const formData = new FormData();
    formData.append('avatar', Photo_pic);
    formData.append('image_type', 'thumbnail');
    await fetch(`${urls.base}${urls.avatar}`, {
      method: 'POST',
      headers: {
        Authorization: `Token ${this.props.status.token}`,
      },
      body: formData,
    }).then(response => response.json()).then(responseData => {
      console.log("3", image, responseData);
      this.videoPost(responseData.link, videoUrl);
    }).catch(err => {
      console.log(err);
    });
  }

  createImage = async (videoUrl) => {
    await createThumbnail({
      url: videoUrl,
      timeStamp: 10000,
    }).then((response) => {
      console.log("2", response);
      this.uploadThumb(response.path, videoUrl);
    }).catch(err => {
      console.log(err);
    });
  }

  onPress = async () => {
    this.setState({ isUploading: true });
    await uploadVideo(this.props.route.params.videoUrl, this.props.status.token, 'video').then(response => {
      console.log("1", response);
      this.createImage(response.link);
    }).catch(err => {
      console.log(err);
    });
    ImagePicker.clean().then(() => {
      console.log('removed all tmp images from tmp directory');
    }).catch(e => {
      alert(e);
    });
    this.setState({ isUploading: false });
  }

  getUserLocation = async (location) => {
    this.setState({
      timestamp: location.timestamp,
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    });
    var result = await this._map.queryRenderedFeaturesAtPoint(location)
    console.log(result);
  }

  render() {
    const user = this.state.userDetails;
    const navigation = this.props.navigation;
    const { status } = this.props;
    const video_url = this.props.route.params.videoUrl;

    return (
      <ScrollView>
        <ProfileHeader header="Create Post" navigation={navigation} />
        {
          this.state.isUploading ?
            <ActivityIndicator/>
            :
            <View>
              <View style={{ padding: 15 }}>
                <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                {user.avatar ? 
              user.avatar.charAt(0)=== 'h'
              ? 
              (
              <Image
                source={{ uri: `${user.avatar}` }}
                style={style.userImage}
              />
             ) :(
              <Image
                source={{ uri: `${urls.avatarbase}${user.avatar}` }}
                style={style.userImage}
              />
             ) 

             : (
              <Image
                //source = {{uri : `${urls.avatarbase}${image.avatar}`}}
                source={require("../../../assets/images/shakira.jpeg")}
                style={style.userImage}
              />
            )}
                  {/* <Image source={{ uri: `${user.avatar}` }} style={style.userImage} /> */}
                  <View style={{ flexDirection: 'column', left: 20 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 24, color: '#131313' }}>
                      {user.first_name} {user.last_name}
                    </Text>

                    <View style={{ flexDirection: 'row' }}>
                      <Text style={{ fontSize: 15, color: '#131313' }}>
                        Alumni
                  </Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{ padding: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                  {this.state.thumbnail ?
                    <Image source={{ uri: `${this.state.thumbnail.uri}` }} style={{ width: 80, height: 80, borderRadius: wp(3) }} /> : null}
                  <View style={{ width: '100%' }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 18, color: 'black', marginBottom: 10 }}>
                      Description
                </Text>

                    <View style={{ borderColor: Colors.primary, borderWidth: 1 }}>
                      <TextInput
                        style={{ fontSize: 15, color: '#949494', width: '100%', padding: 10 }}
                        placeholder={"Enter the caption for your video here."}
                        onChangeText={caption => this.setState({ caption })}
                        multiline={true}
                        numberOfLines={8}
                        returnKeyType="done"
                        scrollEnabled={true}
                      />
                    </View>
                  </View>

                </View>
              </View>

              <View style={style.attachContainer}>
                <MapboxGL.MapView styleURL={MapboxGL.StyleURL.Dark} ref={c => (this._map = c)}>
                  <MapboxGL.UserLocation
                    ref={d => (this._location = d)}
                    visible={false}
                    onUpdate={this.getUserLocation}
                  />
                </MapboxGL.MapView>
              </View>

              <View>
                <View style={{ flexDirection: 'column', padding: 15 }}>
                  <View style={{ marginBottom: 5 }}>
                    <Text style={{ fontWeight: 'bold', fontSize: 18, marginBottom: 10 }}>Share</Text>
                  </View>
                  <View style={style.shareIcons}>
                    <TouchableOpacity style={style.shareIconFB}>
                      <Icon type="FontAwesome" name="facebook" style={{ color: '#fff' }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.shareIconTW}>
                      <Icon type="FontAwesome" name="twitter" style={{ color: '#fff' }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.shareIconIN}>
                      <Icon type="FontAwesome" name="instagram" style={{ color: '#fff' }} />
                    </TouchableOpacity>
                    <TouchableOpacity style={style.shareIconYT}>
                      <Icon type="FontAwesome" name="youtube" style={{ color: '#fff' }} />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              <View style={style.submitView}>
                <TouchableOpacity style={style.submit} onPress={() => this.onPress()}>
                  <Text style={style.submitText}>POST</Text>
                </TouchableOpacity>
              </View>

            </View>
        }
      </ScrollView>
    );
  }
}

function mapStatetoProps(state) {
  return {
    Post_status: state.post,
    status: state.user,
    // code: state.code,
  };
}

const actionsPost = {
  getInitialState: getInitialState,
  post: post
}

export default connect(
  mapStatetoProps,
  actionsPost
)(PostScreen);
