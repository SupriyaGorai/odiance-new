import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../../Constants/Colors';
export default {
  mainImgRow: {
    height: 150,
  },
  nextImgRow: {
    width: 120,
  },
  headerRow: {
    height: 50,
    backgroundColor: Colors.white,
    borderBottomColor: '#2F2E31',
    borderBottomWidth: 0.4,
  },
  imageBorder: {
    height: 45,
    width: 45,
    borderRadius: wp(45 / 2),
    backgroundColor: '#C2C2C2',
    alignItems: 'center',
    justifyContent: 'center',
  },

  Textarea: {
    width: 150,
  },
  attachContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    marginHorizontal: wp('8%'),
    //flexDirection : 'column',
    // marginTop: '52%',
    // marginBottom: '15%',
    // top: '5%',
    // width: 'auto',
    // height: hp('60%'),
    // bottom: hp('5%'),
  },
  mainImage: {
    height: 100,
    width: 100,
    borderRadius: wp(110 / 2),
    backgroundColor: '#C2C2C2',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageSquare: {
    height: 80,
    width: 100,
    borderRadius: 10,
  },
  creditRow: {
    flexDirection: 'column',
  },
  creditImage: {
    borderWidth: 1,
    borderRadius: wp(150 / 2),
    height: 50,
    width: 50,
    borderColor: '#2D2D2D',
    backgroundColor: '#2F2E31',
  },
  imageCircle: {
    borderWidth: 1,
    borderRadius: wp(90 / 2),
    height: 90,
    width: 90,
    borderColor: '#2D2D2D',
    backgroundColor: '#2F2E31',
  },
  musicRow: {
    alignSelf: 'center',
  },
  Text: {
    fontSize: 100,
    color: '#555555',
    margin: 10,
  },
  mainContainer: {
    paddingHorizontal: wp('3%'),
  },
  submit: {
    backgroundColor: '#80131e',
    borderWidth: 1,
    borderColor: '#80131e',
    borderRadius: 35,
    margin: 10,
    height: 50,
    width: wp(100) - 40,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20
  },
  icon: {
    fontSize: 10,
    color: 'black',
  },
  emptyRow: {
    alignSelf: 'center',
    height: 90,
  },
  colStyle : {
   width : 110,
  },
  mainText : {
    justifyContent: 'center', flex: 0.6
  },

  name :{
    fontWeight: 'bold',
    right: wp('1%'),
  },

  text : 
  {
      fontWeight : 'bold',
    },
    dummyText : {
        color: '#616161'
    },
    row : {
        
            borderBottomColor: '#2F2E31',
            borderBottomWidth: 0.4,

    },
    creditText : {
        color: '#000', fontSize: 16, fontWeight: 'bold'
    },
    creditName : {
        
            color: '#616161',
            fontSize: 14,
            fontWeight: 'bold',
          
    },
    shareIcons :{
        flexDirection: 'row',
        
       
      },
      submitText : {
        color : '#fff',
        fontWeight : 'bold',
        fontSize:18,
      },
      submitView : {
        alignSelf: 'center',
        padding: 10,
      },
      userImage: {
        width: 80,
        height: 80,
        aspectRatio: 1,
        borderRadius: wp(10),
        borderWidth: wp(1.5),
        borderColor: '#d1d1d1',
      },
      shareIconFB: {
        aspectRatio: 1,
        backgroundColor: '#3b5998',
        width: 40,
        height: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 0,
        borderRadius: wp(10),
      },
      shareIconTW: {
        aspectRatio: 1,
        backgroundColor: '#00acee',
        width: 40,
        height: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        borderRadius: wp(10),
      },
      shareIconIN: {
        aspectRatio: 1,
        backgroundColor: '#C13584',
        width: 40,
        height: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        borderRadius: wp(10),
      },
      shareIconYT: {
        aspectRatio: 1,
        backgroundColor: '#c4302b',
        width: 40,
        height: 'auto',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        borderRadius: wp(10),
      }
};
