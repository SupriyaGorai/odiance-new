import { HistoryConstants } from "./../_constants/HistoryConstants";

const initialState = {
  searchText: "",
};

export const HistoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case HistoryConstants.searchText:
      return {
        ...state,
        searchText: action.payload,
      };
    default:
      return state;
  }
};
