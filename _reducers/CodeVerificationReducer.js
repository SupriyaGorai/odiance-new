export default function codeReducer (state=initialStates,action) { 
    console.log(action.type)
    switch(action.type){
        case 'CODE_REQUEST':
        return { ...state,
            loading : true, code: action.payload.code, message: '', valid: false, email: ''}
        case 'CODE_SUCCESS' :
         return { ...state,
             loading: false, code: action.payload.code, message: '', valid: true, email: action.payload.email, register: false}
         case 'CODE_FAILURE' :
            return { ...state,
                loading: false, message: action.payload.message, valid: false, email: ''}
        case 'REGISTER_OPENED':
            return { ...state,
                register: true,
            }
            case 'REMOVE_MESSAGE':
                return { ...state,
                    message : '',
                }
               
         case 'default':
             return state   

    }
    return state;
  };
  const initialStates = {
    loading : false,
    code: '',
    message: '',
    valid: false,
    email: '',
    register: false,
};