import { HomeConstants } from "../_constants";

const toogleReducer = (
  state = {
    enabled: false,
    date: "",
    sliderValue: 50,
    searchText: "",
  },
  action
) => {
  switch (action.type) {
    case HomeConstants.toggleSearch:
      return {
        ...state,
        enabled: !state.enabled,
      };
    case HomeConstants.dateValue:
      return {
        ...state,
        date: action.payload,
      };
    case HomeConstants.sliderValue:
      return {
        ...state,
        sliderValue: action.payload,
      };
    case HomeConstants.searchText:
      return {
        ...state,
        searchText: action.payload,
      };
    default:
      return state;
  }
};

export const HomeReducers = {
  toggleSearch: toogleReducer,
};
