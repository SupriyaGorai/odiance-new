import RegistrationConstants from "../_constants/RegistrationConstants";
import ViewerRegistrationConstants from "../_constants/ViewerRegistrationConstants";
import googleLoginConstants from "../_constants/googleLoginConstants";
import updateuserConstants from "../_constants/updateuserConstants";
import { loginConstants } from "../_constants";

//TODO: check async storage for user data and token
// if present set the intial state to logged in

const statusReducer = (
  state = {
    loggedIn: false,
    loggingIn: false,
    msg: null,
    token: null,
    id: null,
    isCreator: false,
    updating: false,
    updated: false,
    isFollow:false,
  },
  action
) => {
  switch (action.type) {
    case googleLoginConstants.googleLogin_request_request:
      return {
        id: "",
        loggingIn: true,
        loggedIn: false,
        msg: "",
        isCreator: false,
        isFollow:false,
      };

    case googleLoginConstants.googleLogin_success_success:
      return {
        id: "",
        loggingIn: false,
        loggedIn: true,
        msg: action.msg,
        token: action.token,
        // user : action.payload,
        isCreator: false,
        isFollow:false,
      };

    case googleLoginConstants.googleLogin_failure_failure:
      return {
        id: "",
        loggingIn: false,
        loggedIn: false,
        msg: action.error,
        // user : null

        isCreator: false,
        isFollow:false,
      };

    case RegistrationConstants.request:
      return {
        loggingIn: true,
        loggedIn: false,
        msg: "",
        id : "",
        isCreator: false,
        isFollow:false,
      };

    case RegistrationConstants.success:
      return {
        loggingIn: false,
        loggedIn: true,
        msg: action.msg,
        token: action.token,
        id : action.id,
        // user : action.user,
        isCreator: true,
        isFollow:false,
      };

    case RegistrationConstants.failure:
      return {
        loggingIn: false,
        loggedIn: false,
        msg: action.error,
        isCreator: false,
        isFollow:false,
        id : "",
      };
    case ViewerRegistrationConstants.ViewerRegister_request:
      return {
        loggingIn: true,
        loggedIn: false,
        msg: "",
        isCreator: false,
        isFollow:false,
        id : "",
      };

    case ViewerRegistrationConstants.ViewerRegister_success:
      return {
        id: action.id,
        loggingIn: false,
        loggedIn: true,
        msg: action.msg,
        token: action.token,
        isCreator: false,
        isFollow:false,
      };

    case ViewerRegistrationConstants.ViewerRegister_failure:
      return {
        loggingIn: false,
        loggedIn: false,
        msg: action.error,
        // user : null
        id : "",
        isCreator: false,
        isFollow:false,
      };
    case loginConstants.LOGOUT:
      return {
        loggingIn: false,
        loggedIn: false,
        msg: "",
        id : "",
        token: null,
        isCreator: false,
        isFollow:false,
      };

    case loginConstants.GET_INITIAL_STATE:
      return ({
        loggedIn,
        loggingIn,
        token,
        id,
        isCreator,
        msg,
        isFollow,
      } = action.payload);

    case loginConstants.LOGIN_REQUEST:
      return {
        id: "",
        loggingIn: true,
        loggedIn: false,
        msg: "",
        isCreator: false,
        isFollow:false,
      };

    case loginConstants.LOGIN_SUCCESS:
      return {
        loggingIn: false,
        loggedIn: true,
        msg: null,
        id : action.id,
        token: action.token,
        isCreator: action.is_creator,
        isFollow:false,
      };

    case loginConstants.LOGIN_FAILURE:
      return {
        loggingIn: false,
        loggedIn: false,
        msg: null,
        id : "",
        // user : null,
        isCreator: false,
        isFollow:false,
      };

    case updateuserConstants.UPDATE_REQUEST:
      return {
        ...state,
        id : "",
        updating: true,
        updated: false,
        isFollow:false,
        // user:action.payload,
      };

    case updateuserConstants.UPDATE_SUCCESS:
      return {
        ...state,
        id : "",
        updating: false,
        updated: true,
        isFollow:false,
        // user:action.payload,
      };

    case updateuserConstants.UPDATE_FAILURE:
      return { ...state, updating: false, updated: false, user: null,isFollow:false, };
    case "REMOVE_MESSAGE":
      return state;
    case "default":
      return state;

    default:
      return state;
  }
};

export const AuthenticationReducers = {
  status: statusReducer,
};
