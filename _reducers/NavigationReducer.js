import { NavigationConstants } from "../_constants/NavigationConstants";

const RouteReducer = (
  state = {
    routeName : null
  },
  action
) => {
  switch (action.type) {
    case NavigationConstants.routeName:
      return {
        ...state,
        routeName : action.payload,
      };
    default:
      return state;
  }
};

export const NavigationReducer = {
 setRoute : RouteReducer,
};
