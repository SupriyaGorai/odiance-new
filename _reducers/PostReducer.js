import PostConstants from '../_constants/PostConstants';
// import {loginConstants} from '../_constants';

//TODO: check async storage for user data and token
// if present set the intial state to logged in

const statusReducer = (state = {
    user : null,
posting : false,
posted : false}
, action) =>{
    switch(action.type){

        case PostConstants.POST_REQUEST:
            return {
                ...state,
                posting : true,
                posted : false,
                    user:action.payload,
            }
        case PostConstants.POST_SUCCESS:
            return {
                ...state,
                posting : false,
                posted : true,
                    user:action.payload,
            }
            case PostConstants.POST_FAILURE:
                return { ...state,
                    posting : false,
                    posted : false,
                }

        
        
        default:
            return state
    }
}

export const PostReducers = {
    Post_status: statusReducer
}