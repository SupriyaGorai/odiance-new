import { CommentsConstants } from "../_constants/CommentsConstants";
// import {loginConstants} from '../_constants';

//TODO: check async storage for user data and token
// if present set the intial state to logged in

const statusReducer = (
  state = {
    user: null,
    posting: false,
    posted: false,
    commentId: 0,
    newCommentText: "",
    replyCommentId: 0,
    replyCommentText: "",
  },
  action
) => {
  switch (action.type) {
    case CommentsConstants.COMMENT_REQUEST:
      return {
        ...state,
        posting: true,
        posted: false,
        user: action.payload,
      };
    case CommentsConstants.COMMENT_SUCCESS:
      return {
        ...state,
        posting: false,
        posted: true,
        user: action.payload,
      };
    case CommentsConstants.COMMENT_FAILURE:
      return { ...state, posting: false, posted: false };

    case CommentsConstants.COMMENT_ID:
      return {
        ...state,
        commentId: action.payload,
      };
    case CommentsConstants.NEW_COMMENT_TEXT:
      return {
        ...state,
        newCommentText: action.payload,
      };

    case CommentsConstants.REPLY_COMMENT_ID:
      return {
        ...state,
        replyCommentId: action.payload,
      };
    case CommentsConstants.REPLY_COMMENT_TEXT:
      return {
        ...state,
        replyCommentText: action.payload,
      };

    default:
      return state;
  }
};

export const CommentsReducers = {
  Comment_status: statusReducer,
};
