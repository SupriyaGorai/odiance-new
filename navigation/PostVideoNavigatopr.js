import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import PostVideoScreen from "../screens/Viewer/PostVideoScreen/PostVideoScreen";
import CommentScreen from "../screens/Viewer/CommentScreen/CommentScreen";
import PostVideoProfile from "./../screens/Viewer/PostVideoScreen/PostVideoScreenProfile";
import HomeScreen from "../screens/Shared/HomeScreen/HomeScreen";
import { connect } from "react-redux";


import PostVideoScreenProfile from "../screens/Viewer/PostVideoScreen/PostVideoScreenProfile";

const Stack = createStackNavigator();

 class PostVideoNavigatopr extends React.Component {
  
  render() {
    
    return (
      <Stack.Navigator
        initialRoute="Home"
        screenOptions={{
          headerShown: false,
        }}
      >
          <Stack.Screen
          name="Home"
          component={HomeScreen}
         
        />
        <Stack.Screen
          name="postVideo"
          component={PostVideoScreen}
         
        />
        <Stack.Screen
          name="PostVideoProfile"
           component={PostVideoProfile}
         
        />

         
      </Stack.Navigator>
    );
  }
}

function mapStateToProps(state) {
  return {
    status: state.user,
    
  };
}

export default connect(mapStateToProps)(PostVideoNavigatopr);
