import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import PostVideoScreen from "../screens/Viewer/PostVideoScreen/PostVideoScreen";
import CommentScreen from "../screens/Viewer/CommentScreen/CommentScreen";

import PostVideoScreenProfile from "../screens/Viewer/PostVideoScreen/PostVideoScreenProfile";

const Stack = createStackNavigator();

export default class CommentNavigator extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { navigation, name } = this.props;
    return (
      <Stack.Navigator
        initialRoute="postVideo"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen
          name="postVideo"
          component={PostVideoScreen}
          initialParams={{ ...this.props.route.params }}
        />
        <Stack.Screen
          name="comment"
          component={CommentScreen}
          initialParams={{ ...this.props.route.params }}
        />

        {/* <Stack.Screen name='PostVideoScreenProfile' component={PostVideoScreenProfile}/> */}
      </Stack.Navigator>
    );
  }
}
