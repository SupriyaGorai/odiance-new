import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ProfileScreen from '../screens/Shared/ProfileScreen/ProfileScreen';
// import InspirationScreen from '../screens/Creator/InspirationScreen/inspirationScreen';
// import EmptyScreen from '../screens/Viewer/EmptyScreen/EmptyScreen';
import SettingsNavigator from './SettingsNavigator'
import CommentNavigator from "./CommentNavigator";
import PostVideoProfile from "./../screens/Viewer/PostVideoScreen/PostVideoScreenProfile";
const Stack = createStackNavigator();

export default class ProfileNavigator extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {navigation, name} = this.props;
    return (
      <Stack.Navigator
      initialRoute="profile"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="profile" component={ProfileScreen} initialParams={{...this.props.route.params}}/>
        <Stack.Screen name="Settings" component={SettingsNavigator} initialParams={{...this.props.route.params}}/>
        <Stack.Screen name="Post" component={CommentNavigator} />
        <Stack.Screen name="PostVideoProfile" component={PostVideoProfile} />
       </Stack.Navigator>
    );
  }
}
