import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import PostScreen from '../screens/Creator/PostScreen/PostScreen';
import CameraScreen  from '../screens/Creator/Camerascreen/CameraScreen';



const Stack = createStackNavigator();

export default class PostNavigator extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {navigation} = this.props;
    return (
      <Stack.Navigator
      initialRoute="Post"
        screenOptions={{
          headerShown: false,
        }}>
          <Stack.Screen name="Camera" component={CameraScreen} initialParams={{...this.props.route.params}}/>
          <Stack.Screen name='Post' component={PostScreen} initialParams={{...this.props.route.params}}/>

          
          {/* <Stack.Screen name="Camera" component={CameraScreen} initialParams={{...this.props.route.params}}/> */}
          {/* <Stack.Screen name='Post' component={PostScreen} initialParams={{...this.props.route.params}}/>
          <Stack.Screen name="Camera" component={CameraScreen} initialParams={{...this.props.route.params}}/> */}
        {/* <Stack.Screen name='Post' component={PostScreen} /> */}
      </Stack.Navigator>
    );
  }
}
