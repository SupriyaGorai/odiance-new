import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import PostVideoScreen from "../screens/Viewer/PostVideoScreen/PostVideoScreen";
import CommentScreen from "../screens/Viewer/CommentScreen/CommentScreen";
import PostVideoProfile from "./../screens/Viewer/PostVideoScreen/PostVideoScreenProfile";
import HomeScreen from "../screens/Shared/HomeScreen/HomeScreen";
import InspireScreen from '../screens/Shared/InspireScreen/InspireScreen';
import Inspirecard from '../screens/Shared/InspireScreen/Inspirecard';
import NotificationNavigator from './NotificationNavigator';
import { connect } from "react-redux";


import PostVideoScreenProfile from "../screens/Viewer/PostVideoScreen/PostVideoScreenProfile";

const Stack = createStackNavigator();

 class PostVideoNavigatopr extends React.Component {
   nav = () => {
    this.props.navigation.navigate('Inspirations', { type: 'type' });
  }
  
  render() {
    
    return (
      <Stack.Navigator
        initialRoute="NotificationNavigator"
        screenOptions={{
          headerShown: false,
         
        }}
      >
          <Stack.Screen name='NotificationNavigator'  component={NotificationNavigator}/> 
        
        <Stack.Screen
          name="PostVideoProfile"
           component={PostVideoProfile}
         
        />
        <Stack.Screen
          name="postVideo"
          component={PostVideoScreen}
          
        />
        <Stack.Screen
          name="Inspirecard"
          component={Inspirecard}
          initialParams={{ navigate: this.nav }}
          
        />
        <Stack.Screen
          name="Inspirations"
          component={InspireScreen}
          initialParams={{ navigate: this.nav }}
        />
        

         
      </Stack.Navigator>
    );
  }
}

function mapStateToProps(state) {
  return {
    status: state.user,
    
  };
}

export default connect(mapStateToProps)(PostVideoNavigatopr);
