import React, { Component } from 'react';
import { View,ScrollView} from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import InspireScreen from '../screens/Shared/InspireScreen/InspireScreen';
import ForInspireScreenNavigation from './ForInspireScreenNavigation';
import Colors from '../Constants/Colors';
import { connect } from "react-redux";

const Tab = createMaterialTopTabNavigator();



export class NotificationNavigator extends Component {

  nav = () => {
    this.props.navigation.navigate('Inspirations', { type: 'type' });
  }
  

  render() {
    
    
    return (
      
      <Tab.Navigator tabBarOptions={{ indicatorStyle: { backgroundColor: Colors.primary, height: 5 } }}>
        <Tab.Screen name="Inspirations" component={InspireScreen} initialParams={{ navigate: this.nav }} />
        {console.log(this.props, 'insp')}
        {this.props.status.isCreator ?
        (
          <Tab.Screen name="Inspires" component={InspireScreen} initialParams={{ navigate: this.nav }} />
        ) : 
        (null)
        }
        
      </Tab.Navigator>
    
    );
  }
}



function mapStateToProps(state) {
  return {
    status: state.user,

  };
}



export default connect(
  mapStateToProps,

)(NotificationNavigator);
