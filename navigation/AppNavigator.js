import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ViewerNavigator from './ViewerNavigator';
import { connect } from 'react-redux';
import CreatorNavigator from './CreatorNavigator';
import { getInitialState } from '../_actions/LoginAction';
const Stack = createStackNavigator();
class AppNavigator extends React.Component {
  constructor(props) {
    super(props);
  }   
   componentDidMount() {
    this.props.getInitialState();//are you getting any value here???undefined pawar ktha
    console.log(this.props);    
}
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator headerMode='none' >
          <Stack.Screen name='Viewer' component={ViewerNavigator} />
          <Stack.Screen name='Creator' component={CreatorNavigator} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

function mapStateToProps(state) {
  return {}
}

function mapDispatchToProps(dispatch) {
  return {
    getInitialState: () => { dispatch(getInitialState()) }
  };
}


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppNavigator);

