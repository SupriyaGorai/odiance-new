import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import NotificationHeader from "../screens/Viewer/components/NotificationHeader";

import HistoryScreen from "../screens/Shared/HistoryScreen/HistoryScreen";
import ViewerUpdate from "../screens/Shared/HistoryScreen/ViewerUpdate";
import PlaylistScreen from "../screens/Viewer/PlaylistScreen/PlaylistScreen";
import HomeScreen from "../screens/Shared/HomeScreen/HomeScreen";
import PostVideoScreen from "../screens/Viewer/PostVideoScreen/PostVideoScreen";

import ReportProblemScreen from "../screens/Shared/SettingsScreen/ReportProblemScreen";
import CopyrightPolicyScreen from "../screens/Shared/SettingsScreen/CopyrightPolicyScreen";
import CommunityGuideScreen from "../screens/Shared/SettingsScreen/CommunityGuideScreen";
import PushNotificationScreen from "../screens/Shared/SettingsScreen/PushNotificationScreen";
import PrivacyPolicyScreen from "../screens/Shared/SettingsScreen/PrivacyPolicyScreen";
import profileScreenEdit from "../screens/Shared/ProfileScreen/ProfileScreenEdit";
import SettingsNavigator from "./SettingsNavigator";
import ProfileScreen from "../screens/Shared/ProfileScreen/ProfileScreen";
import PostVideoProfile from "./../screens/Viewer/PostVideoScreen/PostVideoScreenProfile";

const Stack = createStackNavigator();

export default class  HistoryNavigator extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, name } = this.props;
    return (
      <Stack.Navigator
        initialRoute="main"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen
          name="main"
          component={HistoryScreen}
          initialParams={{ ...this.props.route.params }}
        />

        <Stack.Screen name="playlist" component={PlaylistScreen} />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Settings" component={SettingsNavigator} />
        <Stack.Screen name="postvideo" component={PostVideoScreen} />
        <Stack.Screen name="profileScreenEditt" component={profileScreenEdit} />
        <Stack.Screen
          name="PostVideoProfile"
           component={PostVideoProfile}
         
        />
        <Stack.Screen name="ViewerUpdate" component={ViewerUpdate} />
        <Stack.Screen name="profile" component={ProfileScreen} />
        <Stack.Screen
          name="NotificationHeader"
          component={NotificationHeader}
        />

        <Stack.Screen name="ReportProblem" component={ReportProblemScreen} />
        <Stack.Screen
          name="CopyrightPolicy"
          component={CopyrightPolicyScreen}
        />
        <Stack.Screen name="CommunityGuide" component={CommunityGuideScreen} />
        <Stack.Screen
          name="PushNotification"
          component={PushNotificationScreen}
        />
        <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicyScreen} />
      </Stack.Navigator>
    );
  }
}
