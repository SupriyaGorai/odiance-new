import {
	heightPercentageToDP as hp,
	widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Colors from '../Constants/Colors';

export default styles = {
	viewerNavigatorIcon: {

		// color: '#fff',
		// fontSize: hp(7),
		//  textAlign: 'center',
		height: 22, 
		resizeMode: 'contain',
		
		
		
	},
	viewerNavigatorCenterIcon: {
		
		height: 37,
		aspectRatio: 1,
		resizeMode:'cover',
		// height: hp(4.8),
		// tintColor: '#fff',
		bottom: 12.5,
		alignSelf: 'center',
		// marginTop: '50%',
		// marginBottom: '50%',
		// top: hp(0),
		// paddingBottom:10
	},
	viewerBar: {
		
		height: hp(8),
		minHeight: 60,
		backgroundColor: '#66001a',
	},
	labelStyle: {
		width: wp(17),
		height: wp(17),
		color: '#fff',
		fontSize: 8,
		textAlign: 'center',	
	},
	labelCenterStyle: {
		bottom: 15,
		width: wp(17),
		color: '#fff',
		fontSize: 8,
		textAlign: 'center',
	},
	navIconStyle: {
		// top: hp(0.3),
		flex:1,
		justifyContent: 'space-between',
		alignItems: 'center',
		flexDirection:'column'
		
	},
	drawer: {
		container: {
			flex: 1,
			
			flexDirection: 'column',
			alignContent: 'center',
			alignItems: 'center',
		},
		home: {
			maxHeight: 60,
			// height: hp(20),
			justifyContent: 'center',
			backgroundColor: '#131313',
			flex: 1,
			justifyContent: 'space-between',
			flexDirection: 'row',
			paddingHorizontal: wp(5),
			// paddingVertical : hp(4)
		},
		menuIcon: {
			height: 50,
			width: 50,
		},
		iconContainer: {
			flex: 0.3,
			alignSelf: 'center',
		},
		topHeader: {
			color: '#fff',
			flex: 0.7,
			fontSize: 16,
			textAlignVertical: 'center',
		},
		roundedTextView: {
			flexDirection: 'row-reverse',
			//borderRadius: hp('50%'),
			marginTop: hp(1.5),
			marginHorizontal: wp(2), 
		},
		textInput: {
			color: Colors.primary,
			borderRadius: hp('50%'),
			paddingLeft: wp(5),
            paddingRight: wp(10),
			marginRight: wp(2),
			borderWidth : 0.5,
			borderColor : '#efefef',
			//marginTop: hp(1.5),
			//paddingHorizontal: wp(4),
			//paddingRight : wp(1),
			//width : '100%',
			flex : 1,
			//textAlign : 'left',
			//backgroundColor : 'red',
		},
		textLabelEnd: {
			position: 'absolute',
			color: Colors.primary,
			alignSelf: 'center',
			fontSize: 18,
			//backgroundColor : 'black',
			fontWeight: 'bold',
			paddingRight: wp(4),
            //flex : 0.1,

		},
		textLabelCenter: {
			marginVertical: hp(1),
			color: Colors.primary,
			fontWeight: '400',
			fontSize: 20,
		},
		buttonClip: {
			backgroundColor: Colors.primary,
			borderRadius: hp(5),
			width: '90%',
			justifyContent : 'space-around'
		},
		buttonLogin: {
			backgroundColor: Colors.primary,
			borderRadius: hp(5),
			width: '90%',
			bottom: hp(1),
			position: 'absolute'
		},
		textWhite: { color: Colors.white,fontWeight : '100'},
	},
};
