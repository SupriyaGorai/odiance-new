import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import ProfileScreen from "../screens/Shared/ProfileScreen/ProfileScreen";
import profileScreenEdit from "../screens/Shared/ProfileScreen/ProfileScreenEdit";
import InspirationScreen from "../screens/Creator/InspirationScreen/inspirationScreen";
import HistoryScreen from "../screens/Shared/HistoryScreen/HistoryScreen";
import CommentNavigator from "./CommentNavigator";
import HomeScreen from "../screens/Shared/HomeScreen/HomeScreen";
import SettingsScreen from "../screens/Shared/SettingsScreen/SettingsScreen";
import ReportProblemScreen from "../screens/Shared/SettingsScreen/ReportProblemScreen";
import CopyrightPolicyScreen from "../screens/Shared/SettingsScreen/CopyrightPolicyScreen";
import CommunityGuideScreen from "../screens/Shared/SettingsScreen/CommunityGuideScreen";
import PushNotificationScreen from "../screens/Shared/SettingsScreen/PushNotificationScreen";
import PrivacyPolicyScreen from "../screens/Shared/SettingsScreen/PrivacyPolicyScreen";
import PostVideoProfile from "./../screens/Viewer/PostVideoScreen/PostVideoScreenProfile";
import ChangePassword from "../screens/Shared/SettingsScreen/ChangePassword";

const Stack = createStackNavigator();

export default class SettingsNavigator extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { navigation, name } = this.props;
    return (
      <Stack.Navigator
        initialRoute="Settings"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="Settings" component={SettingsScreen} />
        <Stack.Screen name="profile" component={ProfileScreen} />
        <Stack.Screen name="Inspirations" component={InspirationScreen} />
        <Stack.Screen name="Post" component={CommentNavigator} />
        <Stack.Screen name="PostVideoProfile" component={PostVideoProfile} />
        <Stack.Screen name="History" component={HistoryScreen} />
        <Stack.Screen name="profileScreenEdit" component={profileScreenEdit} />
        <Stack.Screen name="ReportProblem" component={ReportProblemScreen} />
        <Stack.Screen
          name="CopyrightPolicy"
          component={CopyrightPolicyScreen}
        />
        <Stack.Screen name="CommunityGuide" component={CommunityGuideScreen} />
        <Stack.Screen
          name="PushNotification"
          component={PushNotificationScreen}
        />
        <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicyScreen} />
        <Stack.Screen name="ChangePassword" component={ChangePassword} />
        <Stack.Screen name="Home" component={HomeScreen} />
      </Stack.Navigator>
    );
  }
}
