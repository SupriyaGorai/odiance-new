import {
  createDrawerNavigator,
  useIsDrawerOpen,
} from "@react-navigation/drawer";
import React, { useReducer } from "react";
import {
  Text,
  View,
  TextInput,
  ToastAndroid,
  Keyboard,
  BackHandler,
  Alert,
} from "react-native";
import HomeScreenNew from "../screens/Shared/HomeScreen/HomeScreenNew";
import LoginScreen from "../screens/Shared/LoginScreen/LoginScreen";
import RegisterScreen from "../screens/Creator/RegisterScreen/RegisterScreen";
import ViewerRegisterScreen from "../screens/Viewer/ViewerRegisterScreen/ViewerRegister";
import EmailLoginScreen from "../screens/Shared/LoginWithEmailScreen/LoginWithEmail";
import styles from "./Styles";
import { Image } from "react-native-elements";
import { Button } from "react-native-paper";
import { connect } from "react-redux";
import ValidateCode from "../_actions/CodeValidationActions";
import { logout } from "../_actions/LoginAction";
import { ActivityIndicator } from "react-native-paper";
import Colors from "../Constants/Colors";
import { TouchableOpacity } from "react-native-gesture-handler";
import PostVideoScreen from "./../screens/Viewer/PostVideoScreen/PostVideoScreen";
import PostVideoProfile from "./../screens/Viewer/PostVideoScreen/PostVideoScreenProfile";
import ForgotPasswordFirstPage from "../screens/Shared/ForgotPassword/ForgotPasswordFirstPage";
import AsyncStorage from "@react-native-community/async-storage";

import PostVideoNavigatopr from "./PostVideoNavigatopr";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from "@react-native-community/google-signin";
const Drawer = createDrawerNavigator();

class HomeDrawer extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    refferal_code: "",
  };
  componentDidMount() {
    GoogleSignin.configure({
      scopes: ["https://www.googleapis.com/auth/drive.readonly"], // what API you want to access on behalf of the user, default is email and profile
      webClientId:
        "250802093474-m5cipn2b70u4qop4e165lmgo1g6uom1a.apps.googleusercontent.com", // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      //hostedDomain: '', // specifies a hosted domain restriction
      //loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
      //accountName: '', // [Android] specifies an account name on the device that should be used
      //iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });
  }
  async removeItemValue(key) {
    try {
        await AsyncStorage.removeItem(key);
        return true;
    }
    catch(exception) {
        return false;
    }
}
  onPressLogout = async () => {
    this.props.navigation.push("Viewer");
    this.props.logout();
    await GoogleSignin.signOut();
    await removeItemValue(googleLoginInfo.email)
  };
  render() {
    const { navigation, code, status, post, enabled } = this.props;
    console.log(status.loggedIn);
    if (code.valid === true && code.register === false) {
      navigation.navigate("Register");
      return <></>;
    } else {
      if (
        code.message !== "" &&
        code.loading === false &&
        code.valid === false &&
        code.email === ""
      ) {
         ToastAndroid.show(code.message, 0.5);
        {
          Keyboard.dismiss();
        }
      }
    }
    console.log(useIsDrawerOpen, "aaa");

    return (
      <View style={styles.drawer.container}>
        <View style={styles.drawer.home}>
          <Image
            borderRadius={30}
            source={require("../assets/logo-no-border.png")}
            style={styles.drawer.menuIcon}
            containerStyle={styles.drawer.iconContainer}
          />
          <Text style={styles.drawer.topHeader}>Be a creator</Text>
        </View>
        <View style={styles.drawer.roundedTextView}>
          <TextInput
            style={styles.drawer.textInput}
            placeholder="Paste the code"
            placeholderTextColor="#C0C0C0"
            onChangeText={(text) => this.setState({ refferal_code: text })}
            returnKeyType="done"
            onSubmitEditing={() => {
              if (!code.loading) {
                this.props.checkCode(this.state.refferal_code, "check");
              }
            }}
          />

          {code.loading ? (
            <ActivityIndicator
              style={styles.drawer.textLabelEnd}
              color={Colors.primary}
            />
          ) : (
            <Text
              style={styles.drawer.textLabelEnd}
              onPress={() => {
                if (!code.loading) {
                  this.props.checkCode(this.state.refferal_code, "check");
                }
              }}
            >
              GO
            </Text>
          )}
        </View>
        <View style={{ height: 20 }} />
        <Button
          onPress={() =>
            ToastAndroid.show("You are one step closer to become a creator", 3)
          }
          style={styles.drawer.buttonClip}
        >
          <Text style={styles.drawer.textWhite}>Submit a clip</Text>
        </Button>
        {status.loggedIn ? (
          <Button
            onPress={() => {
              this.onPressLogout() && navigation.closeDrawer();
            }}
            style={styles.drawer.buttonLogin}
          >
            <Text style={styles.drawer.textWhite}>Logout</Text>
          </Button>
        ) : (
          <Button
            onPress={() => {
              navigation.navigate("Login", {}) && navigation.closeDrawer();
            }}
            style={styles.drawer.buttonLogin}
          >
            <Text style={styles.drawer.textWhite}>Login</Text>
          </Button>
        )}
      </View>
    );
  }
}
function mapStatetoProps(state) {
  return {
    code: state.code,
    status: state.user,
    post: state.post,
    enabled: state.search.enabled,
  };
}

const actionCreators = {
  checkCode: ValidateCode,
  logout: logout,
};

const ConnectedHomeDrawer = connect(
  mapStatetoProps,
  actionCreators
)(HomeDrawer);

class HomeNavigator extends React.Component {
  backAction = () => {
    if (this.props.route.name === "Home") {
      Alert.alert("Hold on!", "Are you sure you want to exit?", [
        {
          text: "Cancel",
          onPress: () => null,
          style: "cancel",
        },
        { text: "YES", onPress: () => BackHandler.exitApp() },
      ]);
      return true;
    }
    // this.props.navigation.dispatch(NavigationAction.back());
    // return true;
  };

  addBackHandler = () => {
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.backAction
    );
  };

  componentDidMount() {
    this.addBackHandler();

    this.didFocusSubscription = this.props.navigation.addListener(
      "focus",
      this.addBackHandler
    );

    this.didBlurSubscription = this.props.navigation.addListener("blur", () => {
      this.backHandler.remove();
    });
  }

  render() {
    return (
      <Drawer.Navigator
        initialRoute="postVideoNav"
        drawerType="front"
        drawerStyle={{ backgroundColor: "white" }}
        drawerContent={(props) => <ConnectedHomeDrawer {...props} />}
        // backBehavior='initialRoute'
      >
        <Drawer.Screen name="Home" component ={HomeScreenNew} />
        <Drawer.Screen name="Login" component={LoginScreen} />
        <Drawer.Screen name="Register" component={RegisterScreen} />
        {!this.props.status.loggedIn && (
          <Drawer.Screen name="EmailLogin" component={EmailLoginScreen} />
        )}
        <Drawer.Screen name="ViewerRegister" component={ViewerRegisterScreen} />
        {/* <Drawer.Screen name="PostVideo" component={PostVideoScreen} /> */}
        {/* <Drawer.Screen name="PostVideoProfile" component={PostVideoProfile} /> */}
        <Drawer.Screen name="postVideoNav" component={PostVideoNavigatopr} />
        <Drawer.Screen name="Forgot" component={ForgotPasswordFirstPage} />
      </Drawer.Navigator>
    );
  }
}

function mapStateToProps(state) {
  return {
    status: state.user,
    search: state.search.enabled,
  };
}

export default connect(mapStateToProps)(HomeNavigator);
