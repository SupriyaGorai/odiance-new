import * as React from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ToastAndroid,
} from "react-native";
import { CommonActions } from "@react-navigation/native";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import HomeNavigator from "./HomeNavigator";
import SettingsNavigator from "./SettingsNavigator";
import styles from "./Styles";
import NotificationNavigator from "./NotificationNavigator";
import FollowingScreen from "../screens/Shared/FollowingScreen/FollowingScreen";
import { connect } from "react-redux";
import { HomeActions } from "../_actions";
import HistoryNavigator from "./HistoryNavigator";
import PostNavigator from "./PostNavigator";
import ProfileNavigator from "./ProfileNavigator";
import { getInitialState } from "../_actions/LoginAction";
import ForInspireScreenNavigation from "./ForInspireScreenNavigation";
//import { NavigationActions } from "../_actions/NavigationActions";
import style from "../screens/Creator/components/style";
const Tab = createMaterialBottomTabNavigator();

class NavIcon extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {
      iconStyle,
      name,
      label,
      type,
      labelStyle,
      source,
      onPressButton,
    } = this.props;
    return (
      <View style={styles.navIconStyle}>
        <Image source={source} style={iconStyle} />
        <Text style={labelStyle}>{label}</Text>
      </View>
    );
  }
}
class CreatorNavigator extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hideNavBar: false, focused: false, Home: HomeNavigator };
  }
  hideNav = () => {
    if (!this.state.hideNavBar) {
      this.setState({ hideNavBar: true });
      this.setState({ focused: true });
    }
  };
  showNav = () => {
    if (this.state.focused) {
      if (this.state.hideNavBar) {
        this.setState({ hideNavBar: false });
      }
    }
  };
  reset = (name,index) => {
    const resetAction = CommonActions.reset({
      index: index,
      routes: [{ name: name }],
    });
    this.props.navigation.dispatch(resetAction);
  };
  componentDidMount() {
    this.props.getInitialState();
  }
  render() {
    const { loggedIn, msg } = this.props.status;
    const { posted } = this.props.post.posted;
    const { navigation, search } = this.props;
    if (!loggedIn) {
      if (msg) {
        ToastAndroid.show(msg, 0.5);
      }
      this.reset("Viewer",0);
    }
    this.showNav();
    console.log(this.props.route,'route_name');
    return (
      <Tab.Navigator
        initialRouteName="Home"
        barStyle = {styles.viewerBar}
        labeled={false}
        backBehavior="initialRoute"
        lazy = {false}

      >
        <Tab.Screen
          name="Home"
          component={this.state.Home}
          listeners={({ navigation, route }) => ({
            tabPress: (e) => {
              e.preventDefault();
              //console.log(navigation.state.index,'routename');
              if (!navigation.isFocused()) {
                this.reset('Home',0)
              }
            },
          })}
          options={{
            tabBarLabel: "Home",
            tabBarIcon: ({ focused }) => {
              // const routename = navigation.state.routeName
              // // if(focused)
              // // {
              //    this.props.setCurrentRoute(routename);
              // // }
              return focused ? (
                <NavIcon
                  source={require("../assets/icons/home-fill.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Home"
                  labelStyle={styles.labelStyle}
                />
              ) : (
                <NavIcon
                  source={require("../assets/icons/home-white.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Home"
                  labelStyle={styles.labelStyle}
                />
              );
            },
          }}
        />

        <Tab.Screen
          name="Following"
          component={ForInspireScreenNavigation}
          listeners={({ navigation, route }) => ({
            tabPress: (e) => {
              e.preventDefault();
              if (!navigation.isFocused()) {
                this.reset('Following',1)
              }
            },
          })}
          options={{
            tabBarLabel: "Following",
            tabBarIcon: ({ focused }) => {
              // const routename = navigation.state.routeName
              // // // if(focused)
              // // // {
              // //    this.props.setCurrentRoute(routename);
              // // // }
              return focused ? (
                <NavIcon
                  source={require("../assets/icons/following-fill.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Following"
                  labelStyle={styles.labelStyle}
                />
              ) : (
                <NavIcon
                  source={require("../assets/icons/following-white.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Following"
                  labelStyle={styles.labelStyle}
                />
              );
            },
          }}
        />

        <Tab.Screen
          name="Post"
          component={PostNavigator}
          options={{
            tabBarLabel: "Post",
            tabBarIcon: ({ focused }) => {
              // const routename = navigation.state.routeName
              // // if(focused)
              // // {
              //    this.props.setCurrentRoute(routename);
              // // }
              return (
                <NavIcon
                  source={require("../assets/icons/searchmain.png")}
                  iconStyle={styles.viewerNavigatorCenterIcon}
                  label="Post"
                  labelStyle={styles.labelCenterStyle}
                />
              );
            },
          }}
        />

        <Tab.Screen
          name="Profile"
          component={ProfileNavigator}
          listeners={({ navigation, route }) => ({
            tabPress: (e) => {
              e.preventDefault();
              if (!navigation.isFocused()) {
                this.reset('Profile',3)
              }
            },
          })}
          options={{
            tabBarLabel: "Profile",
            tabBarIcon: ({ focused }) => {
              // const routename = navigation.state.routeName
              // // if(focused)
              // // {
              //    this.props.setCurrentRoute(routename);
              // // }
              return focused ? (
                <NavIcon
                  source={require("../assets/icons/profile-fill.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Profile"
                  labelStyle={styles.labelStyle}
                />
              ) : (
                <NavIcon
                  source={require("../assets/icons/profile-white.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Profile"
                  labelStyle={styles.labelStyle}
                />
              );
            },
          }}
        />

        <Tab.Screen
          name="History"
          component={HistoryNavigator}
          listeners={({ navigation, route }) => ({
            tabPress: (e) => {
              e.preventDefault();
              if (!navigation.isFocused()) {
                this.reset('History',4)
              }
            },
          })}
          options={{
            tabBarLabel: "History",
            tabBarIcon: ({ focused }) => {
              //const routename = navigation.state.routeName
              // // if(focused)
              // // {
              //    this.props.setCurrentRoute(routename);
              // // }
              return focused ? (
                <NavIcon
                  source={require("../assets/icons/history-fill.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="History"
                  labelStyle={styles.labelStyle}
                />
              ) : (
                <NavIcon
                  source={require("../assets/icons/history-white.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="History"
                  labelStyle={styles.labelStyle}
                />
              );
            },
          }}
        />
      </Tab.Navigator>
    );
  }
}

function mapStateToProps(state) {
  return {
    status: state.user,
    search: state.search.enabled,
    post: state.post,
    route : state.route,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout()),
    getInitialState: getInitialState,
    toggleSearch: () => dispatch(HomeActions.toggle()),
    //setCurrentRoute : (route) => dispatch(NavigationActions.setRouteName(route)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatorNavigator);
