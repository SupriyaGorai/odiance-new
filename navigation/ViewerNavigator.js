import * as React from "react";
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ToastAndroid,
} from "react-native";
import HomeNavigator from "./HomeNavigator";
import ForInspireScreenNavigation from "./ForInspireScreenNavigation";
import HistoryNavigator from "./HistoryNavigator";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import styles from "./Styles";
import { connect } from "react-redux";
import { HomeActions } from "../_actions";
import { CommonActions} from "@react-navigation/native";
import FollowingScreen from "../screens/Shared/FollowingScreen/FollowingScreen";
import { getInitialState } from "../_actions/LoginAction";
import ViewerNotification from "../screens/Viewer/ViewerNotificationScreen/ViewerNotification";
const Tab = createMaterialBottomTabNavigator();

class NavIcon extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      iconStyle,
      name,
      label,
      type,
      labelStyle,
      source,
      onPressButton,
    } = this.props;
    return (
      <View style={styles.navIconStyle}>
        <Image source={source} style={iconStyle} />
        <Text style={labelStyle}>{label}</Text>
      </View>
    );
  }
}
class ViewerNavigator extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hideNavBar: false, focused: false, Home: HomeNavigator };
  }
  hideNav = () => {
    if (!this.state.hideNavBar) {
      this.setState({ hideNavBar: true });
      this.setState({ focused: true });
    }
  };
  showNav = () => {
    if (this.state.focused) {
      if (this.state.hideNavBar) {
        this.setState({ hideNavBar: false });
      }
    }
  };

  reset = (name,index) => {
    const resetAction = CommonActions.reset({
      index: index,
      routes: [{ name: name }],
    });
    this.props.navigation.dispatch(resetAction);
  };
   async componentDidMount() {
     await this.props.getInitialState();
    console.log(this.props, "postvalue");
    console.log("componentDidMount1111");
    console.log(this.props.status,'ssss');
  }

  render() {
    this.showNav();
    const { navigation, search, status } = this.props;
    const { loggedIn, msg, isCreator } = this.props.status;
    if (loggedIn) {
      if (isCreator === true) {
        this.reset("Creator",0);
      }
    }
    return (
      <Tab.Navigator
        initialRouteName="Home"
        barStyle={
          this.state.hideNavBar
            ? { ...styles.viewerBar, display: "none" }
            : styles.viewerBar
        }
        labeled={false}
        options={{
          unMountOnBlur: true,
        }}

        // backBehavior="initialRoute"
      >
        <Tab.Screen
          name="Home"
          component={HomeNavigator}
          listeners={({ navigation, route }) => ({
            tabPress: (e) => {
              e.preventDefault();

              if (search) {
                this.props.toggleSearch();
              }
              if (!navigation.isFocused()) {
                //to prevent re-rendering home screen.
                this.reset("Home",0);
              }
            },
          })}
          options={{
            tabBarLabel: "Home",

            tabBarIcon: ({ focused }) => {
              return focused ? (
                <NavIcon
                  source={require("../assets/icons/home-fill.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Home"
                  labelStyle={styles.labelStyle}
                />
              ) : (
                <NavIcon
                  source={require("../assets/icons/home-white.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Home"
                  labelStyle={styles.labelStyle}
                />
              );
            },
          }}
        />

        {loggedIn ? (
          <Tab.Screen
            name="Following"
            component={ForInspireScreenNavigation}
            listeners={({ navigation, route }) => ({
              tabPress: (e) => {
                e.preventDefault();
                if (!navigation.isFocused()) {
                  this.reset('Following',1)
                }
              },
            })}
            options={{
              tabBarLabel: "Following",
              tabBarIcon: ({ focused }) => {
                return focused ? (
                  <NavIcon
                    source={require("../assets/icons/following-fill.png")}
                    iconStyle={styles.viewerNavigatorIcon}
                    label="Following"
                    labelStyle={styles.labelStyle}
                  />
                ) : (
                  <NavIcon
                    source={require("../assets/icons/following-white.png")}
                    iconStyle={styles.viewerNavigatorIcon}
                    label="Following"
                    labelStyle={styles.labelStyle}
                  />
                );
              },
            }}
          />
        ) : (
          <Tab.Screen
            name="Following"
            component={HomeNavigator}
            listeners={({ navigation, route }) => ({
              tabPress: (e) => {
                e.preventDefault();
                navigation.navigate("Login");
              },
            })}
            options={{
              tabBarLabel: "Following",
              tabBarIcon: ({ focused }) => {
                return focused ? (
                  <NavIcon
                    source={require("../assets/icons/following-fill.png")}
                    iconStyle={styles.viewerNavigatorIcon}
                    label="Following"
                    labelStyle={styles.labelStyle}
                  />
                ) : (
                  <NavIcon
                    source={require("../assets/icons/following-white.png")}
                    iconStyle={styles.viewerNavigatorIcon}
                    label="Following"
                    labelStyle={styles.labelStyle}
                  />
                );
              },
            }}
          />
        )}
        <Tab.Screen
          name="Search"
          component ={HomeNavigator}
          listeners={({ navigation, route }) => ({
            tabPress: (e) => {
              e.preventDefault();
              this.reset("Home",0);
              this.props.toggleSearch();
              navigation.navigate("Home");
            },
          })}
          options={{
            tabBarLabel: "Search",
            tabBarIcon: ({ focused }) => {
              return (
                <NavIcon
                  source={require("../assets/icons/searchmain.png")}
                  iconStyle={styles.viewerNavigatorCenterIcon}
                  label="Search"
                  labelStyle={styles.labelCenterStyle}
                />
              );
            },
          }}
        />

        <Tab.Screen
          name="Notification"
          component={ViewerNotification}
          listeners={({ navigation, route }) => ({
            tabPress: (e) => {
              e.preventDefault();
              if (search) {
                this.reset("Viewer",0);
                this.props.toggleSearch();
              }
              if (!loggedIn) {
                navigation.navigate("Login", {});
              } else {
                this.reset("Notification",3);
              }
            },
          })}
          options={{
            tabBarLabel: "Notification",
            tabBarIcon: ({ focused }) => {
              return focused ? (
                <NavIcon
                  source={require("../assets/icons/notification-fill.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Notification"
                  labelStyle={styles.labelStyle}
                />
              ) : (
                <NavIcon
                  source={require("../assets/icons/notification-white.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="Notification"
                  labelStyle={styles.labelStyle}
                />
              );
            },
          }}
        />

        <Tab.Screen
          name="History"
          component={HistoryNavigator}
          listeners={({ navigation, route }) => ({
            tabPress: (e) => {
              e.preventDefault();
              if (search) {
                this.reset("Home",0);
                navigation.navigate("Home", {});
                this.props.toggleSearch();
              }
              if (!loggedIn) {
                navigation.navigate("Login", {});
              } else {
                this.reset("History",5);
              }
            },
          })}
          options={{
            tabBarLabel: "History",
            tabBarIcon: ({ focused }) => {
              return focused ? (
                <NavIcon
                  source={require("../assets/icons/history-fill.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="History"
                  labelStyle={styles.labelStyle}
                />
              ) : (
                <NavIcon
                  source={require("../assets/icons/history-white.png")}
                  iconStyle={styles.viewerNavigatorIcon}
                  label="History"
                  labelStyle={styles.labelStyle}
                />
              );
            },
          }}
        />
      </Tab.Navigator>
    );
  }
}

function mapStateToProps(state) {
  return {
    status: state.user,
    search: state.search.enabled,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    toggleSearch: () => dispatch(HomeActions.toggle()),
    getInitialState: getInitialState,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewerNavigator);
