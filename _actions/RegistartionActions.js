import RegistrationConstants from '../_constants/RegistrationConstants';
import Register from '../_services/RegistratonServices'
import {StoreUser} from '../_storage/Storage';
import AsyncStorage from '@react-native-community/async-storage';
import { ToastAndroid } from 'react-native';

StoreToken = async (token) => {
  try {
    console.log(token);
    const stored = await AsyncStorage.setItem('token', token)
  } catch (error) {
    console.log('failed to store token', error.message);
  }
}

function Registration_Request(user) {
  return {
    type: RegistrationConstants.request,
    user,
  };
}
function Registration_Success(msg,token,id) {
  return {
    type: RegistrationConstants.success,
    msg,token,id
  };
}
function Registration_Failure(error) {
  return {
    type: RegistrationConstants.failure,
    error
  };
}
function Remove_message() {
  return {
    type: 'REMOVE_MESSAGE',
  };
}
export default function RegisterUser(user) {
  return async (dispatch) => {
    dispatch(Registration_Request(user))
    
      const data = await Register(user);
      if (data.success === true) {
        console.log(data.token)
        await StoreToken(data.token);
        const user = {id : data.creator.id,is_creator: data.creator.is_creator};
        console.log(user);
        await StoreUser(user);
        if(data.msg)
        {
        ToastAndroid.show(data.msg,0.5);
        }
        dispatch(Registration_Success('Registration Succesfull',data.token,data.creator.id));
        Remove_message()
      } else {
        dispatch(Registration_Failure('Registration Failed'));
        Remove_message()
      }
    }
}
    
  
  
