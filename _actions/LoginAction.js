import { loginConstants } from "../_constants/LoginConstants";
import { loginService } from "../_services/LoginServices";
import { ReadUser, StoreUser, RemoveUser } from "../_storage/Storage";
import { ToastAndroid } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";

export const loginActions = {
  login,
  getInitialState,
};

const ReadToken = async () => {
  try {
    const token = await AsyncStorage.getItem("token");
    return token;
  } catch (error) {
    return null;
  }
};
const initialState = async () => {
  const token = await ReadToken();
  const user = await ReadUser();
  console.log(user, "intial");
  // console.log("_actions > LoginAction.js > initialState:",user);
  return {
    loggedIn: token != null,
    loggingIn: false,
    msg: null,
    token: token,
    isCreator: user.user.is_creator || false,
    id: user.user.id,
  };
};
function Remove_message() {
  return {
    type: "REMOVE_MESSAGE",
  };
}
function getState() {
  return {
    type: "default",
  };
}

StoreToken = async (token) => {
  try {
    console.log(token);
    const stored = await AsyncStorage.setItem("token", token);
  } catch (error) {
    console.log("failed to store token", error.message);
  }
};

ClearToken = async () => {
  try {
    await AsyncStorage.removeItem("token");
  } catch (error) {
    console.log("failed to delete token", error.message);
  }
};

export function login(email, password, deviceId, deviceType) {
  return async (dispatch) => {
    dispatch(request({ email }));
    console.log("before try login action");
    try {
      console.log("in try");
      const data = await loginService.login(
        email,
        password,
        deviceId,
        deviceType
      );
      if (data.success === true) {
        console.log(data.msg);
        const user = { id: data.user.id, is_creator: data.user.is_creator };
        console.log(user, "aaaaann");
        await StoreToken(data.token); // store token in async storage
        await StoreUser(user); // store user in async storage
        ToastAndroid.show("login successfull", 0.5);
        dispatch(success(data.token, data.user.is_creator, data.user.id));
      } else {
        //console.log('here trina');
        ToastAndroid.show("wrong username or password", ToastAndroid.SHORT);
        dispatch(failure("wrong username or password"));
        //ToastAndroid.show(data.error.non_field_errors,0.5);
      }
    } catch (e) {
      //console.log('catch hete trina')
      //dispatch(failure(e.message));
      ToastAndroid.show("wrong username or password", 0.5);
      dispatch(failure(e.message));
      //dispatch(getState());
    }
  };

  function request(email) {
    return { type: loginConstants.LOGIN_REQUEST, email };
  }
  function success(token, is_creator, id) {
    return { type: loginConstants.LOGIN_SUCCESS, token, is_creator, id };
  }
  function failure(error) {
    return { type: loginConstants.LOGIN_FAILURE, error };
  }
}

export function getInitialState() {
  return async (dispatch) => {
    dispatch({
      type: loginConstants.GET_INITIAL_STATE,
      payload: await initialState(),
    });
  };
}

export const demoCall = () => {
  dispatch({ type: loginConstants.GET_INITIAL_STATE, payload: getData() });
};
const getData = () => {};
export function logout() {
  console.log("trying to logout");
  return async (dispatch) => {
    await ClearToken();
    await RemoveUser();
    dispatch({ type: loginConstants.LOGOUT });
    // navigation.navigate('Home');

    ToastAndroid.show("logged out succesfully", 0.5);
  };
}
