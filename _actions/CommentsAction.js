import { CommentsConstants } from "../_constants/CommentsConstants";
import CommentsService from "../_services/CommentsService";
import { ToastAndroid } from "react-native";

function Comments_Success(text) {
  return {
    type: CommentsConstants.COMMENT_SUCCESS,
    payload: text,
  };
}
function Comments_Failure() {
  return {
    type: CommentsConstants.COMMENT_FAILURE,
  };
}
export default function Comments(text, token, navigation) {
  return async (dispatch) => {
    try {
      console.log("hi there");
      const result = await CommentsService(text, token);
      console.log(result);
      ToastAndroid.show(result.msg, 0.5);
      dispatch(Comments_Success(result));
      navigation.navigate("Home");
    } catch (error) {
      dispatch(Comments_Failure(error));
    }
  };
}

export const setCommentId = (val) => {
  return {
    type: CommentsConstants.COMMENT_ID,
    payload: val,
  };
};
export const setNewCommentText = (val) => {
  return {
    type: CommentsConstants.NEW_COMMENT_TEXT,
    payload: val,
  };
};

export const setReplyCommentId = (val) => {
  return {
    type: CommentsConstants.REPLY_COMMENT_ID,
    payload: val,
  };
};
export const setReplyCommentText = (val) => {
  return {
    type: CommentsConstants.REPLY_COMMENT_TEXT,
    payload: val,
  };
};
