import { HomeConstants } from "../_constants";

function toggle() {
  return { type: HomeConstants.toggleSearch };
}
function dateValue(val) {
  return {
    type: HomeConstants.dateValue,
    payload: val,
  };
}
function searchText(val) {
  return {
    type: HomeConstants.searchText,
    payload: val,
  };
}
function sliderValue(val) {
  return {
    type: HomeConstants.sliderValue,
    payload: val,
  };
}

export const HomeActions = {
  toggle: toggle,
  dateValue: dateValue,
  searchText: searchText,
  sliderValue: sliderValue,
};
