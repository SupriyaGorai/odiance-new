import { HistoryConstants } from "../_constants";

export const setSearchtext = (val) => {
  return {
    type: HistoryConstants.searchText,
    payload: val,
  };
};
