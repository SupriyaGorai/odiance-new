import { NavigationConstants } from "../_constants/NavigationConstants";

function setRouteName(route) {
  return { type: NavigationConstants.routeName,
            payload : route };
}
export const NavigationActions = {
  setRouteName : setRouteName
};
