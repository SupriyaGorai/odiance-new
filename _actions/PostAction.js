import postConstants from '../_constants/PostConstants';
import postService from '../_services/PostServices';
import { ToastAndroid } from 'react-native';

function Post_Success(user) {

  return {
    type: postConstants.POST_SUCCESS,
    payload: user,
  };
}
function Post_Failure() {
  return {
    type: postConstants.POST_FAILURE,
  };
}
export default function post(formData, token, navigation) {
  return async dispatch => {
    try {
      console.log('hi there');
      const result = await postService(formData, token);
      console.log(result);
      ToastAndroid.show(result.msg, 0.5);
      dispatch(Post_Success(result));
      navigation.navigate('Home');
    } catch (error) {
      dispatch(Post_Failure(error))
    }
  }
}