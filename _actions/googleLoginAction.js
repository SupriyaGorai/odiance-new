import googleLoginConstants from '../_constants/googleLoginConstants';
import googleLogin from '../_services/googleLoginServices';
import {StoreUser} from '../_storage/Storage';
import AsyncStorage from '@react-native-community/async-storage';
import { ToastAndroid } from 'react-native';


StoreToken = async (token) => {
  try {
    console.log(token);
    const stored = await AsyncStorage.setItem('token', token)
  } catch (error) {
    console.log('failed to store token', error.message);
  }
}

function googleLogin_request_request(user) {
  return {
    type: googleLoginConstants.googleLogin_request,
    user,
  };
}
export function googleLogin_success_success(msg,token) {
  return {
    type: googleLoginConstants.googleLogin_success,
    msg,token
  };
}
function googleLogin_failure_failure(error) {
  return {
    type: googleLoginConstants.googleLogin_failure,
    error
  };
}
function Remove_message() {
  return {
    type: 'REMOVE_MESSAGE',
  };
}
export default async function ViewerRegisterGoogle(userInfo) {
  console.log(userInfo,'here')
  const data = await googleLogin(userInfo)
  
    return{
     googleLogin_request_request(userInfo)
      {
        data?(
          await StoreToken(data.token),
          await StoreUser(data.user),
          ToastAndroid.show(data.msg,0.5),
          googleLogin_success_success('Registration Successfull',data.token),
          Remove_message(),
        ):(
          googleLogin_failure_failure('Registration Failed'),
          Remove_message()
        )
        
      }
   
      
     
      
      
      
    }
  }
  