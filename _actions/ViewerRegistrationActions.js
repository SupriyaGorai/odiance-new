import ViewerRegistrationConstants from "../_constants/ViewerRegistrationConstants";
import Register from "../_services/ViewerRegistrationServices";
import { StoreUser } from "../_storage/Storage";
import AsyncStorage from "@react-native-community/async-storage";
import { ToastAndroid } from "react-native";

StoreToken = async (token) => {
  try {
    console.log(token);
    const stored = await AsyncStorage.setItem("token", token);
  } catch (error) {
    console.log("failed to store token", error.message);
  }
};
StoreId = async (id) => {
  try {
    console.log(id);
    const stored = await AsyncStorage.setItem("id", id);
  } catch (error) {
    console.log("failed to store id", error.message);
  }
};

function ViewerRegistration_Request(user) {
  return {
    type: ViewerRegistrationConstants.ViewerRegister_request,
    user,
  };
}
export function ViewerRegistration_Success(msg, token,id) {
  return {
    type: ViewerRegistrationConstants.ViewerRegister_success,
    msg,
    token,
    id,
  };
}
function ViewerRegistration_Failure(error) {
  
  return {
    
    type: ViewerRegistrationConstants.ViewerRegister_failure,
    error,
  };
}
function Remove_message() {
  return {
    type: "REMOVE_MESSAGE",
  };
}
export default function ViewerRegister(user, navigation) {
  return async (dispatch) => {
    dispatch(ViewerRegistration_Request(user));
    try {
      const data = await Register(user);
      console.log(data,'after regi');
      if (data.success === true) {
        const user = { id: data.user.id, is_creator: data.user.is_creator };
        await StoreToken(data.token);
        ///await StoreId(data.user.id);
        await StoreUser(user);
        if (data.msg) {
          ToastAndroid.show(data.msg, 0.5);
        }
        dispatch(
          ViewerRegistration_Success("Registration Successfull", data.token,data.user.id)
        );
        navigation.navigate("Home");
        dispatch(Remove_message());
      } else {
       
        dispatch(ViewerRegistration_Failure());
        
        dispatch(Remove_message());
      }
    } catch (e) {
      console.log(e,'act');
      dispatch(ViewerRegistration_Failure(e.message));
      dispatch(Remove_message());
    }
  };
}
