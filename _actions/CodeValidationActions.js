import CodeValidationConstants from '../_constants/CodeValidationConstants';
import CodeApi from '../_services/CodeApi';

function code_request(refferal_code) {
  return {
    type: CodeValidationConstants.CODE_REQUEST,
    payload: {code: refferal_code},
  };
}
function code_sucess(refferal_code, email) {
  return {
    type: CodeValidationConstants.CODE_SUCCESS,
    payload: {code: refferal_code, email: email},
  };
}
function code_failure(error) {
  return {
    type: CodeValidationConstants.CODE_FAILURE,
    payload: {message: error},
  };
}
 function remove_message() {
  return {
    type: 'REMOVE_MESSAGE',
  };
}

export function register_opened(){
  return{
    type: 'REGISTER_OPENED'
  }
}

export default function ValidateCode(refferal_code, type) {
  return async dispatch => {
    if(type === 'check'){
      dispatch(code_request(refferal_code));
      try{
        const data = await CodeApi(refferal_code);
        console.log(data);
        dispatch(code_sucess(refferal_code, data.email));
      }catch(e){
        dispatch(code_failure(e.message))
        dispatch(remove_message());
      }
    }else if(type === 'open'){
      dispatch(register_opened())
    }
  };
}
