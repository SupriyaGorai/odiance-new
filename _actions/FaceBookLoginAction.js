import googleLoginConstants from '../_constants/googleLoginConstants';
import FaceBookLogin from '../_services/faceBookLoginServices';
import {StoreUser} from '../_storage/Storage';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
import { ToastAndroid } from 'react-native';


StoreToken = async (token) => {
  try {
    console.log(token);
    const stored = await AsyncStorage.setItem('token', token)
  } catch (error) {
    console.log('failed to store token', error.message);
  }
}

function googleLogin_request_request(user) {
  return {
    type: googleLoginConstants.googleLogin_request,
    user,
  };
}
export function googleLogin_success_success(msg,token) {
  return {
    type: googleLoginConstants.googleLogin_success,
    msg,token
  };
}
function googleLogin_failure_failure(error) {
  return {
    type: googleLoginConstants.googleLogin_failure,
    error
  };
}
function Remove_message() {
  return {
    type: 'REMOVE_MESSAGE',
  };
}
export default async function ViewerRegisterFaceBook(userInfo) {
  console.log(userInfo,'here')
  
    
     googleLogin_request_request(userInfo)
   
      
      const data = await FaceBookLogin(userInfo);
      
      if (data.success === true) {
        console.log(data,'data');
        
        await StoreToken(data.token);
        
        await StoreUser(data.user);
        if(data.msg)
        {
        ToastAndroid.show(data.msg,0.5);
        }        
       googleLogin_success_success('Registration Successfull',data.token)
        Remove_message()
      } else {
        googleLogin_failure_failure('Registration Failed')
        Remove_message()
      }
    
    
  }
  