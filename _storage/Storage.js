import AsyncStorage from "@react-native-community/async-storage";
export default {
    user: {
        StoreUser,
        ReadUser,
        RemoveUser
    }
}

export const StoreUser = async (user) => {
    console.log(user,'from async');
    try {
        const stored = await AsyncStorage.setItem('user', JSON.stringify(user));
         console.log('user details stored successfully',stored);
        // return { success: true, msg: stored }
    } catch (error) {
        console.error('error while storing user data',error);
        
        // return { success: false, msg: error }
    }
}


export const ReadUser = async () => {
    try {
        // console.log(user,'hjnfjfjfhfncncbchcgngccbdhddbgdgxg');
        const user = await AsyncStorage.getItem('user');
        return { success: true, msg:'user load successful' ,'user': JSON.parse(user) }
    } catch (error) {
        return { success: false, msg: error }
    }
}

export const RemoveUser = async () => {
    try {
        const stored = await AsyncStorage.removeItem('user');
        return { success: true, msg: 'user data deleted' }
    } catch (error) {
        return { success: false, msg: error }
    }
}
