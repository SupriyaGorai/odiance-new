import RNFirebase from '@react-native-firebase/app';

const configurationOptions = {
  debug: true,
  promptOnMissingPlayServices: true,
  apiKey: "AIzaSyAiAo3-OmRqQpcpKObkmGtlr0caCLwUiyk",
  authDomain: "odiance.firebaseapp.com",
  databaseURL: "https://my-project-odian-1595850677383.firebaseio.com",
  projectId: "my-project-odian-1595850677383",
  storageBucket: "my-project-odian-1595850677383.appspot.com",
  messagingSenderId: "105952880220",
   appId: "1:250802093474:android:628aa86342dce6ffbe86fb",
  }

const firebase = RNFirebase.initializeApp(configurationOptions)

export default firebase;
