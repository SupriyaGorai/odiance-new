import axios from 'axios';
import urls from '../Constants/urls';


export default async function FaceBookLogin(userInfo) {
    console.log(userInfo,'from sevice')
  const requestOptions = {
    url: `${urls.userbase}${urls.social_signUp}`,
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    data: {
   
    
    login_type : "facebook",
    first_name : userInfo.first_name,
    last_name : userInfo.last_name ,
    avatar : userInfo.picture.data.url,
    email : userInfo.email ,
    google_id : "",
    fb_id : userInfo.id,
    },
  };
  try{
    const response = await axios(requestOptions);
    if (response.data.success === true) {
         console.log(response,'res')
        // fetch the user data
        // TODO: set the token in local storage
        return response;
        
    } else {
        throw new Error('googleLoginServices failed');
        
    }
}catch(e){
  console.log(e)
    throw new Error ('googleLoginServices fail');
}
}
  