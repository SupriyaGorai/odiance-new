import axios from "axios";
import urls from "../Constants/urls";

export async function POST(URL, token, body) {
  var res;
  await axios({
    method: "POST",
    url: `${urls.base + URL}`,
    headers: {
      Authorization: `Token ${token}`,
      "Content-Type": "application/json",
    },
    data: JSON.stringify(body),
  })
    .then((responseJSON) => {
      res = responseJSON;
    })
    .catch((error) => {
      res = error;
    });
  return res;
}

export async function GET(URL, token) {
  var res;
  await axios({
    method: "GET",
    url: `${urls.base + URL}`,
    headers: {
      Authorization: `Token ${token}`,
      "Content-Type": "application/json",
    },
  })
    .then((responseJSON) => {
      res = responseJSON;
    })
    .catch((error) => {
      res = error;
    });
  return res;
}

export async function uploadImage(URL, token, body) {
  var res;
  await fetch(`${urls.base + URL}`, {
    method: "POST",
    headers: { Authorization: `Token ${token}` },
    body: body,
  })
    .then((responseJSON) => {
      res = responseJSON;
    })
    .catch((error) => {
      res = error;
    });
  return res;
}

export async function POST2(URL, body) {
  var res;
  await axios({
    method: "POST",
    url: `${urls.base + URL}`,
    headers: {
      "Content-Type": "application/json",
    },
    data: JSON.stringify(body),
  })
    .then((responseJSON) => {
      res = responseJSON;
    })
    .catch((error) => {
      res = error;
    });
  return res;
}
