import axios from "axios";
import urls from "../Constants/urls";

export default async function GetComments(user_id, post_id) {
  // const postId = await post_id;
  const requestOptions = {
    url: `${urls.base}${urls.user}${user_id}${urls.post1}${post_id}${
      urls.comment1
      }`,
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  try {
    const response = await axios(requestOptions);

    return response.data;
  } catch (e) {
    console.log(e);
    throw new Error("The comments is loading PLease Wait!");
  }
}
