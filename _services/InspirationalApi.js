import axios from "axios";
import { ToastAndroid } from "react-native";
import urls from "../Constants/urls";

export default async function InspirationalApi(post_id,user_id,api_body,token) {
  try {
      console.log(post_id,user_id,api_body,token,'body');
    let response = await axios.post(
      `http://111.93.169.90:8008/user/${user_id}/post/${post_id}/inspiration/`,
      api_body,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      }
    );
    if (response.data.success) {
      ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
      return response.data;
    } else {
      throw new Error("ins failed");
    }
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
}
