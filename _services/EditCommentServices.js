import axios from "axios";
import { ToastAndroid } from "react-native";
import urls from "../Constants/urls";

export default async function EditingComments(
  data,
  token,
  post_id,
  comment_id
) {
  try {
    let response = await axios.patch(
      `${urls.base}${urls.post}${post_id}${urls.comment1}${comment_id}${
        urls.last_slash
      }`,
      data,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      }
    );

    if (response.data.success === true) {
      ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
      return response.data.success;
    } else {
      throw new Error("Post failed");
    }
  } catch (e) {
    console.log(e);
    throw new Error("post fail");
  }
}
