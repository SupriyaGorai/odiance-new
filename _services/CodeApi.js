import axios from 'axios';
import urls from '../Constants/urls';

export default async function CodeApi(refferal_code) {
  const requestOptions = {
    url: `${urls.base}${urls.refferalCode}`,
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    data: {refferal_code},
  };
  try{
    const response = await axios(requestOptions);
    
    if(response.data.success === true){
      return response.data;
    }else{
      throw  new Error ('Invalid code!')
    }
  }catch(e){
    throw  new Error ('Invalid code!')
  }
}