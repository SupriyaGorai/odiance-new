import { ToastAndroid } from 'react-native';
import urls from '../Constants/urls';
import axios from 'axios';
export default async  function  uploadVideoThumbnail(thumbnail,token) {
  console.log(thumbnail,'services')
    const video_thumbnail = {
        name: 'photo.jpeg',
        type: 'image/jpeg',
        uri: thumbnail.path,
      };
     const formData = new FormData();
     formData.append('avatar', video_thumbnail);
     formData.append('image_type','thumbnail');
   const requestOptions = {
        url: `${urls.base}${urls.avatar}`,
        method: 'POST',
        headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': `Token ${token}`,
        },
        data: formData,
      };
      try {
        const response = await axios(requestOptions);
        console.log(response,'thumbnailresponse');
        if (response.data.success === true) {
          ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
          // fetch the user data
          // TODO: set the token in local storage
          return response.data;
        } else {
          throw new Error('Post failed');
    
        }
      } catch (e) {
        console.log(e)
        throw new Error('post fail');
      }
 }