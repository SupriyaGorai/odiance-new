import axios from 'axios';
import { ToastAndroid } from 'react-native';
import urls from '../Constants/urls';
import { ReadUser } from '../_storage/Storage';
import { getInitialState } from '../_actions/LoginAction';


export default async function postService(formData, token) {
  var res;
  console.log(formData);
  await axios({
    url: `${urls.base}${urls.post}`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token ${token}`
    },
    body: JSON.stringify(formData),
  }).then((responseJSON) => {
    res = responseJSON;
  }).catch((error) => {
    res = error;
  });
  return res;


  // try {
  //   const response = await axios(requestOptions);
  //   console.log(response.data);
  //   if (response.data.success === true) {
  //     ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
  //     return response.data;
  //   } else {
  //     throw new Error('Post failed');
  //   }
  // } catch (e) {
  //   console.log(e)
  //   throw new Error('post fail');
  // }
}
