import axios from "axios";
import urls from "../Constants/urls";

export default async function GetPostDetails(user_id, post_id) {
  try {
    let listData = await axios.get(
      `${urls.base}${urls.user}${user_id}${urls.post1}${post_id}`,
      {
        headers: { "Content-Type": "application/json" },
      }
    );
   console.log('here')
    return listData.data;
  } catch (err) {
    console.log(e);
    throw new Error("post fail");
  }
}
