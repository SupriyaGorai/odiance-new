import axios from "axios";
import urls from "../Constants/urls";

export default async function LoadUserDetails(id) {
  const user_id = await id;
  const requestOptions = {
    url: `${urls.userbase}${user_id}${urls.last_slash}`,
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    const response = await axios(requestOptions);
    return response.data;

    //   if (response.data.success === true) {
    //      const res = await response.data;
    //    // ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
    //     return res;
    //   } else {
    //     throw new Error('loading failed');

    //   }
    // }
  } catch (e) {
    console.log(e);
    throw new Error("loading failed");
  }
}
