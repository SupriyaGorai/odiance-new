import axios from "axios";
import { ToastAndroid} from 'react-native';

export default async function get_the_video_list_of_a_specific_play_list(playlist_id, token) {
  try {
    let response = await axios.get(
        `http://111.93.169.90:8008/user/VideoInSpecificPlayList/${playlist_id}/`,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      }
    );
     console.log(response,'get');
     ToastAndroid.show(
      response.data.msg ,
      ToastAndroid.SHORT
    );
    return response;
  } catch (e) {
    console.log(e);
    throw new Error("post fail");
  }
}
