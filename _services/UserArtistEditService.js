import axios from 'axios';
import {
  ToastAndroid
} from 'react-native';
import urls from '../Constants/urls';

export default async function UpdateArtist(data, token,id) {
   const sid = id.toString();  
  const sendData = {
    id: "143",
    first_name:"kamalesh 11",
    last_name:"gupta",
    avatar:"https://s3-us-east-2.amazonaws.com/testodiance/143_creator_1596464672780photo.jpg",
    alumni:"",
    bio:"",
    about:"",
    city:"",
    country:"",
    //latitude:22.596415,
    //longitude:88.4311966666667,
    is_creator:"1",
   }
  const headers = {
    'Content-Type': 'application/json',
    Authorization: `Token ${token}`,
  }
  //console.log('sendData:',finalData);
  const url = `${urls.userbase}${urls.update_artist}`
  try {
    const obj = {
      method: 'POST',
      url: 'http://111.93.169.90:8008/user/update-artist/',
      data: JSON.stringify(sendData),
      headers: headers
    }

    console.log('axios obj: ', obj);
    let response = await axios(obj);

    if (response.data.success === true) {
      ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
      console.log('response: ', response);

      return response;
    } else {
      throw new Error("Post failed");
    }
  } catch (e) {
    console.log(e);
    throw new Error("post fail");
  }
}