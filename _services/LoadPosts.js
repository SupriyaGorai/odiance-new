import axios from "axios";
import urls from "../Constants/urls";

export default async function loadPosts(creator_id) {
  const requestOptions = {
    url: `${urls.userbase}${creator_id}${urls.post1}`,
    method: "GET",
    headers: { "Content-Type": "application/json" },
  };
  try {
    const response = await axios(requestOptions);
    return response.data;
  } catch (e) {
    throw new Error("The Profile is loading PLease Wait!");
  }
}
