import axios from "axios";
import { ToastAndroid} from 'react-native';

export default async function CreateMyNewPlaylistName( data,token) {
  try {
    let response = await axios.post(
      `http://111.93.169.90:8008/user/MyPlayList/`,
      data,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      }
    );
     console.log(response.data,'fav');
     ToastAndroid.show(
      response.data.msg ,
      ToastAndroid.SHORT
    );
    return response.data;
  } catch (e) {
    console.log(e);
    throw new Error("post fail");
  }
}
