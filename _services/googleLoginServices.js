import axios from 'axios';

import urls from '../Constants/urls';
import PushNotification from "react-native-push-notification";


export default async function googleLogin(userInfo,deviceId,deviceType) {

    console.log(userInfo,deviceId,deviceType,'from sevice')
    
  const requestOptions = {
    url: `${urls.userbase}${urls.social_signUp}`,
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    data: {
   
    
    login_type : "google",
    first_name  : userInfo.user.givenName,
    last_name : userInfo.user.familyName ,
    avatar : userInfo.user.photo,
    email : userInfo.user.email ,
    google_id : userInfo.user.id,
    fb_id : "",
    deviceType: deviceType ,
    deviceId: deviceId,
    },
  };
  try{
    const response = await axios(requestOptions);
    if (response.data.success === true) {
         console.log(response,'res')
        // fetch the user data
        // TODO: set the token in local storage
        return response.data;
        
    } else {
        return new Error('googleLoginServices failed');
        
    }
}catch(e){
  console.log(e)
    return new Error ('googleLoginServices fail');
}
}
  