import axios from "axios";

export default async function AddToViewList(userID, postId, data,token) {
  try {
    let response = await axios.post(
      `http://111.93.169.90:8008/user/${userID}/post/${postId}/views/`,
      data,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      }
    );
    // console.log(response.data);
    return response.data;
  } catch (e) {
    console.log(e);
    throw new Error("post fail");
  }
}
