
import axios from 'axios';
import urls from '../Constants/urls';


export const loginService = {
    login,
    
};

async function login(email, password,deviceId,deviceType) {
    const sendData = {
        email : email,
        password : password,
        deviceId : deviceId,
        deviceType : deviceType
    }
    const Data = JSON.stringify(sendData)
    console.log(Data);
    const requestOptions = {
        url: `${urls.base}${urls.login}`,
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        data: Data
    };
    try{
        
        console.log('service in try')
        const response = await axios(requestOptions);
        if (response.data.success === true) {
            // fetch the user datasddd
            // TODO: set the token in local storage
             return response.data;
        } else {
            console.log('service in try else')
              throw new Error(response.data.error.non_field_errors);
            
        }
    }catch(e){
        console.log('service in catch')
        throw new Error ('wrong username or password')
    }
        
}

