import axios from "axios";
import { ToastAndroid } from "react-native";
import urls from "../Constants/urls";

export default async function LikeDislike(post_id, api_body, token) {
  try {
    console.log(post_id, api_body, token, 'post_id, api_body, token');
    let response = await axios.post(
      `http://111.93.169.90:8008/post/${post_id}/like/`,
      api_body,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      }
    );
    if (response.data.success) {
      ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
      return response.data;
    } else {
      throw new Error("Post failed");
    }
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
}
