import axios from 'axios';
import urls from '../Constants/urls';
import { ToastAndroid } from 'react-native';

export default async function Register(user) {
  const sendData = {
      email: user.email,
      first_name: user.fname,
      last_name: user.lname,
      password: user.password,
      sex : user.gender,
      deviceId : user.deviceId,
      deviceType : user.deviceType,
  }
  // const Data = JSON.stringify(sendData);
  // console.log(Data);
  // const requestOptions = {
  //   url: `${urls.base}${urls.ViewerRegister}`,
  //   method: 'POST',
  //   headers: {'Content-Type': 'application/json'},
  //   data: Data
  // };
  try{
    const Data = JSON.stringify(sendData);
  console.log(Data);
  const requestOptions = {
    url: `${urls.base}${urls.ViewerRegister}`,
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    data: Data
  };
    console.log('here services');
    const response = await axios(requestOptions);
    console.log(response,'services');
    if (response.data.success === true) {
        // fetch the user data
        // TODO: set the token in local storage
        // ToastAndroid.show(response.data.msg,0.5);
        return response.data;
    } else {
         throw new Error('Registration failed');
        
        
    }
}catch(e){

  if(JSON.stringify(e.response.data.error.email)){
  console.log(JSON.stringify(e.response.data.error))
  ToastAndroid.show(JSON.stringify(e.response.data.error.email), 0.5)
  }
  throw new Error('Registration failed');
}
}