import axios from "axios";
import { ToastAndroid } from "react-native";
import urls from "../Constants/urls";

export default async function DeleteComments(comment_id, token, post_id) {
  try {
    let response = await axios.delete(
      `${urls.base}${urls.post}${post_id}${urls.comment1}${comment_id}${
        urls.last_slash
      }`,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      }
    );
    if (response.status === 200) {
      ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
      return response.status;
    } else {
      throw new Error("Post failed");
    }
  } catch (e) {
    console.log(e);
    throw new Error("post fail");
  }
}
