import axios from "axios";
import { ToastAndroid } from "react-native";
import urls from "../Constants/urls";

export default async function AddFollower(data, token, my_id) {
  try {
    let response = await axios.post(
      `http://111.93.169.90:8008/user/${my_id}/follow/`,
      data,
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Token ${token}`,
        },
      }
    );
    if (response.status === 200) {
      // ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
      return response;
    } else {
      throw new Error("Post failed");
    }
  } catch (e) {
    console.log(e);
    throw new Error("post fail");
  }
}
