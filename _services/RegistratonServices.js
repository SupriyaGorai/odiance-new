import axios from 'axios';
import urls from '../Constants/urls';


export default async function Register(user) {
  const sendData = {
    email: user.email,
      first_name: user.fname,
      last_name: user.lname,
      password: user.password,
      refferal_code : user.code,
      date_joined : user.dob,
      alumni : user.alumini,
      sex : user.gender, 
      latitude: user.latitude,
      longitude: user.longitude,
      deviceId : user.deviceId,
      deviceType : user.deviceType     

  }
  const Data = JSON.stringify(sendData)
  console.log(Data);
  const requestOptions = {
    url: `${urls.base}${urls.create}`,
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    data: Data
  };
  try{
    const response = await axios(requestOptions);
    console.log(response);
    if (response.data.success === true) {
         
        // fetch the user data
        // TODO: set the token in local storage
        return response.data;
    } else {
        throw new Error('Registration failed');
        
    }
}catch(e){
  console.log(e)
    throw new Error ('Registration fail');
}
}
  