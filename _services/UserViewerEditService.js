import axios from 'axios';
import { ToastAndroid } from 'react-native';
import urls from '../Constants/urls';
import { ReadUser } from '../_storage/Storage';
import { getInitialState } from '../_actions/LoginAction';





export default async function UpdateViewer(name,token,post_id) {
  const postId = await post_id
  // const path_viewer = await photo_path_Viewer
  console.log(postId,'huhkh');
  
  // const navigation = this.props.navigation;
  
 
  const requestOptions = {
    url: `${urls.userbase}${urls.update_viewer}`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token ${token}`
    },
    data: {
       
            
            first_name:name.first_name_edit,
            last_name:name.last_name_edit,
            avatar: '',
            sex : 'NA',
            is_creator:false,
            id:postId,
          
    },
  };

  try {
     
    //  const navigation = await nav
    //  console.log(navigation,'hhgg');
    const response = await axios(requestOptions);
    console.log(response.data);
    if (response.data.success === true) {
      ToastAndroid.show(response.data.msg, ToastAndroid.SHORT);
        // navigation.navigate('profile');
      // fetch the user data
      // TODO: set the token in local storage
      return response.data;
    } else {
      throw new Error('updateViewer failed');

    }
  } catch (e) {
    console.log(e)
    throw new Error('updateViewer fail');
  }
}

  
