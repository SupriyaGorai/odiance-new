import { ToastAndroid } from 'react-native';
import urls from '../Constants/urls';
import axios from 'axios';
import RNFetchBlob from 'react-native-fetch-blob';
export default async function uploadVideo(video, token, video_type) {
  var res;
  await RNFetchBlob.fetch(
    'POST',
    `${urls.base}${urls.video}`, {
    'Content-Type': 'multipart/form-data',
    'Authorization': `Token ${token}`,
  }, [
    { name: 'video', filename: `${new Date().getTime()}.mp4`, type: 'video/mp4', data: RNFetchBlob.wrap(video.path) },
    { name: 'video_type', data: video_type },
  ]).then((response) => {
    res = response.json();
  }).catch(err => {
    console.log(err);
  });

  return res;
 
 }