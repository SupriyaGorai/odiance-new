export const HomeConstants = {
  toggleSearch: "TOGGLE_SEARCH",
  dateValue: "DATE_VALUE",
  searchText: "SEARCH_TEXT",
  sliderValue: "SLIDER_VALUE",
};
